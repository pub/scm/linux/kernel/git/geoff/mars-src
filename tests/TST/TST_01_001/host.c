/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <malloc.h>
#include <mars/task.h>

#include "mars_test.h"

static int starve_memory = 0;

void *mars_malloc(size_t size)
{
	return starve_memory ? 0 : malloc(size);
}

uint64_t mars_ea_memalign(size_t boundary, size_t size)
{
	return starve_memory ? 0 : mars_ptr_to_ea(memalign(boundary, size));
}

void mars_ea_free(uint64_t ea)
{
	free(mars_ea_to_ptr(ea));
}

int main(void)
{
	int ret;
	struct mars_context *mars_context;

	starve_memory = 1;

	ret = mars_context_create(&mars_context, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_ERROR_MEMORY);

	return 0;
}
