/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <spu_mfcio.h>
#include <mars/module.h>

#include "mars_test.h"
#include "common.h"

void mars_module_main(void)
{
	int ret, i;
	static struct mars_mutex mutex;
	uint32_t shared_data __attribute__((aligned(16)));
	uint64_t shared_data_ea;
	uint64_t mutex_ea;

	shared_data_ea = *(uint64_t *)((uint8_t *)mars_module_get_workload() +
		offsetof(struct mars_workload_context, data));

	mutex_ea = *(uint64_t *)((uint8_t *)mars_module_get_workload() +
		offsetof(struct mars_workload_context, data) +
		sizeof(uint64_t));

	for (i = 0; i < LOCK_COUNT; i++) {
		ret = mars_module_mutex_lock_get(mutex_ea, &mutex);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		mfc_get(&shared_data, shared_data_ea, sizeof(uint32_t),
			0,0,0);
		mfc_write_tag_mask(1 << 0);
		mfc_read_tag_status_all();

		shared_data++;

		mfc_put(&shared_data, shared_data_ea, sizeof(uint32_t),
			0,0,0);
		mfc_write_tag_mask(1 << 0);
		mfc_read_tag_status_all();

		ret = mars_module_mutex_unlock_put(mutex_ea, &mutex);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	mars_module_workload_finish();
}
