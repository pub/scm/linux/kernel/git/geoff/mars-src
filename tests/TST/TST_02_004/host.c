/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <libspe2.h>
#include <mars/base.h>
#include <mars/context.h>
#include <mars/mutex.h>
#include <mars/workload_queue.h>

#include "mars_test.h"
#include "common.h"

extern spe_program_handle_t mpu_prog;

static uint32_t shared_data __attribute__((aligned(16)));
static uint64_t mutex_ea;

int main(void)
{
	int ret, i;
	struct mars_context *mars_context;
	uint16_t workload_id[WORKLOAD_COUNT];

	shared_data = 0;

	ret = mars_context_create(&mars_context, 0, 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_mutex_create(&mutex_ea);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < WORKLOAD_COUNT; i++) {
		uint64_t workload_ea;

		ret = mars_workload_queue_add_begin(
			mars_context, &workload_id[i], &workload_ea,
			mpu_prog.elf_image, NULL);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	
		ret = mars_workload_queue_add_end(
			mars_context, workload_id[i], 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_workload_queue_schedule_begin(
			mars_context, workload_id[i], 0, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		mars_ea_put_uint64(
			workload_ea + offsetof(struct mars_workload_context,
					       data),
			mars_ptr_to_ea(&shared_data));
		mars_ea_put_uint64(
			workload_ea + offsetof(struct mars_workload_context,
					       data) + sizeof(uint64_t),
			mutex_ea);

		ret = mars_workload_queue_schedule_end(
			mars_context, workload_id[i], 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	for (i = 0; i < WORKLOAD_COUNT; i++) {
		ret = mars_workload_queue_wait(
			mars_context, workload_id[i], 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_workload_queue_remove_begin(
			mars_context, workload_id[i], 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_workload_queue_remove_end(
			mars_context, workload_id[i], 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_mutex_destroy(mutex_ea);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_context);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	MARS_TEST_ASSERT_ERROR(shared_data, WORKLOAD_COUNT * LOCK_COUNT);

	return 0;
}
