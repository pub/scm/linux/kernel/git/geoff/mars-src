/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <libspe2.h>
#include <mars/task.h>

#include "mars_test.h"

#define SEND_THREAD_COUNT	150
#define SEND_TASK_COUNT		150
#define WAIT_TASK_COUNT		300

extern spe_program_handle_t mpu1_prog, mpu2_prog;

static uint32_t counter;
static struct mars_task_id wait_task_id[WAIT_TASK_COUNT]
	__attribute__((aligned(16)));

void *thread_proc(void *arg)
{
	int ret;
	struct mars_task_id *task_id = (struct mars_task_id *)arg;

	mars_test_counter_add(&counter, 1);
	mars_test_counter_wait(&counter, SEND_THREAD_COUNT);

	ret = mars_task_signal_send(task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return NULL;
}

int main(void)
{
	int ret, task_ret, i;
	struct mars_context *mars_context;
	struct mars_task_id send_task_id[SEND_TASK_COUNT];
	struct mars_task_args task_args;
	pthread_t thread_handles[SEND_THREAD_COUNT];

	mars_test_counter_set(&counter, 0);

	ret = mars_context_create(&mars_context, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < SEND_TASK_COUNT; i++) {
		ret = mars_task_create(
			mars_context,
			&send_task_id[i],
			NULL,
			mpu1_prog.elf_image,
			0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	for (i = 0; i < WAIT_TASK_COUNT; i++) {
		ret = mars_task_create(
			mars_context,
			&wait_task_id[i],
			NULL,
			mpu2_prog.elf_image,
			MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	for (i = 0; i < WAIT_TASK_COUNT; i++) {
		ret = mars_task_schedule(
			&wait_task_id[i],
			NULL,
			0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	for (i = 0; i < SEND_TASK_COUNT; i++) {
		task_args.type.u64[0] = mars_ptr_to_ea(&wait_task_id[i]);

		ret = mars_task_schedule(
			&send_task_id[i],
			&task_args,
			0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	for (i = 0; i < SEND_THREAD_COUNT; i++) {
		ret = pthread_create(
			&thread_handles[i],
			NULL,
			thread_proc,
			&wait_task_id[SEND_TASK_COUNT + i]);
		MARS_TEST_ASSERT_ERROR(ret, 0);
	}

	for (i = 0; i < SEND_THREAD_COUNT; i++) {
		ret = pthread_join(
			thread_handles[i],
			NULL);
		MARS_TEST_ASSERT_ERROR(ret, 0);
	}

	for (i = 0; i < SEND_TASK_COUNT; i++) {
		ret = mars_task_wait(&send_task_id[i], &task_ret);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(task_ret, MARS_SUCCESS);
	}

	for (i = 0; i < WAIT_TASK_COUNT; i++) {
		ret = mars_task_wait(&wait_task_id[i], &task_ret);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(task_ret, MARS_SUCCESS);
	}

	for (i = 0; i < SEND_TASK_COUNT; i++) {
		ret = mars_task_destroy(&send_task_id[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	for (i = 0; i < WAIT_TASK_COUNT; i++) {
		ret = mars_task_destroy(&wait_task_id[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_context);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
