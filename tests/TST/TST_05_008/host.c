/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <libspe2.h>
#include <mars/task.h>

#include "mars_test.h"

#define SET_TASK_COUNT		32

extern spe_program_handle_t mpu1_prog, mpu2_prog;

int main(void)
{
	int ret, task_ret, i;
	struct mars_context *mars_context;
	struct mars_task_id set_task_id[SET_TASK_COUNT];
	struct mars_task_id wait_task_id;
	struct mars_task_args task_args;
	uint64_t event_flag_ea;

	ret = mars_context_create(&mars_context, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_event_flag_create(
		mars_context,
		&event_flag_ea,
		MARS_TASK_EVENT_FLAG_MPU_TO_MPU,
		MARS_TASK_EVENT_FLAG_CLEAR_MANUAL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(
		mars_context,
		&wait_task_id,
		NULL,
		mpu1_prog.elf_image,
		MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < SET_TASK_COUNT; i++) {
		ret = mars_task_create(
			mars_context,
			&set_task_id[i],
			NULL,
			mpu2_prog.elf_image,
			0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	task_args.type.u64[0] = event_flag_ea;

	ret = mars_task_schedule(
		&wait_task_id,
		&task_args,
		0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < SET_TASK_COUNT; i++) {
		task_args.type.u32[2] = i;

		ret = mars_task_schedule(
			&set_task_id[i],
			&task_args,
			0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	for (i = 0; i < SET_TASK_COUNT; i++) {
		ret = mars_task_wait(&set_task_id[i], &task_ret);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(task_ret, MARS_SUCCESS);
	}

	ret = mars_task_wait(&wait_task_id, &task_ret);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_ERROR(task_ret, MARS_SUCCESS);

	for (i = 0; i < SET_TASK_COUNT; i++) {
		ret = mars_task_destroy(&set_task_id[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_task_destroy(&wait_task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_event_flag_destroy(event_flag_ea);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_context);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
