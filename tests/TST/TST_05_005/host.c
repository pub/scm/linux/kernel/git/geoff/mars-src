/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <libspe2.h>
#include <mars/task.h>

#include "mars_test.h"

#define SET_THREAD_COUNT	32

extern spe_program_handle_t mpu_prog;

static uint32_t counter;
static uint64_t event_flag_ea;

void *thread_proc(void *arg)
{
	int ret;
	uint32_t bits = 1 << *((int *)arg);

	mars_test_counter_add(&counter, 1);
	mars_test_counter_wait(&counter, SET_THREAD_COUNT);

	ret = mars_task_event_flag_set(event_flag_ea, bits);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return NULL;
}

int main(void)
{
	int ret, task_ret, i;
	struct mars_context *mars_context;
	struct mars_task_id task_id;
	struct mars_task_args task_args;
	pthread_t thread_handles[SET_THREAD_COUNT];
	int thread_args[SET_THREAD_COUNT];

	mars_test_counter_set(&counter, 0);

	ret = mars_context_create(&mars_context, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_event_flag_create(
		mars_context,
		&event_flag_ea,
		MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
		MARS_TASK_EVENT_FLAG_CLEAR_MANUAL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	task_args.type.u64[0] = event_flag_ea;

	ret = mars_task_create(
		mars_context,
		&task_id,
		NULL,
		mpu_prog.elf_image,
		MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < SET_THREAD_COUNT; i++) {
		thread_args[i] = i;

		ret = pthread_create(
			&thread_handles[i],
			NULL,
			thread_proc,
			thread_args + i);
		MARS_TEST_ASSERT_ERROR(ret, 0);
	}

	for (i = 0; i < SET_THREAD_COUNT; i++) {
		ret = pthread_join(
			thread_handles[i],
			NULL);
		MARS_TEST_ASSERT_ERROR(ret, 0);
	}

	ret = mars_task_wait(&task_id, &task_ret);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_ERROR(task_ret, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_event_flag_destroy(event_flag_ea);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_context);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
