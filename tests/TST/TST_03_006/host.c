/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <libspe2.h>
#include <mars/task.h>

#include "mars_test.h"

#define THREAD_COUNT	10
#define TASK_COUNT	75

extern spe_program_handle_t mpu_prog;

static uint32_t counter_schedule;
static uint32_t counter_wait;
static struct mars_context *mars_context;

void *thread_proc(void *arg)
{
	(void)arg;

	int ret, i;
	struct mars_task_id task_id[TASK_COUNT];

	for (i = 0; i < TASK_COUNT; i++) {
		ret = mars_task_create(
			mars_context,
			&task_id[i],
			NULL,
			mpu_prog.elf_image,
			0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	mars_test_counter_add(&counter_schedule, 1);
	mars_test_counter_wait(&counter_schedule, THREAD_COUNT);

	for (i = 0; i < TASK_COUNT; i++) {
		ret = mars_task_schedule(&task_id[i], NULL, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	mars_test_counter_add(&counter_wait, 1);
	mars_test_counter_wait(&counter_wait, THREAD_COUNT);

	for (i = 0; i < TASK_COUNT; i++) {
		ret = mars_task_wait(&task_id[i], NULL);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	for (i = 0; i < TASK_COUNT; i++) {
		ret = mars_task_destroy(&task_id[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	return NULL;
}

int main(void)
{
	int ret, i;
	pthread_t thread_handles[THREAD_COUNT];

	mars_test_counter_set(&counter_schedule, 0);
	mars_test_counter_set(&counter_wait, 0);

	ret = mars_context_create(&mars_context, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < THREAD_COUNT; i++) {
		ret = pthread_create(
			&thread_handles[i],
			NULL,
			thread_proc,
			NULL);
		MARS_TEST_ASSERT_ERROR(ret, 0);
	}
	
	for (i = 0; i < THREAD_COUNT; i++) {
		ret = pthread_join(
			thread_handles[i],
			NULL);
		MARS_TEST_ASSERT_ERROR(ret, 0);
	}

	ret = mars_context_destroy(mars_context);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
