/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <libspe2.h>
#include <mars/task.h>

#include "mars_test.h"

#define THREAD_COUNT	10

static uint32_t counter_create;
static uint32_t counter_destroy;

void *thread_proc(void *arg)
{
	int ret;

	mars_test_counter_add(&counter_create, 1);
	mars_test_counter_wait(&counter_create, THREAD_COUNT);

	ret = mars_context_create((struct mars_context **)arg, 0, 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	mars_test_counter_add(&counter_destroy, 1);
	mars_test_counter_wait(&counter_destroy, THREAD_COUNT);

	ret = mars_context_destroy(*((struct mars_context **)arg));
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return NULL;
}

int main(void)
{
	int ret, i;
	pthread_t thread_handles[THREAD_COUNT];
	struct mars_context *mars_contexts[THREAD_COUNT];

	mars_test_counter_set(&counter_create, 0);
	mars_test_counter_set(&counter_destroy, 0);

	for (i = 0; i < THREAD_COUNT; i++) {
		ret = pthread_create(
			&thread_handles[i],
			NULL,
			thread_proc,
			(void *)&mars_contexts[i]);
		MARS_TEST_ASSERT_ERROR(ret, 0);
	}

	for (i = 0; i < THREAD_COUNT; i++) {
		ret = pthread_join(
			thread_handles[i],
			NULL);
		MARS_TEST_ASSERT_ERROR(ret, 0);
	}

	return 0;
}
