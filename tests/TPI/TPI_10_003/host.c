/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#define TEST1_ELEMENT_COUNT 4096

#include <pthread.h>
#include <libspe2.h>
#include <mars/task.h>
#include <mars/mutex.h>
#include <mars_test.h>

struct thread_arg_st {
	int32_t *start_ticks;
	int32_t *end_ticks;
	uint64_t mutex;
	pthread_barrier_t *barrier;
};

void *test_thread1(void *arg)
{
	int ret, i;
	struct thread_arg_st *thread_arg = arg;
	int32_t *end_ticks = thread_arg->end_ticks;

	ret = mars_mutex_lock(thread_arg->mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < TEST1_ELEMENT_COUNT; i++) {
		ret = mars_mutex_lock(thread_arg->mutex);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		end_ticks[i] = mars_task_get_ticks();

		ret = pthread_barrier_wait(thread_arg->barrier);
		MARS_TEST_ASSERT(ret == PTHREAD_BARRIER_SERIAL_THREAD
				 || ret == 0);
	}

	return NULL;
}

void *test_thread2(void *arg)
{
	int ret, i;
	struct thread_arg_st *thread_arg = arg;
	int32_t *start_ticks = thread_arg->start_ticks;


	for (i = 0; i < TEST1_ELEMENT_COUNT; i++) {
		usleep(500);
		start_ticks[i] = mars_task_get_ticks();
		ret = mars_mutex_unlock(thread_arg->mutex);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = pthread_barrier_wait(thread_arg->barrier);
		MARS_TEST_ASSERT(ret == PTHREAD_BARRIER_SERIAL_THREAD
				 || ret == 0);
	}

	ret = mars_mutex_unlock(thread_arg->mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return NULL;
}

void test1(void)
{
	int ret, i;
	int threads = 2;
	size_t size;

	struct mars_context *mars_ctx;
	struct thread_arg_st thread_arg;
	pthread_t thread[threads];
	pthread_barrier_t barrier;
	uint64_t mutex;
	int32_t *start_ticks, *end_ticks, *ticks;

	size = sizeof(uint32_t) * TEST1_ELEMENT_COUNT;

	ret = posix_memalign((void **)&start_ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);
	ret = posix_memalign((void **)&end_ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);
	ret = posix_memalign((void **)&ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_context_create(&mars_ctx, 0, 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_mutex_create(&mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = pthread_barrier_init(&barrier, NULL, threads);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	thread_arg.start_ticks = start_ticks;
	thread_arg.end_ticks = end_ticks;
	thread_arg.barrier = &barrier;
	thread_arg.mutex = mutex;

	ret = pthread_create(&thread[0], NULL, test_thread1, &thread_arg);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = pthread_create(&thread[1], NULL, test_thread2, &thread_arg);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	for (i = 0; i < threads; i++) {
		ret = pthread_join(thread[i], NULL);
		MARS_TEST_ASSERT_EQUAL(ret, 0);
	}

	ret = mars_mutex_destroy(mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	size = TEST1_ELEMENT_COUNT;

	for (i = 0; i < size; i++) {
		ticks[i] = end_ticks[i] - start_ticks[i];
	}

	mars_test_print_statistics(ticks, size,
				   "mutex start of unlock end of lock");
	/*Note:end of unlock to end of lock time to small to be reliably measured*/

	free(start_ticks);
	free(end_ticks);
	free(ticks);
}

int main(int argc, char *argv[])
{
	test1();

	return 0;
}
