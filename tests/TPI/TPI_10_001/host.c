/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#define TEST1_ELEMENT_COUNT 4096

#include <pthread.h>
#include <libspe2.h>
#include <mars/task.h>
#include <mars/mutex.h>
#include <mars_test.h>

struct thread_arg_st {
	int32_t *ticks1;
	int32_t *ticks2;
};

void *test_thread(void *arg)
{
	int ret, i;
	int32_t start, end;
	struct thread_arg_st *thread_arg = arg;
	int32_t *lock_ticks = thread_arg->ticks1;
	int32_t *unlock_ticks = thread_arg->ticks2;
	uint64_t mutex;

	ret = mars_mutex_create(&mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < TEST1_ELEMENT_COUNT; i++) {
		start = mars_task_get_ticks();
		ret = mars_mutex_lock(mutex);
		end = mars_task_get_ticks();
		lock_ticks[i] = end - start;
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		start = mars_task_get_ticks();
		ret = mars_mutex_unlock(mutex);
		end = mars_task_get_ticks();
		unlock_ticks[i] = end - start;
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_mutex_destroy(mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return NULL;
}

void test1(void)
{
	int ret;
	size_t size;
	int32_t *lock_ticks, *unlock_ticks;
	struct mars_context *mars_ctx;
	struct thread_arg_st thread_arg;
	pthread_t thread;

	size = sizeof(uint32_t) * TEST1_ELEMENT_COUNT;

	ret = posix_memalign((void **)&lock_ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);
	ret = posix_memalign((void **)&unlock_ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_context_create(&mars_ctx, 0, 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	thread_arg.ticks1 = lock_ticks;
	thread_arg.ticks2 = unlock_ticks;
	ret = pthread_create(&thread, NULL, test_thread, &thread_arg);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = pthread_join(thread, NULL);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	size = TEST1_ELEMENT_COUNT;
	mars_test_print_statistics(lock_ticks, size, "mutex lock");
	mars_test_print_statistics(unlock_ticks, size, "mutex unlock");

	free(lock_ticks);
	free(unlock_ticks);
}

int main(int argc, char *argv[])
{
	test1();

	return 0;
}
