/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <libspe2.h>
#include <mars/task.h>
#include <mars/mutex.h>
#include <mars_test.h>
#include "common.h"

extern spe_program_handle_t mpu1_prog, mpu2_prog ALIGN128;

static struct mars_task_args task_args;

int test1(void)
{
	int i, exit_code, ret;
	int32_t start[LOOP_COUNT] ALIGN128;
	int32_t end[LOOP_COUNT] ALIGN128;
	int32_t time[LOOP_COUNT] ALIGN128;
	struct test_task_args_st args ALIGN128;
	uint32_t tb_freq;

	uint64_t mutex ALIGN128;
	int spe_cnt = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);

	static struct mars_context *mars ALIGN128;
	static struct mars_task_id task_id[2] ALIGN128;

	ret = mars_context_create(&mars, spe_cnt, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_test_get_timebase_freq(&tb_freq);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_mutex_create(&mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(mars, &task_id[0], "10004", mpu1_prog.elf_image,
			       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(mars, &task_id[1], "10004", mpu2_prog.elf_image,
			       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	args.mutex_ea = mutex;
	args.tb_freq = tb_freq;
	args.ticks1_ea = mars_ptr_to_ea(start);
	args.ticks2_ea = mars_ptr_to_ea(end);
	task_args.type.u64[0] = mars_ptr_to_ea(&args);

	for (i = 0; i < 2; i++) {
		ret = mars_task_schedule(&task_id[i], &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}
	for (i = 0; i < 2; i++) {
		ret = mars_task_wait(&task_id[i], &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_destroy(&task_id[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_mutex_destroy(mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < LOOP_COUNT; i++) {
		time[i] = end[i] - start[i];
	}

	mars_test_print_statistics(time, LOOP_COUNT,
				   "mutex: start of put - end of get");
	return 0;
}

int main(void)
{
	int ret;
	ret = test1();

	return 0;
}
