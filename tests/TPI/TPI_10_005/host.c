/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <libspe2.h>
#include <mars/task.h>
#include <mars/mutex.h>
#include <mars_test.h>
#include "common.h"

extern spe_program_handle_t mpu1_prog;

struct thread_arg_st {
	int32_t *ticks1;
	int32_t *ticks2;
	struct mars_context *mars_ctx;
};

void *test_thread(void *arg)
{
	int i, ret;
	struct thread_arg_st *thread_arg = arg;
	int32_t *start_ticks = thread_arg->ticks1;
	int32_t *end_ticks = thread_arg->ticks2;
	struct mars_context *mars_ctx = thread_arg->mars_ctx;
	struct mars_task_id task_id;
	struct mars_task_args task_args;
	static __thread struct test_task_args_st args ALIGN128;

	uint32_t sender_to_receiver = UINT32_MAX;
	uint32_t receiver_to_sender = UINT32_MAX;
	uint64_t mutex ALIGN128;

	ret = mars_task_create(mars_ctx, &task_id, NULL,
			       mpu1_prog.elf_image,
			       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_mutex_create(&mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	args.mutex_ea = mutex;
	args.sender_to_receiver_ea = mars_ptr_to_ea(&sender_to_receiver);
	args.receiver_to_sender_ea = mars_ptr_to_ea(&receiver_to_sender);
	args.ticks1_ea = mars_ptr_to_ea(start_ticks);
	args.ticks2_ea = mars_ptr_to_ea(end_ticks);
	task_args.type.u64[0] = mars_ptr_to_ea(&args);

	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < TEST1_ELEMENT_COUNT; i++) {
		ret = mars_mutex_lock(mutex);
		mars_test_counter_set(&sender_to_receiver, i);
		mars_test_counter_wait(&receiver_to_sender, i);

		usleep(1000);/*give mpu time to block*/

		start_ticks[i] = mars_task_get_ticks();
		ret = mars_mutex_unlock(mutex);
	}

	ret = mars_mutex_destroy(mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_wait(&task_id, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return NULL;
}

void test1()
{
	int ret, i;
	size_t size;
	int32_t *start_ticks, *end_ticks, *ticks;
	struct mars_context *mars_ctx;
	pthread_t thread;
	struct thread_arg_st thread_arg;

	size = sizeof(uint32_t) * TEST1_ELEMENT_COUNT;

	ret = posix_memalign((void **)&start_ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = posix_memalign((void **)&end_ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = posix_memalign((void **)&ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_context_create(&mars_ctx, 0, 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	thread_arg.ticks1 = start_ticks;
	thread_arg.ticks2 = end_ticks;
	thread_arg.mars_ctx = mars_ctx;

	ret = pthread_create(&thread, NULL, test_thread, &thread_arg);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = pthread_join(thread, NULL);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	size = TEST1_ELEMENT_COUNT;

	for (i = 0; i < size; i++) {
		ticks[i] = end_ticks[i] - start_ticks[i];
	}

	mars_test_print_statistics(ticks, size, "start of acquire end of release");

	free(start_ticks);
	free(end_ticks);
	free(ticks);
}

int main(int argc, char *argv[])
{
	test1();

	return 0;
}
