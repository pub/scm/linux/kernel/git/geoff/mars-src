/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars_test.h>
#include <mars/task.h>
#include <kernel_internal_types.h>
#include "common.h"

int mars_task_main(const struct mars_task_args *task_args)
{
	int32_t i, ret, lock[LOOP_COUNT], unlock[LOOP_COUNT];
	int32_t end, start;
	struct test_task_args_st args ALIGN128;
	static struct mars_mutex mutex;

	mfc_get(&args, task_args->type.u64[0],
		sizeof(struct test_task_args_st), 0, 0, 0);
	mfc_write_tag_mask(1);
	mfc_read_tag_status_all();

	for (i = 0; i < LOOP_COUNT; i++) {
		start = mars_task_get_ticks();
		ret = mutex_lock_get(args.mutex_ea, &mutex);
		end = mars_task_get_ticks();
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		lock[i] = end - start;

		start = mars_task_get_ticks();
		ret = mutex_unlock_put(args.mutex_ea, &mutex);
		end = mars_task_get_ticks();
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		unlock[i] = end - start;
	}

	mfc_put(lock, args.ticks1_ea, LOOP_COUNT * 4, 0, 0, 0);
	mfc_write_tag_mask(1);
	mfc_read_tag_status_all();

	mfc_put(unlock, args.ticks2_ea, LOOP_COUNT * 4, 0, 0, 0);
	mfc_write_tag_mask(1);
	mfc_read_tag_status_all();

	mars_task_exit(0);

	return 0;
}
