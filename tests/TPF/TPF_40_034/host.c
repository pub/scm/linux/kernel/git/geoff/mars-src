/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>
#include <common.h>

extern spe_program_handle_t mpu1_prog, mpu2_prog, mpu3_prog;
extern spe_program_handle_t mpu4_prog, mpu5_prog, mpu6_prog;

static uint32_t shared_resources[MAX_RECEIVERS] ALIGN128;

/*
 * This test times the execution time of mars_task_queue_push() and
 * mars_task_queue_pop() separately using mpu1.c and mpu2.c.
 */

uint32_t aligned_num(uint32_t num, uint32_t aligned)
{
	return num + (aligned - num % aligned);
}

void test1(uint32_t receivers)
{
	int ret;
	int32_t *push_ticks;
	int32_t *pop_ticks;
	struct mars_context *mars_ctx;
	struct mars_task_id sender_task_id, receiver_task_id[receivers];
	struct mars_task_args task_args;
	uint64_t queue;
	uint64_t barrier1;
	uint64_t barrier2;

	char name[128];

	static struct test_task_args_st args ALIGN128;

	uint32_t i, size;

	size =
	    aligned_num(TEST1_ELEMENT_COUNT * receivers,
			128) * sizeof(uint32_t);
	ret = posix_memalign((void **)&push_ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);
	ret = posix_memalign((void **)&pop_ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_test_get_timebase_freq(&args.tb_freq);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_create(mars_ctx, &queue, ELEMENT_SIZE, QUEUE_DEPTH,
				   MARS_TASK_QUEUE_MPU_TO_MPU);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	sprintf(name, "%d", 0);
	ret = mars_task_create(mars_ctx, &sender_task_id, name,
			     mpu1_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_barrier_create(mars_ctx, &barrier1, receivers + 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_barrier_create(mars_ctx, &barrier2, receivers + 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < receivers; i++) {
		sprintf(name, "%d", i);
		ret = mars_task_create(mars_ctx, &receiver_task_id[i], name,
				       mpu2_prog.elf_image,
				       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	for (i = 0; i < receivers; i++) {
		shared_resources[i] = 0;
		args.shared_resources_ea[i] =
		    mars_ptr_to_ea(&shared_resources[i]);
	}

	args.queue_ea = queue;
	args.barrier1_ea = barrier1;
	args.barrier2_ea = barrier2;
	args.ticks1_ea = mars_ptr_to_ea(push_ticks);
	args.ticks2_ea = mars_ptr_to_ea(pop_ticks);
	args.receivers = receivers;

	task_args.type.u64[0] = mars_ptr_to_ea(&args);
	ret = mars_task_schedule(&sender_task_id, &task_args, 255);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < receivers; i++) {
		ret =
		    mars_task_schedule(&receiver_task_id[i], &task_args,
				       receivers - i);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_task_wait(&sender_task_id, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < receivers; i++) {
		ret = mars_task_wait(&receiver_task_id[i], NULL);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_task_destroy(&sender_task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < receivers; i++) {
		ret = mars_task_destroy(&receiver_task_id[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_task_barrier_destroy(barrier1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_barrier_destroy(barrier2);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_destroy(queue);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	mars_test_print_statistics(push_ticks, TEST1_ELEMENT_COUNT * receivers,
				   "push");
	mars_test_print_statistics(pop_ticks, TEST1_ELEMENT_COUNT * receivers,
				   "pop");

	free(push_ticks);
	free(pop_ticks);
}

#ifndef QUEUE_INTERMEDIATE

void test2(uint32_t receivers)
{
	int ret;
	int32_t *ticks;
	struct mars_context *mars_ctx;
	struct mars_task_id sender_task_id, receiver_task_id[receivers];
	struct mars_task_args task_args;
	uint64_t queue;
	uint64_t barrier1;
	uint64_t barrier2;
	char name[128];

	static struct test_task_args_st args ALIGN128;

	uint32_t i, size;
	static uint32_t mpu_run ALIGN128;

	size = aligned_num(TEST1_ELEMENT_COUNT * receivers,
			128) * sizeof(uint32_t);
	ret = posix_memalign((void **)&ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_test_get_timebase_freq(&args.tb_freq);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_create(mars_ctx, &queue, ELEMENT_SIZE, QUEUE_DEPTH,
				   MARS_TASK_QUEUE_MPU_TO_MPU);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	sprintf(name, "%d", 255);
	ret = mars_task_create(mars_ctx, &sender_task_id, name,
			     mpu3_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_barrier_create(mars_ctx, &barrier1, receivers + 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_barrier_create(mars_ctx, &barrier2, receivers + 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < receivers; i++) {
		sprintf(name, "%02d", i);
		ret = mars_task_create(mars_ctx, &receiver_task_id[i], name,
				       mpu4_prog.elf_image,
				       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	args.queue_ea = queue;
	args.barrier1_ea = barrier1;
	args.barrier2_ea = barrier2;
	args.ticks1_ea = mars_ptr_to_ea(ticks);
	args.receivers = receivers;
	args.mpu_run_ea = mars_ptr_to_ea(&mpu_run);
	task_args.type.u64[0] = mars_ptr_to_ea(&args);

	for (i = 0; i < receivers; i++) {
		shared_resources[i] = 0;
	}

	for (i = 0; i < receivers; i++) {
		args.shared_resources_ea[i] =
		    mars_ptr_to_ea(&shared_resources[i]);
	}

	ret = mars_task_schedule(&sender_task_id, &task_args, 255);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < receivers; i++) {
		ret = mars_task_schedule(&receiver_task_id[i], &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_task_wait(&sender_task_id, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < receivers; i++) {
		ret = mars_task_wait(&receiver_task_id[i], NULL);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_task_destroy(&sender_task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < receivers; i++) {
		ret = mars_task_destroy(&receiver_task_id[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_task_barrier_destroy(barrier1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_barrier_destroy(barrier2);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_destroy(queue);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
#ifdef QUEUE_EMPTY
	mars_test_print_statistics(ticks, TEST2_ELEMENT_COUNT * receivers,
				   "beginning of push - end of pop");
#endif
#ifdef QUEUE_FULL
	mars_test_print_statistics(ticks, TEST2_ELEMENT_COUNT * receivers,
				   "beginning of pop - end of push");
#endif
	free(ticks);
}

void test3(uint32_t receivers)
{
	int ret;
	int32_t *ticks;
	struct mars_context *mars_ctx;
	struct mars_task_id sender_task_id, receiver_task_id[receivers];
	struct mars_task_args task_args;
	uint64_t queue;
	uint64_t barrier1;
	uint64_t barrier2;
	char name[128];

	static struct test_task_args_st args ALIGN128;

	uint32_t i, size;
	static uint32_t mpu_run ALIGN128;

	size =
	    aligned_num(TEST1_ELEMENT_COUNT * receivers,
			128) * sizeof(uint32_t);
	ret = posix_memalign((void **)&ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_test_get_timebase_freq(&args.tb_freq);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_create(mars_ctx, &queue, ELEMENT_SIZE, QUEUE_DEPTH,
				   MARS_TASK_QUEUE_MPU_TO_MPU);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	sprintf(name, "%d", 254);
	ret = mars_task_create(mars_ctx, &sender_task_id, name,
			     mpu5_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_barrier_create(mars_ctx, &barrier1, receivers + 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_barrier_create(mars_ctx, &barrier2, receivers + 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < receivers; i++) {
		sprintf(name, "%02d", i);
		ret = mars_task_create(mars_ctx, &receiver_task_id[i], name,
				       mpu6_prog.elf_image,
				       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	args.queue_ea = queue;
	args.barrier1_ea = barrier1;
	args.barrier2_ea = barrier2;
	args.ticks1_ea = mars_ptr_to_ea(ticks);
	args.receivers = receivers;
	args.mpu_run_ea = mars_ptr_to_ea(&mpu_run);
	task_args.type.u64[0] = mars_ptr_to_ea(&args);

	for (i = 0; i < receivers; i++) {
		shared_resources[i] = 0;
		args.shared_resources_ea[i] =
		    mars_ptr_to_ea(&shared_resources[i]);
	}

	ret = mars_task_schedule(&sender_task_id, &task_args, 255);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < receivers; i++) {
		ret = mars_task_schedule(&receiver_task_id[i], &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_task_wait(&sender_task_id, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < receivers; i++) {
		ret = mars_task_wait(&receiver_task_id[i], NULL);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_task_destroy(&sender_task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < receivers; i++) {
		ret = mars_task_destroy(&receiver_task_id[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_task_barrier_destroy(barrier1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_barrier_destroy(barrier2);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_destroy(queue);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

#ifdef QUEUE_EMPTY
	mars_test_print_statistics(ticks, TEST3_ELEMENT_COUNT * receivers,
				   "end of push - end of pop");
#endif
#ifdef QUEUE_FULL
	mars_test_print_statistics(ticks, TEST3_ELEMENT_COUNT * receivers,
				   "end of pop - end of push");
#endif
	free(ticks);

}
#endif

int main(int argc, char *argv[])
{
	MARS_TEST_PERF_INIT();
	int i;
	for (i = MAX_RECEIVERS; i <= MAX_RECEIVERS; i++) {
		test1(i);
#ifndef QUEUE_INTERMEDIATE
		test2(i);
		test3(i);
#endif
	}

	return 0;
}
