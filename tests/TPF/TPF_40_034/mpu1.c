/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <malloc.h>
#include <mars/task.h>
#include <mars_test.h>
#include <common.h>

uint32_t aligned_num(uint32_t num, uint32_t aligned)
{
	return num + (aligned - num % aligned);
}

int mars_task_main(const struct mars_task_args *task_args)
{
	int ret, i, j;
	uint32_t start, end;
	char buf[ELEMENT_SIZE];
	static struct test_task_args_st args ALIGN128;
	uint32_t *push_ticks;
	uint32_t *pop_ticks;

	uint32_t size;
	uint32_t receivers;

	mfc_get(&args, task_args->type.u64[0], sizeof(struct test_task_args_st),
		0, 0, 0);
	mfc_write_tag_mask(1);
	mfc_read_tag_status_all();
	
	receivers = args.receivers;

	size =
	    aligned_num(TEST1_ELEMENT_COUNT * receivers,
			128) * sizeof(uint32_t);
	pop_ticks = (uint32_t *) memalign(128, size);
	push_ticks = (uint32_t *) memalign(128, size);
	if (!pop_ticks || !push_ticks) {
		return 1;
	}

	/*pre-fill queue if required by test scenareo */
	for (i = 0; i < QUEUE_PRE_CONDITION; i++) {
		ret = mars_task_queue_push(args.queue_ea, buf);
	}

	for (i = 0; i < TEST1_ELEMENT_COUNT; i++) {

		mars_test_usleep(args.tb_freq, DELAY);

		/* ensure receivers wait untill sender has finished pushing */
		ret = mars_task_barrier_notify(args.barrier1_ea);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		ret = mars_task_barrier_wait(args.barrier1_ea);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		for (j = 0; j < receivers; j++) {
			start = mars_task_get_ticks();
			ret = mars_task_queue_push(args.queue_ea, buf);
			end = mars_task_get_ticks();
			push_ticks[i * receivers + j] = end - start;
		}

		/* wait at barrier untill receivers have finished poping */
		ret = mars_task_barrier_notify(args.barrier2_ea);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		ret = mars_task_barrier_wait(args.barrier2_ea);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		for (j = 0; j < receivers; j++) {
			pop_ticks[i * receivers + j] =
			    mars_test_int32_get(args.shared_resources_ea[j]);
		}

	}

	mfc_put(push_ticks, args.ticks1_ea, size, 0, 0, 0);
	mfc_write_tag_mask(1);
	mfc_read_tag_status_all();

	mfc_put(pop_ticks, args.ticks2_ea, size, 0, 0, 0);
	mfc_write_tag_mask(1);
	mfc_read_tag_status_all();

	free(pop_ticks);
	free(push_ticks);

	return 0;
}
