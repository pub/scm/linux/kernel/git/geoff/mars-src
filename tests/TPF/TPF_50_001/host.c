/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>
#include "common.h"
extern spe_program_handle_t mpu1_prog, mpu2_prog ALIGN128;

static struct mars_task_args task_args;

int test1(void)
{
	int exit_code, ret;
	int32_t acquire[LOOP_COUNT] ALIGN128;
	int32_t release[LOOP_COUNT] ALIGN128;

	struct test_task_args_st args ALIGN128;

	uint64_t semaphore ALIGN128;
	int spe_cnt = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);

	static struct mars_context *mars ALIGN128;
	static struct mars_task_id task_id ALIGN128;

	ret = mars_context_create(&mars, spe_cnt, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_semaphore_create(mars, &semaphore, 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(mars, &task_id, "50001", mpu1_prog.elf_image,
			       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	args.semaphore_ea = semaphore;
	args.ticks1_ea = mars_ptr_to_ea(acquire);
	args.ticks2_ea = mars_ptr_to_ea(release);
	task_args.type.u64[0] = mars_ptr_to_ea(&args);

	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_wait(&task_id, &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	MARS_TEST_ASSERT_EQUAL(exit_code, 0);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_semaphore_destroy(semaphore);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	mars_test_print_statistics(acquire, LOOP_COUNT,
				   "single semaphore: acquire");
	mars_test_print_statistics(release, LOOP_COUNT,
				   "single semaphore: release");

	return 0;
}

int test2(void)
{
	static struct mars_context *mars;
	static struct mars_task_id task_id;
	int exit_code, ret;
	uint64_t semaphore ALIGN128;
	int32_t acquire[LOOP_COUNT], release[LOOP_COUNT];

	struct test_task_args_st args ALIGN128;
	int spe_cnt = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);

	ret = mars_context_create(&mars, spe_cnt, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_semaphore_create(mars, &semaphore, 50);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(mars, &task_id, "50001", mpu2_prog.elf_image,
			       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	args.semaphore_ea = semaphore;
	args.ticks1_ea = mars_ptr_to_ea(acquire);
	args.ticks2_ea = mars_ptr_to_ea(release);
	task_args.type.u64[0] = mars_ptr_to_ea(&args);

	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_wait(&task_id, &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	MARS_TEST_ASSERT_EQUAL(exit_code, 0);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_semaphore_destroy(semaphore);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	mars_test_print_statistics(acquire, LOOP_COUNT,
				   "max_wait semaphore: acquire");
	mars_test_print_statistics(release, LOOP_COUNT,
				   "max_wait semaphore: release");
	return 0;
}

int main(void)
{
	int ret;
	ret = test1();
	ret = test2();

	return 0;
}
