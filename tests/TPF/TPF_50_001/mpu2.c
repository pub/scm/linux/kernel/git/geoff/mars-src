/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include "mars_task_test.h"
#include "common.h"

int mars_task_main(const struct mars_task_args *task_args)
{
	int i, j, ret, acquire[LOOP_COUNT], release[LOOP_COUNT];
	int t1, t2, t3, t4;
	struct test_task_args_st args ALIGN128;

	mfc_get(&args, task_args->type.u64[0], sizeof(struct test_task_args_st),
		0, 0, 0);
	mfc_write_tag_mask(1);
	mfc_read_tag_status_all();

	for (i = 0; i < LOOP_COUNT; i++) {
		t1 = mars_task_get_ticks();
		for (j = 0; j < 50; j++) {
			ret = mars_task_semaphore_acquire(args.semaphore_ea);
		}
		t2 = mars_task_get_ticks();
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		t3 = mars_task_get_ticks();
		for (j = 0; j < 50; j++) {
			ret = mars_task_semaphore_release(args.semaphore_ea);
		}
		t4 = mars_task_get_ticks();
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		acquire[i] = (t2 - t1) / 50;
		release[i] = (t4 - t3) / 50;
	}

	mfc_put(acquire, args.ticks1_ea, LOOP_COUNT * 4, 0, 0, 0);
	mfc_write_tag_mask(1);
	mfc_read_tag_status_all();

	mfc_put(release, args.ticks2_ea, LOOP_COUNT * 4, 0, 0, 0);
	mfc_write_tag_mask(1);
	mfc_read_tag_status_all();

	mars_task_exit(0);

	return 0;
}
