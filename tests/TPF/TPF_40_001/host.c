/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>
#include "common.h"

extern spe_program_handle_t mpu1_prog, mpu2_prog, mpu3_prog;

void test1()
{
	int ret, i;
	uint32_t start, end;
	int32_t push_ticks[TEST1_ELEMENT_COUNT];
	static int32_t pop_ticks[TEST1_ELEMENT_COUNT] ALIGN128;
	uint32_t sender_to_receiver = UINT32_MAX;
	uint32_t receiver_to_sender = UINT32_MAX;
	char buf[ELEMENT_SIZE];
	struct mars_context *mars_ctx;
	struct mars_task_id task_id;
	struct mars_task_args task_args;
	uint64_t queue;
	static struct test_task_args_st args ALIGN128;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_create(mars_ctx, &queue,
		ELEMENT_SIZE, QUEUE_DEPTH, MARS_TASK_QUEUE_HOST_TO_MPU);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(mars_ctx, &task_id, NULL,
		mpu1_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	args.queue_ea = queue;
	args.sender_to_receiver_ea = mars_ptr_to_ea(&sender_to_receiver);
	args.receiver_to_sender_ea = mars_ptr_to_ea(&receiver_to_sender);
	args.ticks1_ea = mars_ptr_to_ea(pop_ticks);
	task_args.type.u64[0] = mars_ptr_to_ea(&args);

	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < TEST1_ELEMENT_COUNT; i++) {
		start = mars_task_get_ticks();
		ret = mars_task_queue_push(queue, buf);
		end = mars_task_get_ticks();
		push_ticks[i] = end - start;
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		mars_test_counter_set(&sender_to_receiver, i);
		mars_test_counter_wait(&receiver_to_sender, i);
	}

	ret = mars_task_wait(&task_id, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_destroy(queue);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	mars_test_print_statistics(push_ticks, TEST1_ELEMENT_COUNT, "push");
	mars_test_print_statistics(pop_ticks, TEST1_ELEMENT_COUNT, "pop");
}

void test2()
{
	int ret, i;
	uint32_t start, end;
	int32_t ticks[TEST2_ELEMENT_COUNT];
	uint32_t sender_to_receiver = UINT32_MAX;
	uint32_t receiver_to_sender = UINT32_MAX;
	char buf[ELEMENT_SIZE];
	struct mars_context *mars_ctx;
	struct mars_task_id task_id;
	struct mars_task_args task_args;
	uint64_t queue;
	static struct test_task_args_st args ALIGN128;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_create(mars_ctx, &queue,
		ELEMENT_SIZE, QUEUE_DEPTH, MARS_TASK_QUEUE_HOST_TO_MPU);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(mars_ctx, &task_id, NULL,
		mpu2_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	args.queue_ea = queue;
	args.sender_to_receiver_ea = mars_ptr_to_ea(&sender_to_receiver);
	args.receiver_to_sender_ea = mars_ptr_to_ea(&receiver_to_sender);
	args.shared_resource_ea = mars_ptr_to_ea(&end);
	task_args.type.u64[0] = mars_ptr_to_ea(&args);

	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	suppress_page_faults(queue, buf, 1);

	for (i = 0; i < TEST2_ELEMENT_COUNT; i++) {
		mars_test_counter_wait(&receiver_to_sender, i * 2);
		mars_test_counter_set(&sender_to_receiver, i * 2);

		/* wait for the MARS task blocked */
		usleep(DELAY);

		start = mars_task_get_ticks();
		ret = mars_task_queue_push(queue, buf);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		mars_test_counter_wait(&receiver_to_sender, i * 2 + 1);
		ticks[i] = end - start;
		mars_test_counter_set(&sender_to_receiver, i * 2 + 1);
	}

	ret = mars_task_wait(&task_id, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_destroy(queue);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	mars_test_print_statistics(
		ticks, TEST2_ELEMENT_COUNT, "beginning of push - end of pop");
}

void test3()
{
	int ret, i;
	uint32_t start, end;
	int32_t ticks[TEST3_ELEMENT_COUNT];
	uint32_t sender_to_receiver = UINT32_MAX;
	uint32_t receiver_to_sender = UINT32_MAX;
	char buf[ELEMENT_SIZE];
	struct mars_context *mars_ctx;
	struct mars_task_id task_id;
	struct mars_task_args task_args;
	uint64_t queue;
	static struct test_task_args_st args ALIGN128;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_create(mars_ctx, &queue,
		ELEMENT_SIZE, QUEUE_DEPTH, MARS_TASK_QUEUE_HOST_TO_MPU);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(mars_ctx, &task_id, NULL,
		mpu3_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	args.queue_ea = queue;
	args.sender_to_receiver_ea = mars_ptr_to_ea(&sender_to_receiver);
	args.receiver_to_sender_ea = mars_ptr_to_ea(&receiver_to_sender);
	args.shared_resource_ea = mars_ptr_to_ea(&end);
	task_args.type.u64[0] = mars_ptr_to_ea(&args);

	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	suppress_page_faults(queue, buf, 1);

	for (i = 0; i < TEST3_ELEMENT_COUNT; i++) {
		mars_test_counter_wait(&receiver_to_sender, i * 2);
		mars_test_counter_set(&sender_to_receiver, i * 2);

		/* wait for the MARS task blocked */
		usleep(DELAY);

		ret = mars_task_queue_push(queue, buf);
		start = mars_task_get_ticks();
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		mars_test_counter_wait(&receiver_to_sender, i * 2 + 1);
		ticks[i] = end - start;
		mars_test_counter_set(&sender_to_receiver, i * 2 + 1);
	}

	ret = mars_task_wait(&task_id, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_destroy(queue);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	mars_test_print_statistics(
		ticks, TEST3_ELEMENT_COUNT, "end of push - end of pop");
}
int main(int argc, char *argv[])
{
	MARS_TEST_PERF_INIT();

	test1();
	test2();
	test3();

	return 0;
}
