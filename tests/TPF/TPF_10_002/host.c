/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdio.h>
#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>

#define TASK_MAX	512

extern spe_program_handle_t mpu_prog;

uint32_t test(int task, int *schedule, int *wait)
{
	int32_t ret, i;
	int32_t start, end;
	struct mars_context *mars_ctx;
	struct mars_task_id task_id[TASK_MAX];
	struct mars_task_args task_args;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < task; i++) {
		ret = mars_task_create(mars_ctx, &task_id[i], NULL,
				       mpu_prog.elf_image,
				       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	for (i = 0; i < task; i++) {
		start = mars_task_get_ticks();
		ret = mars_task_schedule(&task_id[i], &task_args, 0);
		end = mars_task_get_ticks();
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		schedule[i] = end - start;
	}

	for (i = 0; i < task; i++) {
		start = mars_task_get_ticks();
		ret = mars_task_wait(&task_id[i], NULL);
		end = mars_task_get_ticks();
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		wait[i] = end - start;
	}

	for (i = 0; i < task; i++) {
		ret = mars_task_destroy(&task_id[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

int main(void)
{
	int task = TASK_MAX;
	int32_t schedule[TASK_MAX], wait[TASK_MAX];

	MARS_TEST_PERF_INIT();

	test(task, schedule, wait);

	mars_test_print_statistics(schedule, task, "task create");
	mars_test_print_statistics(wait, task, "task destroy");

	return 0;
}
