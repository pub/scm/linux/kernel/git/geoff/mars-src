/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>
#include <common.h>
#include <stdio.h>

extern spe_program_handle_t mpu1_prog, mpu2_prog, mpu3_prog;

struct thread_arg_st {
	int32_t *ticks1;
	int32_t *ticks2;
	struct mars_context *mars_ctx;
	uint64_t barrier;
};

void *test1_thread(void *arg)
{
	int i, ret;
	uint32_t start, end;
	struct thread_arg_st *thread_arg = arg;
	int32_t *push_ticks = thread_arg->ticks1;
	int32_t *pop_ticks = thread_arg->ticks2;
	uint32_t sender_to_receiver = UINT32_MAX;
	uint32_t receiver_to_sender = UINT32_MAX;
	char buf[ELEMENT_SIZE];
	struct mars_context *mars_ctx = thread_arg->mars_ctx;
	struct mars_task_id task_id;
	struct mars_task_args task_args;
	uint64_t queue;
	static __thread struct test_task_args_st args ALIGN128;

	ret = mars_task_queue_create(mars_ctx, &queue,
		ELEMENT_SIZE, QUEUE_DEPTH, MARS_TASK_QUEUE_MPU_TO_HOST);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(mars_ctx, &task_id, NULL,
		mpu1_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	args.queue_ea = queue;
	args.barrier_ea = thread_arg->barrier;
	args.sender_to_receiver_ea = mars_ptr_to_ea(&sender_to_receiver);
	args.receiver_to_sender_ea = mars_ptr_to_ea(&receiver_to_sender);
	args.ticks1_ea = mars_ptr_to_ea(push_ticks);
	task_args.type.u64[0] = mars_ptr_to_ea(&args);

	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < TEST1_ELEMENT_COUNT; i++) {
		mars_test_counter_wait(&sender_to_receiver, i);
		start = mars_task_get_ticks();
		ret = mars_task_queue_pop(queue, buf);
		end = mars_task_get_ticks();
		pop_ticks[i] = end - start;
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		mars_test_counter_set(&receiver_to_sender, i);
	}

	ret = mars_task_wait(&task_id, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_destroy(queue);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return NULL;
}

void test1(int mpu, pthread_t *threads, struct thread_arg_st *thread_args)
{
	int ret, i;
	size_t size;
	int32_t *push_ticks, *pop_ticks;
	struct mars_context *mars_ctx;
	uint64_t barrier;

	size = sizeof(int32_t) * TEST1_ELEMENT_COUNT * mpu;

	ret = posix_memalign((void **)&push_ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	pop_ticks = (int32_t *)malloc(size);
	MARS_TEST_ASSERT(pop_ticks != NULL);

	ret = mars_context_create(&mars_ctx, 0, 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_barrier_create(mars_ctx, &barrier, mpu);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < mpu; i++) {
		thread_args[i].ticks1 = &push_ticks[TEST1_ELEMENT_COUNT * i];
		thread_args[i].ticks2 = &pop_ticks[TEST1_ELEMENT_COUNT * i];
		thread_args[i].mars_ctx = mars_ctx;
		thread_args[i].barrier = barrier;
		ret = pthread_create(&threads[i], NULL,
			test1_thread, &thread_args[i]);
		MARS_TEST_ASSERT_EQUAL(ret, 0);
	}

	for (i = 0; i < mpu; i++) {
		ret = pthread_join(threads[i], NULL);
		MARS_TEST_ASSERT_EQUAL(ret, 0);
	}

	ret = mars_task_barrier_destroy(barrier);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	size = TEST1_ELEMENT_COUNT * mpu;
	mars_test_print_statistics(push_ticks, size, "push");
	mars_test_print_statistics(pop_ticks, size, "pop");

	free(pop_ticks);
	free(push_ticks);
}

void *test2_thread(void *arg)
{
	int i, ret;
	int32_t start, end;
	struct thread_arg_st *thread_arg = arg;
	int32_t *ticks = thread_arg->ticks1;
	uint32_t sender_to_receiver = UINT32_MAX;
	uint32_t receiver_to_sender = UINT32_MAX;
	char buf[ELEMENT_SIZE];
	struct mars_context *mars_ctx = thread_arg->mars_ctx;
	struct mars_task_id task_id;
	struct mars_task_args task_args;
	uint64_t queue;
	static __thread struct test_task_args_st args ALIGN128;

	ret = mars_test_get_timebase_freq(&args.tb_freq);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_task_queue_create(mars_ctx, &queue,
		ELEMENT_SIZE, QUEUE_DEPTH, MARS_TASK_QUEUE_MPU_TO_HOST);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(mars_ctx, &task_id, NULL,
		mpu2_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	args.queue_ea = queue;
	args.barrier_ea = thread_arg->barrier;
	args.sender_to_receiver_ea = mars_ptr_to_ea(&sender_to_receiver);
	args.receiver_to_sender_ea = mars_ptr_to_ea(&receiver_to_sender);
	args.shared_resource_ea = mars_ptr_to_ea(&end);
	task_args.type.u64[0] = mars_ptr_to_ea(&args);

	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < TEST2_ELEMENT_COUNT; i++) {
		mars_test_counter_wait(&sender_to_receiver, i * 2);
		mars_test_counter_set(&receiver_to_sender, i * 2);

		usleep(DELAY);

		start = mars_task_get_ticks();
		ret = mars_task_queue_pop(queue, buf);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		mars_test_counter_wait(&sender_to_receiver, i * 2 + 1);
		ticks[i] = end - start;
		mars_test_counter_set(&receiver_to_sender, i * 2 + 1);
	}

	ret = mars_task_wait(&task_id, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_destroy(queue);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return NULL;
}

void test2(int mpu, pthread_t *threads, struct thread_arg_st *thread_args)
{
	int ret, i;
	size_t size;
	int32_t *ticks;
	struct mars_context *mars_ctx;
	uint64_t barrier;

	size = sizeof(int32_t) * TEST2_ELEMENT_COUNT * mpu;
	ticks = (int32_t *)malloc(size);
	MARS_TEST_ASSERT(ticks != NULL);

	ret = mars_context_create(&mars_ctx, 0, 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_barrier_create(mars_ctx, &barrier, mpu);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < mpu; i++) {
		thread_args[i].ticks1 = &ticks[TEST2_ELEMENT_COUNT * i];
		thread_args[i].mars_ctx = mars_ctx;
		thread_args[i].barrier = barrier;
		ret = pthread_create(&threads[i], NULL,
			test2_thread, &thread_args[i]);
		MARS_TEST_ASSERT_EQUAL(ret, 0);
	}

	for (i = 0; i < mpu; i++) {
		ret = pthread_join(threads[i], NULL);
		MARS_TEST_ASSERT_EQUAL(ret, 0);
	}

	ret = mars_task_barrier_destroy(barrier);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	size = TEST2_ELEMENT_COUNT * mpu;
	mars_test_print_statistics(
		ticks, size, "beginning of pop - end of push");

	free(ticks);
}

void *test3_thread(void *arg)
{
	int i, ret;
	int32_t start, end;
	struct thread_arg_st *thread_arg = arg;
	int32_t *ticks = thread_arg->ticks1;
	uint32_t sender_to_receiver = UINT32_MAX;
	uint32_t receiver_to_sender = UINT32_MAX;
	char buf[ELEMENT_SIZE];
	struct mars_context *mars_ctx = thread_arg->mars_ctx;
	struct mars_task_id task_id;
	struct mars_task_args task_args;
	uint64_t queue;
	static __thread struct test_task_args_st args ALIGN128;

	ret = mars_test_get_timebase_freq(&args.tb_freq);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_task_queue_create(mars_ctx, &queue,
		ELEMENT_SIZE, QUEUE_DEPTH, MARS_TASK_QUEUE_MPU_TO_HOST);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(mars_ctx, &task_id, NULL,
		mpu3_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	args.queue_ea = queue;
	args.barrier_ea = thread_arg->barrier;
	args.sender_to_receiver_ea = mars_ptr_to_ea(&sender_to_receiver);
	args.receiver_to_sender_ea = mars_ptr_to_ea(&receiver_to_sender);
	args.shared_resource_ea = mars_ptr_to_ea(&end);
	task_args.type.u64[0] = mars_ptr_to_ea(&args);

	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < TEST3_ELEMENT_COUNT; i++) {
		mars_test_counter_wait(&sender_to_receiver, i * 2);
		mars_test_counter_set(&receiver_to_sender, i * 2);

		usleep(DELAY);

		ret = mars_task_queue_pop(queue, buf);
		start = mars_task_get_ticks();
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		mars_test_counter_wait(&sender_to_receiver, i * 2 + 1);
		ticks[i] = end - start;
		mars_test_counter_set(&receiver_to_sender, i * 2 + 1);
	}

	ret = mars_task_wait(&task_id, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_destroy(queue);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return NULL;
}

void test3(int mpu, pthread_t *threads, struct thread_arg_st *thread_args)
{
	int ret, i;
	size_t size;
	int32_t *ticks;
	struct mars_context *mars_ctx;
	uint64_t barrier;

	size = sizeof(int32_t) * TEST3_ELEMENT_COUNT * mpu;
	ticks = (int32_t *)malloc(size);
	MARS_TEST_ASSERT(ticks != NULL);

	ret = mars_context_create(&mars_ctx, 0, 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_barrier_create(mars_ctx, &barrier, mpu);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < mpu; i++) {
		thread_args[i].ticks1 = &ticks[TEST3_ELEMENT_COUNT * i];
		thread_args[i].mars_ctx = mars_ctx;
		thread_args[i].barrier = barrier;
		ret = pthread_create(&threads[i], NULL,
			test3_thread, &thread_args[i]);
		MARS_TEST_ASSERT_EQUAL(ret, 0);
	}

	for (i = 0; i < mpu; i++) {
		ret = pthread_join(threads[i], NULL);
		MARS_TEST_ASSERT_EQUAL(ret, 0);
	}

	ret = mars_task_barrier_destroy(barrier);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	size = TEST3_ELEMENT_COUNT * mpu;
	mars_test_print_statistics(
		ticks, size, "end of pop - end of push");

	free(ticks);
}

int main(int argc, char *argv[])
{
	int spe, mpu;
	size_t size;
	pthread_t *threads;
	struct thread_arg_st *thread_args;

	spe = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);

	mpu = MAX_MPUS;

	threads = (pthread_t *)malloc(sizeof(pthread_t) * mpu);
	MARS_TEST_ASSERT(threads != NULL);

	size = sizeof(struct thread_arg_st) * mpu;
	thread_args = (struct thread_arg_st *)malloc(size);
	MARS_TEST_ASSERT(thread_args != NULL);

	MARS_TEST_PERF_INIT();

	test1(mpu, threads, thread_args);
	test2(mpu, threads, thread_args);
	test3(mpu, threads, thread_args);

	free(thread_args);
	free(threads);

	return 0;
}
