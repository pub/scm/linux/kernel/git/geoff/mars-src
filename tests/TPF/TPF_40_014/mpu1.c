/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <mars_test.h>
#include "common.h"
#include <stdio.h>

int mars_task_main(const struct mars_task_args *task_args)
{
	int ret, i;
	uint32_t start, end;
	char buf[ELEMENT_SIZE];
	static struct test_task_args_st args ALIGN128;
	static int32_t push_ticks[TEST1_ELEMENT_COUNT] ALIGN128;

	mfc_get(&args, task_args->type.u64[0],
		sizeof(struct test_task_args_st), 0, 0, 0);
	mfc_write_tag_mask(1);
	mfc_read_tag_status_all();

	for (i = 0; i < QUEUE_DEPTH - 1; i++) {
		ret = mars_task_queue_push(args.queue_ea, buf);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_task_barrier_notify(args.barrier_ea);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	ret = mars_task_barrier_wait(args.barrier_ea);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < TEST1_ELEMENT_COUNT; i++) {
		start = mars_task_get_ticks();
		ret = mars_task_queue_push(args.queue_ea, buf);
		end = mars_task_get_ticks();
		push_ticks[i] = end - start;
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		mars_test_counter_set(args.sender_to_receiver_ea, i);
		mars_test_counter_wait(args.receiver_to_sender_ea, i);
	}

	mfc_put(push_ticks, args.ticks1_ea, sizeof(push_ticks), 0, 0, 0);
	mfc_write_tag_mask(1);
	mfc_read_tag_status_all();

	return 0;
}
