/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>
#include <unistd.h>
#include <common.h>

extern spe_program_handle_t mpu1_prog;

struct thread_arg_st {
	int32_t *ticks1;
	int32_t *ticks2;
	int32_t *ticks3;
	struct mars_context *mars_ctx;
};

void *test_thread(void *arg)
{
	int ret, i;
	struct thread_arg_st *thread_arg = arg;
	int32_t *start1_ticks = thread_arg->ticks1;
	int32_t *start2_ticks = thread_arg->ticks2;
	int32_t *end_ticks = thread_arg->ticks3;
	struct mars_context *mars_ctx = thread_arg->mars_ctx;
	struct mars_task_id task_id;
	struct mars_task_args task_args;
	static __thread struct test_task_args_st args ALIGN128;

	uint32_t sender_to_receiver = UINT32_MAX;
	uint32_t receiver_to_sender = UINT32_MAX;

	ret = mars_task_create(mars_ctx, &task_id, NULL,
			       mpu1_prog.elf_image,
			       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	args.sender_to_receiver_ea = mars_ptr_to_ea(&sender_to_receiver);
	args.receiver_to_sender_ea = mars_ptr_to_ea(&receiver_to_sender);
	args.ticks_ea = mars_ptr_to_ea(end_ticks);
	task_args.type.u64[0] = mars_ptr_to_ea(&args);

	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < TEST1_ELEMENT_COUNT; i++) {
		mars_test_counter_set(&sender_to_receiver, i);
		mars_test_counter_wait(&receiver_to_sender, i);
		usleep(DELAY);	/*give mpu time to block */
		start1_ticks[i] = mars_task_get_ticks();
		mars_task_signal_send(&task_id);
		start2_ticks[i] = mars_task_get_ticks();
	}

	ret = mars_task_wait(&task_id, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return NULL;
}

void test1(int mpu, pthread_t * threads, struct thread_arg_st *thread_args)
{
	int ret, i;
	size_t size;
	int32_t *start1_ticks, *start2_ticks, *end_ticks; int32_t *ticks;
	char text[64];
	struct mars_context *mars_ctx;

	size = sizeof(uint32_t) * TEST1_ELEMENT_COUNT * mpu;

	ret = posix_memalign((void **)&start1_ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = posix_memalign((void **)&start2_ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = posix_memalign((void **)&end_ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = posix_memalign((void **)&ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_context_create(&mars_ctx, 0, 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < mpu; i++) {
		thread_args[i].ticks1 = &start1_ticks[TEST1_ELEMENT_COUNT * i];
		thread_args[i].ticks2 = &start2_ticks[TEST1_ELEMENT_COUNT * i];
		thread_args[i].ticks3 = &end_ticks[TEST1_ELEMENT_COUNT * i];
		thread_args[i].mars_ctx = mars_ctx;
		ret = pthread_create(&threads[i], NULL, test_thread,
				     &thread_args[i]);
		MARS_TEST_ASSERT_EQUAL(ret, 0);
	}

	for (i = 0; i < mpu; i++) {
		ret = pthread_join(threads[i], NULL);
		MARS_TEST_ASSERT_EQUAL(ret, 0);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	size = TEST1_ELEMENT_COUNT * mpu;

	for (i = 0; i < size; i++) {
		ticks[i] = start2_ticks[i] - start1_ticks[i];
	}
	sprintf(text, "signal host-mpu: signal (%d mpu)", mpu);
	mars_test_print_statistics(ticks, size, text);

	for (i = 0; i < size; i++) {
		ticks[i] = end_ticks[i] - start1_ticks[i];
	}
	sprintf(text, "signal host-mpu: start signal - end wait (%d mpu)", mpu);
	mars_test_print_statistics(ticks, size, text);

	for (i = 0; i < size; i++) {
		ticks[i] = end_ticks[i] - start2_ticks[i];
	}
	sprintf(text, "signal host-mpu: end signal - end wait (%d mpu)", mpu);
	mars_test_print_statistics(ticks, size, text);

	free(start1_ticks);
	free(start2_ticks);
	free(end_ticks);
	free(ticks);
}

int main(int argc, char *argv[])
{
	int mpu,spe;
	size_t size;
	pthread_t *threads;
	struct thread_arg_st *thread_args;

	spe = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);

	for (mpu = MIN_MPUS; mpu <= MAX_MPUS; mpu++) {

		threads = (pthread_t *) malloc(sizeof(pthread_t) * mpu);
		MARS_TEST_ASSERT(threads != NULL);

		size = sizeof(struct thread_arg_st) * mpu;
		thread_args = (struct thread_arg_st *)malloc(size);
		MARS_TEST_ASSERT(thread_args != NULL);

		MARS_TEST_PERF_INIT();

		test1(mpu, threads, thread_args);
		free(thread_args);
		free(threads);
	}

	return 0;
}
