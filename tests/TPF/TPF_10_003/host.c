/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdio.h>
#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>

#define	NUM_RETRY	10
#define TASK_INITIAL	4
#define TASK_MAX	512

extern spe_program_handle_t mpu_prog;

uint32_t test(int yield, int task)
{
	int ret, i;
	uint32_t start, end;
	struct mars_context *mars_ctx;
	struct mars_task_id task_id[TASK_MAX];
	struct mars_task_args task_args;

	task_args.type.u32[0] = yield;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < task; i++) {
		ret = mars_task_create(mars_ctx, &task_id[i], NULL,
			mpu_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	start = mars_task_get_ticks();

	for (i = 0; i < task; i++) {
		ret = mars_task_schedule(&task_id[i], &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	for (i = 0; i < task; i++) {
		ret = mars_task_wait(&task_id[i], NULL);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	end = mars_task_get_ticks();

	for (i = 0; i < task; i++) {
		ret = mars_task_destroy(&task_id[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return end - start;
}

int main(void)
{
	int i, yield, task;

	MARS_TEST_PERF_INIT();

	for (yield = 0; yield <= 4; yield++) {
		for (task = TASK_INITIAL; task <= TASK_MAX; task *= 2) {
			uint32_t average = 0;
			char buf[256];

			for (i = 0; i < NUM_RETRY; i++)
				average += test(task, yield);
			average /= NUM_RETRY;

			sprintf(buf, "yield:%d task:%d ", yield, task);
			MARS_TEST_PERF_RESULT_PRINT(buf, average);
		}
	}

	return 0;
}

