/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>
#include <common.h>

extern spe_program_handle_t mpu1_prog, mpu2_prog, mpu3_prog;

uint32_t sender_to_receiver = UINT32_MAX;
uint32_t receiver_to_sender = UINT32_MAX;
uint32_t shared_resource = UINT32_MAX;

struct thread_arg_st {
	uint32_t id;
	uint32_t pushers;
	uint64_t queue;
	int32_t *ticks1;
	int32_t *ticks2;
	pthread_barrier_t *barrier;
};

void *test1_thread(void *arg)
{
	int i, ret;
	int32_t start, end;
	struct thread_arg_st *thread_arg = arg;
	int32_t *push_ticks = thread_arg->ticks1;
	int32_t *pop_ticks = thread_arg->ticks2;
	char buf[ELEMENT_SIZE];

	for (i = 0; i < TEST1_ELEMENT_COUNT; i++) {
		/*stagger pushes starting with thread id 0*/
		mars_test_counter_wait(&sender_to_receiver,
				       (i * thread_arg->pushers +
					thread_arg->id) * 2);
		mars_test_counter_set(&receiver_to_sender,
				      (i * thread_arg->pushers +
				       thread_arg->id) * 2);

#ifdef QUEUE_EMPTY
		usleep(DELAY);
#endif

		start = mars_task_get_ticks();
		ret = mars_task_queue_pop(thread_arg->queue, buf);
		end = mars_task_get_ticks();
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		mars_test_counter_wait(&sender_to_receiver,
				       (i * thread_arg->pushers +
					thread_arg->id) * 2 + 1);
		mars_test_counter_set(&receiver_to_sender,
				      (i * thread_arg->pushers +
				       thread_arg->id) * 2 + 1);

		pop_ticks[i] = shared_resource;
		push_ticks[i] = end - start;

	}
	return NULL;
}

void test1(uint32_t pushers)
{
	int ret, i;
	size_t size;
	int32_t *push_ticks, *pop_ticks;

	struct thread_arg_st thread_arg[pushers];
	static __thread struct test_task_args_st args ALIGN128;
	struct mars_context *mars_ctx;
	struct mars_task_id task_id;
	struct mars_task_args task_args;
	uint64_t queue;
	pthread_t *threads;

	threads = (pthread_t *) malloc(sizeof(pthread_t) * pushers);
	MARS_TEST_ASSERT(threads != NULL);

	size = sizeof(uint32_t) * TEST1_ELEMENT_COUNT * pushers;
	push_ticks = (int32_t *) malloc(size);
	MARS_TEST_ASSERT(push_ticks != NULL);
	pop_ticks = (int32_t *) malloc(size);
	MARS_TEST_ASSERT(push_ticks != NULL);

	ret = posix_memalign((void **)&pop_ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_test_get_timebase_freq(&args.tb_freq);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_create(&mars_ctx, 0, 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_create(mars_ctx, &queue, ELEMENT_SIZE, QUEUE_DEPTH,
				   MARS_TASK_QUEUE_MPU_TO_HOST);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(mars_ctx, &task_id, NULL, mpu1_prog.elf_image,
			     MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	args.queue_ea = queue;
	args.shared_resource_ea = mars_ptr_to_ea(&shared_resource);
	args.pushers = pushers;
	args.sender_to_receiver_ea = mars_ptr_to_ea(&sender_to_receiver);
	args.receiver_to_sender_ea = mars_ptr_to_ea(&receiver_to_sender);
	task_args.type.u64[0] = mars_ptr_to_ea(&args);

	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < pushers; i++) {
		thread_arg[i].id = i;
		thread_arg[i].pushers = pushers;
		thread_arg[i].queue = queue;
		thread_arg[i].ticks1 = &push_ticks[TEST1_ELEMENT_COUNT * i];
		thread_arg[i].ticks2 = &pop_ticks[TEST1_ELEMENT_COUNT * i];
		ret =
		    pthread_create(&threads[i], NULL, test1_thread,
				   &thread_arg[i]);
		MARS_TEST_ASSERT_EQUAL(ret, 0);
	}

	for (i = 0; i < pushers; i++) {
		ret = pthread_join(threads[i], NULL);
		MARS_TEST_ASSERT_EQUAL(ret, 0);
	}

	ret = mars_task_wait(&task_id, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_destroy(queue);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	size = TEST1_ELEMENT_COUNT * pushers;
	mars_test_print_statistics(push_ticks, size, "push");
	mars_test_print_statistics(pop_ticks, size, "pop");

	free(threads);
	free(pop_ticks);
	free(push_ticks);
}

#ifndef QUEUE_INTERMEDIATE

void *test2_thread(void *arg)
{
	int i, ret;
	int32_t host_ticks, mpu_ticks;
	struct thread_arg_st *thread_arg = arg;
	int32_t *push_ticks = thread_arg->ticks1;
	char buf[ELEMENT_SIZE];

	for (i = 0; i < TEST2_ELEMENT_COUNT; i++) {

		/*stagger pushes to give mpu time to block on pop*/
		mars_test_counter_wait(&sender_to_receiver,
				       (i * thread_arg->pushers +
					thread_arg->id) * 2);
		mars_test_counter_set(&receiver_to_sender,
				      (i * thread_arg->pushers +
				       thread_arg->id) * 2);

#ifdef QUEUE_FULL
		usleep(5000);	/*block on push*/
		host_ticks = mars_task_get_ticks();	/*start ticks*/
#endif
		ret = mars_task_queue_pop(thread_arg->queue, buf);
#ifdef QUEUE_EMPTY
		host_ticks = mars_task_get_ticks();	/*end ticks*/
#endif
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		mars_test_counter_wait(&sender_to_receiver,
				       (i * thread_arg->pushers +
					thread_arg->id) * 2 + 1);
		mars_test_counter_set(&receiver_to_sender,
				      (i * thread_arg->pushers +
				       thread_arg->id) * 2 + 1);

		mpu_ticks = shared_resource;

#ifdef QUEUE_FULL
		push_ticks[i] = mpu_ticks - host_ticks;	/*end - start*/
#endif
#ifdef QUEUE_EMPTY
		push_ticks[i] = host_ticks - mpu_ticks;	/*end -start*/
#endif
	}
	return NULL;
}

void test2(uint32_t pushers)
{
	int ret, i;
	size_t size;
	int32_t *ticks;

	struct thread_arg_st thread_arg[pushers];
	static __thread struct test_task_args_st args ALIGN128;
	struct mars_context *mars_ctx;
	struct mars_task_id task_id;
	struct mars_task_args task_args;
	uint64_t queue;
	pthread_t *threads;

	threads = (pthread_t *) malloc(sizeof(pthread_t) * pushers);
	MARS_TEST_ASSERT(threads != NULL);

	size = sizeof(uint32_t) * TEST1_ELEMENT_COUNT * pushers;
	ticks = (int32_t *) malloc(size);
	MARS_TEST_ASSERT(ticks != NULL);

	ret = mars_test_get_timebase_freq(&args.tb_freq);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_create(&mars_ctx, 0, 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_create(mars_ctx, &queue, ELEMENT_SIZE, QUEUE_DEPTH,
				   MARS_TASK_QUEUE_MPU_TO_HOST);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(mars_ctx, &task_id, NULL, mpu2_prog.elf_image,
			     MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	args.queue_ea = queue;
	args.pushers = pushers;
	args.shared_resource_ea = mars_ptr_to_ea(&shared_resource);
	args.sender_to_receiver_ea = mars_ptr_to_ea(&sender_to_receiver);
	args.receiver_to_sender_ea = mars_ptr_to_ea(&receiver_to_sender);
	task_args.type.u64[0] = mars_ptr_to_ea(&args);

	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < pushers; i++) {
		thread_arg[i].id = i;
		thread_arg[i].pushers = pushers;
		thread_arg[i].queue = queue;
		thread_arg[i].ticks1 = &ticks[TEST2_ELEMENT_COUNT * i];
		ret =
		    pthread_create(&threads[i], NULL, test2_thread,
				   &thread_arg[i]);
		MARS_TEST_ASSERT_EQUAL(ret, 0);
	}

	for (i = 0; i < pushers; i++) {
		ret = pthread_join(threads[i], NULL);
		MARS_TEST_ASSERT_EQUAL(ret, 0);
	}

	ret = mars_task_wait(&task_id, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_destroy(queue);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	size = TEST1_ELEMENT_COUNT * pushers;
#ifdef QUEUE_EMPTY
	mars_test_print_statistics(ticks, TEST2_ELEMENT_COUNT * pushers,
				   "beginning of push - end of pop");
#endif
#ifdef QUEUE_FULL
	mars_test_print_statistics(ticks, TEST2_ELEMENT_COUNT * pushers,
				   "beginning of pop - end of push");
#endif

	free(threads);
	free(ticks);
}

void *test3_thread(void *arg)
{
	int i, ret;
	int32_t host_ticks, mpu_ticks;
	struct thread_arg_st *thread_arg = arg;
	int32_t *push_ticks = thread_arg->ticks1;
	char buf[ELEMENT_SIZE];

	for (i = 0; i < TEST1_ELEMENT_COUNT; i++) {

		/*stagger pushes to give mpu time to block on pop*/
		mars_test_counter_wait(&sender_to_receiver,
				       (i * thread_arg->pushers +
					thread_arg->id) * 2);
		mars_test_counter_set(&receiver_to_sender,
				      (i * thread_arg->pushers +
				       thread_arg->id) * 2);

#ifdef QUEUE_FULL
		usleep(5000);
#endif

		ret = mars_task_queue_pop(thread_arg->queue, buf);
		host_ticks = mars_task_get_ticks();	/*end ticks for empty, but start ticks for full*/
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		mars_test_counter_wait(&sender_to_receiver,
				       (i * thread_arg->pushers +
					thread_arg->id) * 2 + 1);
		mars_test_counter_set(&receiver_to_sender,
				      (i * thread_arg->pushers +
				       thread_arg->id) * 2 + 1);

		mpu_ticks = shared_resource;

#ifdef QUEUE_FULL
		push_ticks[i] = mpu_ticks - host_ticks;
#endif
#ifdef QUEUE_EMPTY
		push_ticks[i] = host_ticks - mpu_ticks;
#endif
	}
	return NULL;
}

void test3(uint32_t pushers)
{
	int ret, i;
	size_t size;
	int32_t *ticks;

	struct thread_arg_st thread_arg[pushers];
	static __thread struct test_task_args_st args ALIGN128;
	struct mars_context *mars_ctx;
	struct mars_task_id task_id;
	struct mars_task_args task_args;
	uint64_t queue;
	pthread_t *threads;

	threads = (pthread_t *) malloc(sizeof(pthread_t) * pushers);
	MARS_TEST_ASSERT(threads != NULL);

	size = sizeof(uint32_t) * TEST1_ELEMENT_COUNT * pushers;
	ticks = (int32_t *) malloc(size);
	MARS_TEST_ASSERT(ticks != NULL);

	ret = mars_test_get_timebase_freq(&args.tb_freq);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_create(&mars_ctx, 0, 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_create(mars_ctx, &queue, ELEMENT_SIZE, QUEUE_DEPTH,
				   MARS_TASK_QUEUE_MPU_TO_HOST);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(mars_ctx, &task_id, NULL, mpu3_prog.elf_image,
			     MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	args.queue_ea = queue;
	args.pushers = pushers;
	args.shared_resource_ea = mars_ptr_to_ea(&shared_resource);
	args.sender_to_receiver_ea = mars_ptr_to_ea(&sender_to_receiver);
	args.receiver_to_sender_ea = mars_ptr_to_ea(&receiver_to_sender);
	task_args.type.u64[0] = mars_ptr_to_ea(&args);

	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < pushers; i++) {
		thread_arg[i].id = i;
		thread_arg[i].pushers = pushers;
		thread_arg[i].queue = queue;
		thread_arg[i].ticks1 = &ticks[TEST3_ELEMENT_COUNT * i];
		ret =
		    pthread_create(&threads[i], NULL, test3_thread,
				   &thread_arg[i]);
		MARS_TEST_ASSERT_EQUAL(ret, 0);
	}

	for (i = 0; i < pushers; i++) {
		ret = pthread_join(threads[i], NULL);
		MARS_TEST_ASSERT_EQUAL(ret, 0);
	}

	ret = mars_task_wait(&task_id, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_destroy(queue);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	size = TEST1_ELEMENT_COUNT * pushers;
#ifdef QUEUE_EMPTY
	mars_test_print_statistics(ticks, TEST3_ELEMENT_COUNT * pushers,
				   "end of push - end of pop");
#endif
#ifdef QUEUE_FULL
	mars_test_print_statistics(ticks, TEST3_ELEMENT_COUNT * pushers,
				   "end of pop - end of push");
#endif

	free(threads);
	free(ticks);
}
#endif

int main(int argc, char *argv[])
{
	uint32_t pushers = 6;

	test1(pushers);
#ifndef QUEUE_INTERMEDIATE
	test2(pushers);
	test3(pushers);
#endif

	return 0;
}
