/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <malloc.h>
#include <mars/task.h>
#include <mars_test.h>
#include <common.h>

int mars_task_main(const struct mars_task_args *task_args)
{
	int ret, i, id, receivers, mpu_run;
#ifndef QUEUE_INTERMEDIATE
	uint32_t reciever_time;
	char buf[ELEMENT_SIZE];
#endif
	static struct test_task_args_st args ALIGN128;

	mfc_get(&args, task_args->type.u64[0], sizeof(struct test_task_args_st),
		0, 0, 0);
	mfc_write_tag_mask(1);
	mfc_read_tag_status_all();

	id = atoi(mars_task_get_name());
	receivers = args.receivers;

	for (i = 0; i < TEST2_ELEMENT_COUNT; i++) {

		/*sync with host */
		mars_test_counter_set(args.receiver_to_sender_ea, i * 2);
		mars_test_counter_wait(args.sender_to_receiver_ea, i * 2);

		mpu_run = 0;
#ifdef QUEUE_FULL
		mars_test_int32_put(args.mpu_run_ea, mpu_run);
#endif

		ret = mars_task_barrier_notify(args.barrier1_ea);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		ret = mars_task_barrier_wait(args.barrier1_ea);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

#ifdef QUEUE_FULL
		mars_test_usleep(args.tb_freq, DELAY);
		ret = mars_task_queue_pop(args.queue_ea, buf);
		reciever_time = mars_task_get_ticks();	/*this is the end time*/
		mars_test_int32_put(args.shared_resources_ea[id],
				    reciever_time);
#endif

#ifdef QUEUE_EMPTY
		ret = mars_task_queue_pop(args.queue_ea, buf);
		reciever_time = mars_task_get_ticks();	/*this is the end time*/
		mars_test_int32_put(args.shared_resources_ea[(uint32_t) buf[0]],
				    reciever_time);
#endif

		mars_test_int32_put(args.mpu_run_ea, ++mpu_run);

		ret = mars_task_barrier_notify(args.barrier2_ea);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		ret = mars_task_barrier_wait(args.barrier2_ea);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*sync with host */
		mars_test_counter_set(args.receiver_to_sender_ea, i * 2 + 1);
		mars_test_counter_wait(args.sender_to_receiver_ea, i * 2 + 1);

	}

	return 0;
}
