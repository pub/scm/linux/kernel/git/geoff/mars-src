/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#ifndef TEST_COMMON_H
#define TEST_COMMON_H

#include <mars/task.h>
#include <mars_test.h>

#define MAX_RECEIVERS   4
#define QUEUE_DEPTH 64

#define QUEUE_PRE_CONDITION 64
#define QUEUE_FULL

#define ELEMENT_SIZE    128
#define TEST1_ELEMENT_COUNT  400
#define TEST2_ELEMENT_COUNT   400
#define TEST3_ELEMENT_COUNT   400

#define ALIGN128	__attribute__((aligned(128)))

struct test_task_args_st {
	uint64_t queue_ea;
	uint64_t sender_to_receiver_ea;
        uint64_t receiver_to_sender_ea;
        uint64_t shared_resources_ea[MAX_RECEIVERS];
	uint64_t ticks1_ea;
	uint64_t ticks2_ea;
	uint64_t barrier1_ea;
	uint64_t barrier2_ea;
	uint64_t mpu_run_ea;
	uint32_t tb_freq;
	uint32_t receivers;
	uint8_t pad[4];
};
#endif
