/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>
#include <string.h>
#include <unistd.h>
#include <common.h>

extern spe_program_handle_t mpu1_prog, mpu2_prog;

struct thread_arg_st {
	int32_t *ticks1;
	int32_t *ticks2;
	int32_t *ticks3;
	struct mars_context *mars_ctx;
};

void *test_thread(void *arg)
{
	int ret;
	struct thread_arg_st *thread_arg = arg;
	int32_t *start1_ticks = thread_arg->ticks1;
	int32_t *start2_ticks = thread_arg->ticks2;
	int32_t *end_ticks = thread_arg->ticks3;
	struct mars_context *mars_ctx = thread_arg->mars_ctx;
	struct mars_task_id task_id_send, task_id_wait;
	struct mars_task_args task_args_send, task_args_wait;
	static __thread struct test_task_args_st args ALIGN128;
	uint64_t barrier;
	uint64_t eventflag;

	ret = mars_task_create(mars_ctx, &task_id_send, NULL,
			       mpu1_prog.elf_image,
			       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(mars_ctx, &task_id_wait, NULL,
			       mpu2_prog.elf_image,
			       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_event_flag_create(mars_ctx, &eventflag,
					  MARS_TASK_EVENT_FLAG_MPU_TO_MPU,
					  MARS_TASK_EVENT_FLAG_CLEAR_AUTO);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_barrier_create(mars_ctx, &barrier, 2);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_test_get_timebase_freq(&args.tb_freq);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	args.barrier_ea = barrier;
	args.eventflag_ea = eventflag;
	args.ticks1_ea = mars_ptr_to_ea(start1_ticks);
	args.ticks2_ea = mars_ptr_to_ea(start2_ticks);
	args.ticks3_ea = mars_ptr_to_ea(end_ticks);
	task_args_send.type.u64[0] = mars_ptr_to_ea(&args);
	task_args_wait.type.u64[0] = mars_ptr_to_ea(&args);

	ret = mars_task_schedule(&task_id_send, &task_args_send, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_schedule(&task_id_wait, &task_args_wait, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_wait(&task_id_send, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_wait(&task_id_wait, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_barrier_destroy(barrier);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id_send);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id_wait);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return NULL;
}

void test1(int mpu, pthread_t * threads,
	   struct thread_arg_st *thread_args)
{
	int ret, i;
	char buffer[64];
	size_t size;
	int32_t *start1_ticks, *start2_ticks, *end_ticks, *ticks;
	struct mars_context *mars_ctx;

	size = sizeof(uint32_t) * TEST1_ELEMENT_COUNT * mpu;

	ret = posix_memalign((void **)&start1_ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = posix_memalign((void **)&start2_ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = posix_memalign((void **)&end_ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = posix_memalign((void **)&ticks, 128, size);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_context_create(&mars_ctx, 0, 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < mpu; i++) {
		thread_args[i].ticks1 = &start1_ticks[TEST1_ELEMENT_COUNT * i];
		thread_args[i].ticks2 = &start2_ticks[TEST1_ELEMENT_COUNT * i];
		thread_args[i].ticks3 = &end_ticks[TEST1_ELEMENT_COUNT * i];
		thread_args[i].mars_ctx = mars_ctx;
		ret = pthread_create(&threads[i], NULL, test_thread,
						  &thread_args[i]);
		MARS_TEST_ASSERT_EQUAL(ret, 0);
	}

	for (i = 0; i < mpu; i++) {
		ret = pthread_join(threads[i], NULL);
		MARS_TEST_ASSERT_EQUAL(ret, 0);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	size = TEST1_ELEMENT_COUNT * mpu;

	for (i = 0; i < size; i++) {
		ticks[i] = start2_ticks[i] - start1_ticks[i];
	}
	sprintf(buffer, "event flag mpu-mpu: event (%d mpu pairs)", mpu);
	mars_test_print_statistics(ticks, size, buffer);

	for (i = 0; i < size; i++) {
		ticks[i] = end_ticks[i] - start1_ticks[i];
	}
	sprintf(buffer, "event flag mpu-mpu: start event - end wait (%d mpu pairs)", mpu);
	mars_test_print_statistics(ticks, size, buffer);

	for (i = 0; i < size; i++) {
		ticks[i] = end_ticks[i] - start2_ticks[i];
	}
	sprintf(buffer, "event flag mpu-mpu: end event - end wait (%d mpu pairs)", mpu);
	mars_test_print_statistics(ticks, size, buffer);

	free(start1_ticks);
	free(start2_ticks);
	free(end_ticks);
	free(ticks);
}

int main(int argc, char *argv[])
{
	int mpu,spe;
	size_t size;
	pthread_t *threads;
	struct thread_arg_st *thread_args;

	spe = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);

	for(mpu = MIN_MPUS; mpu<=MAX_MPUS; mpu++){

	threads = (pthread_t *) malloc(sizeof(pthread_t) * mpu);
	MARS_TEST_ASSERT(threads != NULL);

	size = sizeof(struct thread_arg_st) * mpu;
	thread_args = (struct thread_arg_st *)malloc(size);
	MARS_TEST_ASSERT(thread_args != NULL);

	MARS_TEST_PERF_INIT();

	test1(mpu, threads, thread_args);

	free(thread_args);
	free(threads);
	}

	return 0;
}
