/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <mars_test.h>
#include <common.h>

int mars_task_main(const struct mars_task_args *task_args)
{
	int ret, i;
	static struct test_task_args_st args ALIGN128;
	int start1_ticks[TEST1_ELEMENT_COUNT] ALIGN128;
	int start2_ticks[TEST1_ELEMENT_COUNT] ALIGN128;

	mfc_get(&args, task_args->type.u64[0], sizeof(struct test_task_args_st),
		0, 0, 0);
	mfc_write_tag_mask(1);
	mfc_read_tag_status_all();

	for (i = 0; i < TEST1_ELEMENT_COUNT; i++) {
		mars_test_counter_wait(args.sender_to_receiver_ea, i);
		mars_test_counter_set(args.receiver_to_sender_ea, i);
		MARS_TEST_ASSERT_EQUAL(ret, 0);

		mars_test_usleep(args.tb_freq, DELAY); /*give wait task time to block*/

		start1_ticks[i] = mars_task_get_ticks();
		/* notify the sender */
		ret = mars_task_event_flag_set(args.eventflag_ea, i+1);
		start2_ticks[i] = mars_task_get_ticks();
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	mfc_put(start1_ticks, args.ticks1_ea, TEST1_ELEMENT_COUNT * 4, 0, 0, 0);
	mfc_write_tag_mask(1);
	mfc_read_tag_status_all();

	mfc_put(start2_ticks, args.ticks2_ea, TEST1_ELEMENT_COUNT * 4, 0, 0, 0);
	mfc_write_tag_mask(1);
	mfc_read_tag_status_all();

	return 0;
}
