/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <spu_mfcio.h>
#include <kernel_internal_types.h>
#include <mars_test.h>
#include "host_and_mpu.h"

static inline void test_dma_wait(uint32_t tag)
{
	mfc_write_tag_mask(1 << tag);
	mfc_write_tag_update_all();
	mfc_read_tag_status();
	mfc_sync(tag);
}

static inline void test_dma_get_and_wait(void *ls, uint64_t ea,
					 uint32_t size, uint32_t tag)
{
	mfc_get((volatile void *)ls, ea, size, tag, 0, 0);
	test_dma_wait(tag);
}

static struct TASK_ARGS task_args __attribute__((aligned(16)));
static struct mars_mutex mutex;

int main(unsigned long long spuid, unsigned long long argp, unsigned long long envp)
{
	int ret;
	uint64_t task_args_ea = argp;
	uint64_t shared_resource;

	test_dma_get_and_wait(&task_args, task_args_ea, sizeof(task_args), 0);

	ret = mutex_lock_get(task_args.mutex_ea, &mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	shared_resource = spuid;
	mars_test_int64_put(task_args.shared_resource_ea, shared_resource);

	mars_test_usleep(task_args.tb_freq, 100000);

	shared_resource = mars_test_int64_get(task_args.shared_resource_ea);
	MARS_TEST_ASSERT(shared_resource == spuid);

	ret = mutex_unlock_put(task_args.mutex_ea, &mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
