/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <unistd.h>
#include <libspe2.h>
#include <mars/base.h>
#include <mars/mutex.h>
#include <mars_test.h>
#include "host_and_mpu.h"

#define THREAD_COUNT 16
#define MAX_TASK_COUNT 16

extern spe_program_handle_t mpu_prog;

static pthread_t task_threads[MAX_TASK_COUNT], threads[THREAD_COUNT];
static uint64_t mutex;
static struct TASK_ARGS task_args __attribute__((aligned(16)));
static int64_t shared_resource __attribute__((aligned(16)));

void *task_thread_proc(void *arg)
{
	int ret;
	unsigned int entry = SPE_DEFAULT_ENTRY;
	struct spe_context *spe;

	spe = spe_context_create(0, NULL);
	MARS_TEST_ASSERT(spe);

	ret = spe_program_load(spe, &mpu_prog);
	MARS_TEST_ASSERT(ret == 0);

	ret = spe_context_run(spe, &entry, 0, &task_args, NULL, NULL);
	MARS_TEST_ASSERT(ret == 0);

	ret = spe_context_destroy(spe);
	MARS_TEST_ASSERT(ret == 0);

	return NULL;
}

void *thread_proc(void *arg)
{
	int ret;

	ret = mars_mutex_lock(mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	shared_resource = (int)(uintptr_t)arg;

	usleep(100000);

	MARS_TEST_ASSERT(shared_resource == (int)(uintptr_t)arg);

	ret = mars_mutex_unlock(mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return NULL;
}

int main(void)
{
	int i, ret;
	uint32_t tb_freq;

	int spe_cnt = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);

	if (spe_cnt > MAX_TASK_COUNT)
		spe_cnt = MAX_TASK_COUNT;

	ret = mars_test_get_timebase_freq(&tb_freq);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_mutex_create(&mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	task_args.mutex_ea = mutex;
	task_args.shared_resource_ea = mars_ptr_to_ea(&shared_resource);
	task_args.tb_freq = tb_freq;

	for (i = 0; i < spe_cnt; i++)
		pthread_create(&task_threads[i], NULL, task_thread_proc, NULL);

	for (i = 0; i < THREAD_COUNT; i++)
		pthread_create(&threads[i], NULL, thread_proc, (void *)(uintptr_t)i);

	for (i = 0; i < THREAD_COUNT; i++)
		pthread_join(threads[i], NULL);

	for (i = 0; i < spe_cnt; i++)
		pthread_join(task_threads[i], NULL);

	ret = mars_mutex_destroy(mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
