/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <spu_mfcio.h>
#include <kernel_internal_types.h>
#include <mars_test.h>

static struct mars_mutex mutex;

int main(unsigned long long spuid, unsigned long long argp, unsigned long long envp)
{
	int ret;
	uint64_t mutex_ea = argp;
	uint64_t lock_counter_ea = envp;
	int32_t lock_counter;

	ret = mutex_lock_get(mutex_ea, &mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	lock_counter = mars_test_int32_get(lock_counter_ea);

	lock_counter++;

	mars_test_int32_put(lock_counter_ea, lock_counter);

	ret = mutex_unlock_put(mutex_ea, &mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
