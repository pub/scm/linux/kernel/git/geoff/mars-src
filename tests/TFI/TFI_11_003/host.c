/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <unistd.h>
#include <libspe2.h>
#include <mars/mutex.h>
#include <mars_test.h>

#define THREAD_COUNT 100
#define LOOP_COUNT   1000

extern spe_program_handle_t mpu_prog;

static uint64_t mutexes[THREAD_COUNT];
static pthread_t threads[THREAD_COUNT];
static int barrier;

void *thread_proc(void *arg)
{
	int ret, i;

	while (barrier)
		usleep(1);

	for (i = 0; i < LOOP_COUNT; i++) {
		ret = mars_mutex_create(&mutexes[(int)(intptr_t)arg]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_mutex_destroy(mutexes[(int)(intptr_t)arg]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	return NULL;
}

int main(void)
{
	int i;

	barrier = 1;

	for (i = 0; i < THREAD_COUNT; i++)
		pthread_create(&threads[i], NULL, thread_proc, (void *)(uintptr_t)i);

	sleep(1);

	barrier = 0;

	for (i = 0; i < THREAD_COUNT; i++)
		pthread_join(threads[i], NULL);

	return 0;
}
