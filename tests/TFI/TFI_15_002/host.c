/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <unistd.h>
#include <libspe2.h>
#include <mars/base.h>
#include <mars/mutex.h>
#include <mars_test.h>
#include "host_and_mpu.h"

extern spe_program_handle_t mpu1_prog, mpu2_prog;

static pthread_t task_1_thread, task_2_thread;
static struct TASK_ARGS task_args;
static uint64_t mutex;
static uint32_t locked __attribute__((aligned(16)));

void *task_thread_proc(void *arg)
{
	int ret;
	unsigned int entry = SPE_DEFAULT_ENTRY;
	struct spe_context *spe;

	spe = spe_context_create(0, NULL);
	MARS_TEST_ASSERT(spe);

	ret = spe_program_load(spe, (spe_program_handle_t *)arg);
	MARS_TEST_ASSERT(ret == 0);

	ret = spe_context_run(spe, &entry, 0, &task_args, NULL, NULL);
	MARS_TEST_ASSERT(ret == 0);

	ret = spe_context_destroy(spe);
	MARS_TEST_ASSERT(ret == 0);

	return NULL;
}

int main(void)
{
	int ret;
	uint32_t tb_freq;

	ret = mars_test_get_timebase_freq(&tb_freq);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_mutex_create(&mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	task_args.mutex_ea = mutex;
	task_args.locked_ea = mars_ptr_to_ea(&locked);
	task_args.tb_freq = tb_freq;

	pthread_create(&task_1_thread, NULL, task_thread_proc, &mpu1_prog);

	usleep(100000);

	pthread_create(&task_2_thread, NULL, task_thread_proc, &mpu2_prog);

	pthread_join(task_1_thread, NULL);

	pthread_join(task_2_thread, NULL);

	ret = mars_mutex_destroy(mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
