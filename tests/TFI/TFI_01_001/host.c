#include "mars/mutex_types.h"
#include "mars/workload_types.h"

#include "kernel_internal_types.h"
#include "workload_internal_types.h"

#include "mars/task_types.h"
#include "mars/task_barrier_types.h"
#include "mars/task_event_flag_types.h"
#include "mars/task_queue_types.h"
#include "mars/task_semaphore_types.h"

#include "task_internal_types.h"
#include "task_barrier_internal_types.h"
#include "task_event_flag_internal_types.h"
#include "task_queue_internal_types.h"
#include "task_semaphore_internal_types.h"

#include "mars_test.h"

#define CHECK_SIZE(type, expected)                       \
	MARS_TEST_ASSERT_EQUAL(sizeof(type), expected)

int main(int argc, char **argv)
{
        CHECK_SIZE(struct mars_mutex, MARS_MUTEX_SIZE);

        CHECK_SIZE(struct mars_workload_module,
                   MARS_WORKLOAD_MODULE_SIZE);
        CHECK_SIZE(struct mars_workload_context,
                   MARS_WORKLOAD_CONTEXT_SIZE);
        CHECK_SIZE(struct mars_workload_queue_header,
		   MARS_WORKLOAD_QUEUE_HEADER_SIZE);
        CHECK_SIZE(struct mars_workload_queue_block, 128);

        CHECK_SIZE(struct mars_kernel_params,
		   MARS_KERNEL_PARAMS_SIZE);

        CHECK_SIZE(struct mars_task_id, 32);
        CHECK_SIZE(struct mars_task_args, 32);

        CHECK_SIZE(struct mars_task_context,
                   MARS_TASK_CONTEXT_SIZE);
        CHECK_SIZE(struct mars_task_barrier,
                   MARS_TASK_BARRIER_SIZE);
        CHECK_SIZE(struct mars_task_event_flag,
                   MARS_TASK_EVENT_FLAG_SIZE);
        CHECK_SIZE(struct mars_task_queue,
                   MARS_TASK_QUEUE_SIZE);
        CHECK_SIZE(struct mars_task_semaphore,
                   MARS_TASK_SEMAPHORE_SIZE);

	return 0;
}
