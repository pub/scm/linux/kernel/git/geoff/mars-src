/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <unistd.h>
#include <libspe2.h>
#include <mars/mutex.h>
#include <mars_test.h>

static uint64_t mutex;
static pthread_t thread;
static int locked;

void *thread_proc(void *arg)
{
	int ret;

	ret = mars_mutex_lock(mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	sleep(1);

	ret = mars_mutex_unlock(mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	usleep(100000);

	MARS_TEST_ASSERT(locked);

	return NULL;
}

int main(void)
{
	int ret;

	locked = 0;

	ret = mars_mutex_create(&mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	pthread_create(&thread, NULL, thread_proc, NULL);

	usleep(100000);

	ret = mars_mutex_lock(mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	locked = 1;

	ret = mars_mutex_unlock(mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	pthread_join(thread, NULL);

	ret = mars_mutex_destroy(mutex);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
