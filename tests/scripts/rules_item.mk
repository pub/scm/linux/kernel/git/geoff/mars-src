#
# Copyright 2008 Sony Corporation
#
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

.SUFFIXES: .o .mpu_eo .mpu .mpu_o .wm_eo .wm .wm_o .task_eo .task .task_o
.c.o:
	$(CC) $(CFLAGS) -c -o $@ $<
.c.mpu_o:
	$(MPU_CC) $(MPU_CFLAGS) -c -o $@ $<
.mpu_o.mpu:
	$(MPU_CC) $(MPU_CFLAGS) -o $@ $(filter %.mpu_o %.o,$^) $(MPU_LDFLAGS)
.mpu.mpu_eo:
	$(EMBEDSPU) $(CFLAGS) $*_prog $< $@
.c.wm_o:
	$(MPU_CC) $(MPU_WM_CFLAGS) -c -o $@ $<
.wm_o.wm:
	$(MPU_CC) $(MPU_WM_CFLAGS) -o $@  $(filter %.wm_o %.o,$^) $(MPU_WM_LDFLAGS)
.wm.wm_eo:
	$(EMBEDSPU) $(CFLAGS) $*_prog $< $@
.c.task_o:
	$(MPU_CC) $(MPU_TASK_CFLAGS) -c -o $@ $<
.task_o.task:
	$(MPU_CC) $(MPU_TASK_CFLAGS) -o $@  $(filter %.task_o %.o,$^) $(MPU_TASK_LDFLAGS)
.task.task_eo:
	$(EMBEDSPU) $(CFLAGS) $*_prog $< $@

# don't remove intermediate files for debugging
.SECONDARY:

.PHONY: all build progs check clean distclean desc

all: build

build:
	@echo "$(test): COMPILING" >&2
	@$(MAKE) progs

progs: $(test)

$(test): $(test_objs)
	$(CC) $(CFLAGS) -o $@ $(test_objs) $(LDFLAGS)

check: build
	$(RUN_ENV) $(top_srcdir)/scripts/run.sh $(test) ./$(test)

desc:
	@$(top_srcdir)/scripts/desc.sh $(test)

clean:
	-rm -f *.o *.mpu_eo *.mpu *.mpu_o *wm_o *.wm *.wm_eo *.task_o *.task *.task_eo $(test) check.log
	-rm -f .stamp*

distclean: clean
	-rm -f *~ check.log

# EOF
