#!/bin/sh
#
# Copyright 2008 Sony Corporation
#
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


run_make()
{
    local opt
    if [ -z "$VERBOSE" -o "$VERBOSE" = 0 ]; then
	opt="-s"
    fi
    make $opt "$@"
}

run_log()
{
    local log="$1"
    shift

    local stamp=".stamp.$$"

    rm -f $stamp
    ( ( "$@" || touch $stamp ) 2>&1 | tee -a "$log" )
    [ ! -e $stamp ]
    local result="$?"

    rm -f $stamp

    return $result
}

show_summary()
{
    local title="$1"
    local passed="$2"
    local failed="$3"

    local num_pass="`echo $passed | wc | awk '{print $2}'`"
    local num_fail="`echo $failed | wc | awk '{print $2}'`"
    local num_total="`expr $num_pass + $num_fail`"

cat <<EOF
==========================
$title

PASS: $num_pass/$num_total: $passed

FAIL: $num_fail/$num_total: $failed
==========================
EOF
}

# EOF
