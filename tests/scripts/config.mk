#
# Copyright 2008 Sony Corporation
#
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

CROSS =
CC = $(CROSS)gcc
CFLAGS = \
	-I$(top_srcdir)/include/host \
	-g -Wall $(OPT_CFLAGS) $(DEFS_CFLAGS) \
	$(ARCH_CFLAGS) $(INCLUDE_CFLAGS) $(EXTRA_CFLAGS)
LDFLAGS = \
	-lmars_task -lmars_base -lspe2 \
	$(LIB_LDFLAGS) $(EXTRA_LDFLAGS)
EMBEDSPU = $(CROSS)embedspu

MPU_TYPE = spu
MPU_CROSS = $(MPU_TYPE)-
MPU_CC = $(MPU_CROSS)gcc
MPU_CFLAGS = \
	-I$(top_srcdir)/include/mpu \
	-g -Wall $(OPT_CFLAGS) $(DEFS_CFLAGS) \
	$(MPU_INCLUDE_CFLAGS) $(EXTRA_MPU_CFLAGS)
MPU_WM_CFLAGS = \
	$(MPU_CFLAGS)
MPU_TASK_CFLAGS = \
	$(MPU_CFLAGS)
MPU_LDFLAGS = \
	-Wl,-gc-sections \
	$(MPU_LIB_LDFLAGS) $(EXTRA_MPU_LDFLAGS)
MPU_WM_LDFLAGS = \
	-lmars_base \
	-Wl,--defsym=__stack=0x3ff0 \
	-Wl,--section-start,.init=0x3000 \
	-Wl,--entry,mars_module_entry -Wl,-u,mars_module_entry \
	$(MPU_LDFLAGS)
MPU_TASK_LDFLAGS = \
	-lmars_task \
	-Wl,--section-start,.init=0x4000 \
	$(MPU_LDFLAGS)

OPT_CFLAGS = -O3
DEFS_CFLAGS = -DMARS_TEST_NAME=\"$(test)\"

# Internal common variables
test = $(notdir $(shell pwd))

ifneq ($(ARCH),)
	ARCH_CFLAGS = -m$(ARCH)
endif

ifneq ($(MARS_SRC),)
	MARS_BUILD = $(MARS_SRC)

	INCLUDE_CFLAGS += \
		-I$(MARS_SRC)/base/include/host \
		-I$(MARS_SRC)/base/include/common \
		-I$(MARS_SRC)/task/include/host \
		-I$(MARS_SRC)/task/include/common \
		-I$(MARS_BUILD)/base/include/host
	LIB_LDFLAGS += \
		-L$(MARS_BUILD)/base/src/host/lib/.libs \
		-L$(MARS_BUILD)/task/src/host/lib/.libs

	MPU_INCLUDE_CFLAGS += \
		-I$(MARS_SRC)/base/include/mpu \
		-I$(MARS_SRC)/base/include/common \
		-I$(MARS_SRC)/task/include/mpu \
		-I$(MARS_SRC)/task/include/common \
		-I$(MARS_BUILD)/base/include/mpu
	MPU_LIB_LDFLAGS += \
		-L$(MARS_BUILD)/base/src/mpu/lib/.libs \
		-L$(MARS_BUILD)/task/src/mpu/lib/.libs

	RUN_LD_LIBRARY_PATH += \
		LD_LIBRARY_PATH=$(MARS_BUILD)/base/src/host/lib/.libs:$(MARS_BUILD)/task/src/host/lib/.libs
else
ifneq ($(MARS_PREFIX),)
	INCLUDE_CFLAGS = -I$(MARS_PREFIX)/include
	LIB_LDFLAGS = -L$(MARS_PREFIX)/lib$(ARCH) -L$(MARS_PREFIX)/lib

	MPU_INCLUDE_CFLAGS = -I$(MARS_PREFIX)/$(MPU_TYPE)/include
	MPU_LIB_LDFLAGS = -L$(MARS_PREFIX)/$(MPU_TYPE)/lib

	RUN_LD_LIBRARY_PATH = LD_LIBRARY_PATH=$(MARS_PREFIX)/lib$(ARCH):$(MARS_PREFIX)/lib
endif
endif

ifneq ($(RUN_LD_LIBRARY_PATH),)
ifneq ($(LD_LIBRARY_PATH),)
	RUN_LD_LIBRARY_PATH += :$(LD_LIBRARY_PATH)
endif
endif

RUN_ENV = $(RUN_LD_LIBRARY_PATH) TIMEOUT=$(timeout)

# EOF
