#!/bin/sh
#
# Copyright 2008 Sony Corporation
#
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# Usage: <NAME> <COMMAND> [<COMMAND-ARGS>]
#

TIMEOUT=${TIMEOUT:-600}

scripts_dir="`dirname $0`"

. "$scripts_dir"/run_functions

name="$1"
shift

log="check.log"

abort_command()
{
    echo >&2
    echo "Abort $test_pid" >&2

    if [ -n "$timer_pid" ]; then
	kill -USR1 $timer_pid 2>/dev/null
    fi
    if [ -n "$test_pid" ]; then
	kill -KILL $test_pid 2>/dev/null
    fi
}

stop_timer()
{
    if [ -n "$sleep_pid" ]; then
	kill -KILL $sleep_pid 2>/dev/null
    fi
    timer_aborted=1
}

run_command()
{
    trap 'abort_command' TERM INT PIPE
    trap 'stop_timer' USR1

    echo "$name: RUNNING" >&2
    test_pid=
    "$@" &
    test_pid=$!

    (
	trap 'stop_timer' USR1
	sleep_pid=
	sleep $TIMEOUT &
	sleep_pid=$!

	if [ "$timer_aborted" = 1 ]; then
	    kill -KILL $sleep_pid 2>/dev/null
	fi

	wait $sleep_pid 2>/dev/null
	if [ "$?" = 0 ]; then
	    kill -KILL $test_pid 2>/dev/null
	    echo "$name: TIMEOUT ($TIMEOUT seconds)" >&2
	fi
    ) &
    timer_pid=$!

    wait $test_pid
    result=$?

    kill -USR1 $timer_pid 2>/dev/null

    if [ "$result" = 0 ]; then
	echo "$name: PASS" >&2
    else
	echo "$name: FAIL" >&2
    fi

    return $result
}


rm -f "$log"
run_log "$log" run_command "$@"

# EOF
