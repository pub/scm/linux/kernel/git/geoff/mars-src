/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef MPU_MARS_TEST_H
#define MPU_MARS_TEST_H 1

#ifndef MARS_TEST_ABORT
#  define MARS_TEST_ABORT exit(1)
#endif /* !MARS_TEST_ABORT */

#include "../common/mars_test.h"

#include <spu_mfcio.h>

static inline void mars_test_int32_put(uint64_t ea, uint32_t val)
{
	const int tag = 0;
	static uint32_t buf[4] __attribute__((aligned(16)));
	int offset = ((ea & 0x0f) >> 2);

	buf[offset] = val;
	mfc_put(&buf[offset], ea, sizeof(buf[0]), tag, 0, 0);
	mfc_sync(tag);
	mfc_write_tag_mask(1 << tag);
	mfc_read_tag_status_all();
}

static inline uint32_t mars_test_int32_get(uint64_t ea)
{
	const int tag = 0;
	static uint32_t buf[4] __attribute__((aligned(16)));
	int offset = ((ea & 0x0f) >> 2);

	mfc_get(&buf[offset], ea, sizeof(buf[0]), tag, 0, 0);
	mfc_write_tag_mask(1 << tag);
	mfc_read_tag_status_all();

	return buf[offset];
}

static inline void mars_test_int64_put(uint64_t ea, uint64_t val)
{
	const int tag = 0;
	static uint64_t buf[2] __attribute__((aligned(16)));
	int offset = ((ea & 0x0f) >> 3);

	buf[offset] = val;
	mfc_put(&buf[offset], ea, sizeof(buf[0]), tag, 0, 0);
	mfc_sync(tag);
	mfc_write_tag_mask(1 << tag);
	mfc_read_tag_status_all();
}

static inline uint64_t mars_test_int64_get(uint64_t ea)
{
	const int tag = 0;
	static uint64_t buf[2] __attribute__((aligned(16)));
	int offset = ((ea & 0x0f) >> 3);

	mfc_get(&buf[offset], ea, sizeof(buf[0]), tag, 0, 0);
	mfc_write_tag_mask(1 << tag);
	mfc_read_tag_status_all();

	return buf[offset];
}

static inline void mars_test_counter_set(uint64_t ea, uint32_t val)
{
	const int tag = 0;

	mfc_sync(tag);
	mars_test_int32_put(ea, val);
}

static inline uint32_t mars_test_counter_get(uint64_t ea)
{
	return mars_test_int32_get(ea);
}

static inline uint32_t mars_test_counter_add(uint64_t ea, int32_t diff)
{
	const int tag = 0;
	static uint32_t buf[32] __attribute__((aligned(128)));
	int offset = ((ea & 0x7f) >> 2);

	mfc_sync(tag);

	do {
		spu_write_event_mask(MFC_LLR_LOST_EVENT);
		spu_write_event_ack(MFC_LLR_LOST_EVENT);

		mfc_getllar(&buf, ea, 0, 0);
		mfc_read_atomic_status();

		buf[offset] += diff;
		spu_dsync();
		mfc_putllc(&buf, ea & ~0x7fLLU, 0, 0);
	} while (mfc_read_atomic_status() & MFC_PUTLLC_STATUS);

	return buf[offset];
}

static inline void mars_test_counter_wait(uint64_t ea, uint32_t val)
{
	static uint32_t buf[32] __attribute__((aligned(128)));
	int cont = 1;
	do {
		spu_write_event_mask(MFC_LLR_LOST_EVENT);
		spu_write_event_ack(MFC_LLR_LOST_EVENT);

		mfc_getllar(&buf, ea & ~0x7fLLU, 0, 0);
		mfc_read_atomic_status();

		if (buf[(ea & 0x7f) >> 2] == val)
			cont = 0;
		else
			spu_read_event_status();
	} while (cont);
}

static inline void mars_test_usleep(uint32_t timebase_freq, uint32_t usec)
{
	uint32_t wait_count, initial, target, tick;

	if (usec == 0)
		return;

	initial = spu_read_decrementer();
	wait_count = (uint64_t)timebase_freq * usec / 1000000;
	target = initial - wait_count;

	if (target < initial) {
		do {
			tick = spu_read_decrementer();
		} while (target < tick && tick < initial);
	} else {
		do {
			tick = spu_read_decrementer();
		} while (tick < initial || target < tick);
	}
}

static inline void mars_test_sleep(uint32_t timebase_freq, uint32_t sec)
{
	mars_test_usleep(timebase_freq, sec * 1000000);
}

/* performance test support */

#define MARS_TEST_PERF_INIT() /* do nothing */

static inline void MARS_TEST_PERF_RESULT_SUBMIT(uint64_t ea, uint32_t time)
{
	mars_test_int32_put(ea, time);
}

#endif /* MPU_MARS_TEST_H */
