/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef COMMON_MARS_TEST_H
#define COMMON_MARS_TEST_H 1

#include <mars/error.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef MARS_TEST_ABORT
#  define MARS_TEST_ABORT abort()
#endif

#ifndef MARS_TEST_NAME
#  define MARS_TEST_NAME "UNKNOWN_TEST"
#endif

/*** assertion amcros ***/

/* generic forms */
#define MARS_TEST_ASSERT_PRINTF(e, f...)				\
	do {								\
		if (!(e)) {						\
			fprintf(stderr,					\
				MARS_TEST_NAME ": ERROR: " __FILE__ ":"	\
				__MARS_TEST_STRING_EXPAND(__LINE__)	\
				": Assertion failed: " f);		\
			MARS_TEST_ABORT;				\
		}							\
	} while (0)

#define MARS_TEST_ASSERT(e)	MARS_TEST_ASSERT_PRINTF(e, "%s\n", #e)

/* comparison */
#define MARS_TEST_ASSERT_COMPARE_INT(e, op, x)				\
	do {								\
		int be = (e);						\
		int bx = (x);						\
		MARS_TEST_ASSERT_PRINTF(be op bx,			\
					#e " " #op " " #x		\
					" (value: %d == 0x%x,"		\
					" expected: %d == 0x%x)\n",	\
					be, be, bx, bx);		\
	} while (0)
#define MARS_TEST_ASSERT_EQUAL_INT(e, x)	MARS_TEST_ASSERT_COMPARE_INT(e, ==, x)

#define MARS_TEST_ASSERT_ENUM(e, x, v2s)				\
	do {								\
		int be = (e);						\
		int bx = (x);						\
		MARS_TEST_ASSERT_PRINTF(be == bx,			\
					#e " == " #x			\
					" (value: %d == %s,"		\
					" expected: %d == %s)\n",	\
					be, v2s(be), bx, v2s(bx));	\
	} while (0)


/* short forms */
#define MARS_TEST_ASSERT_EQUAL(e, x)	MARS_TEST_ASSERT_EQUAL_INT(e, x)
#define MARS_TEST_ASSERT_ERROR(e, x)	MARS_TEST_ASSERT_ENUM(e, x, mars_test_strerr)


/*** internal macros and functions. don't use directly. ***/
#define __MARS_TEST_STRING_EXPAND(s) __MARS_TEST_STRING(s)
#define __MARS_TEST_STRING(s) #s

#define __MARS_TEST_ENUM_STR(e) case e: return # e
static inline const char *mars_test_strerr(int err)
{
	switch (err) {
	__MARS_TEST_ENUM_STR(MARS_SUCCESS);
	__MARS_TEST_ENUM_STR(MARS_ERROR_NULL);
	__MARS_TEST_ENUM_STR(MARS_ERROR_PARAMS);
	__MARS_TEST_ENUM_STR(MARS_ERROR_INTERNAL);
	__MARS_TEST_ENUM_STR(MARS_ERROR_MEMORY);
	__MARS_TEST_ENUM_STR(MARS_ERROR_ALIGN);
	__MARS_TEST_ENUM_STR(MARS_ERROR_LIMIT);
	__MARS_TEST_ENUM_STR(MARS_ERROR_STATE);
	__MARS_TEST_ENUM_STR(MARS_ERROR_FORMAT);
	__MARS_TEST_ENUM_STR(MARS_ERROR_BUSY);
	default:
		return "UNKNOWN";
	}
}

#endif /* COMMON_MARS_TEST_H */
