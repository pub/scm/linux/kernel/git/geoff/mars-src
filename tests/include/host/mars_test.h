/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef HOST_MARS_TEST_H
#define HOST_MARS_TEST_H 1

#include "../common/mars_test.h"

#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>

#define MARS_TEST_BUFSIZ	128

static inline uint32_t mars_test_lwarx(const void *p)
{
	uint32_t r;
	asm volatile("lwarx %[r],0,%[p]" : [r]"=r"(r) : [p]"b"(p));
	return r;
}

static inline uint32_t mars_test_stwcx(void *p, uint32_t v)
{
	uint32_t r;
	asm volatile("stwcx. %[v],0,%[p]\n"
		     "mfcr %[r]" : [r]"=r"(r) : [p]"b"(p), [v]"r"(v));
	return r & 0x20000000;
}

static inline void mars_test_lwsync(void)
{
	asm volatile("lwsync");
}

static inline void mars_test_counter_set(uint32_t *ptr, uint32_t val)
{
	mars_test_lwsync();

	*ptr = val;

	mars_test_lwsync();
}

static inline uint32_t mars_test_counter_get(uint32_t *ptr)
{
	return *ptr;
}

static inline uint32_t mars_test_counter_add(uint32_t *ptr, int32_t diff)
{
	uint32_t cur;

	mars_test_lwsync();

	do {
		cur = mars_test_lwarx(ptr);
		cur += diff;

		if (mars_test_stwcx(ptr, cur))
			break;

		usleep(100);
	} while (1);

	return cur;
}

static inline void mars_test_counter_wait(uint32_t *ptr, uint32_t val)
{
	while (*ptr != val)
		usleep(100);
}

static inline int mars_test_get_timebase_freq(uint32_t *timebase_freq)
{
	FILE *fp;
	char string[MARS_TEST_BUFSIZ];

	fp = fopen("/proc/cpuinfo", "r");
	if (fp == NULL)
		return -1;

	while (feof(fp) == 0) {
		string[MARS_TEST_BUFSIZ - 1] = '\0';
		if (fgets(string, MARS_TEST_BUFSIZ - 1, fp) == NULL &&
			ferror(fp) != 0) {
			return -1;
		}

		sscanf(string, "timebase        : %u", timebase_freq);
	}

	if (fclose(fp) == EOF)
		return -1;

	return 0;
}
/* performance test support */

#define MARS_TEST_PERF_INIT() /* do nothing */

#define MARS_TEST_MESSAGE(f...)			   		\
	do {							\
		fprintf(stderr,					\
			MARS_TEST_NAME ": # " __FILE__ ":"	\
			__MARS_TEST_STRING_EXPAND(__LINE__)	\
			": " f);				\
	} while (0)

#define MARS_TEST_PERF_RESULT_PRINT(tag, ticks)			   \
	do {							   \
		uint32_t __freq;				   \
		long long int __ticks = (ticks);			   \
		mars_test_get_timebase_freq(&__freq);		   \
		fprintf(stderr, MARS_TEST_NAME ": PERF: %s: %lld ticks: %f msec\n", \
		       tag, __ticks,					\
		       (double)__ticks * 1000.0 / (double)__freq);	\
	} while (0)

static inline void mars_test_tally_statistics(const int32_t *ticks, int count,
	double *average, double *variance, int32_t *maximum, int32_t *minimum)
{
	int i;

	*average = 0.0;
	*maximum = INT32_MIN;
	*minimum = INT32_MAX;
	
	for (i = 0; i < count; i++) {
		*average += ticks[i];
		if (*maximum < ticks[i])
			*maximum = ticks[i];
		if (*minimum > ticks[i])
			*minimum = ticks[i];
	}
	*average /= count;

	*variance = 0.0;
	for (i = 0; i < count; i++) {
		double tmp = ticks[i] - *average;
		*variance += tmp * tmp;
	}
	*variance = sqrt(*variance / count);
}

static inline void mars_test_print_statistics(
	const int32_t *ticks, int count, char *description)
{
        double ave, var;
        int32_t max, min;
        char buf[256];

        mars_test_tally_statistics(ticks, count, &ave, &var, &max, &min);

        sprintf(buf, "%s: average", description);
        MARS_TEST_PERF_RESULT_PRINT(buf, (int64_t)ave);

        sprintf(buf, "%s: variance", description);
        MARS_TEST_PERF_RESULT_PRINT(buf, (int64_t)var);

        sprintf(buf, "%s: maximum", description);
        MARS_TEST_PERF_RESULT_PRINT(buf, max);

        sprintf(buf, "%s: minimum", description);
        MARS_TEST_PERF_RESULT_PRINT(buf, min);
}

#endif /* HOST_MARS_TEST_H */
