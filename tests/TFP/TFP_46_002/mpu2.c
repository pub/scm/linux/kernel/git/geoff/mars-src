/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <string.h>
#include <spu_mfcio.h>
#include <mars/task.h>
#include "mars_task_test.h"
#include "host_and_mpu.h"

static uint64_t queue_item[2] __attribute__((aligned(16)));

int mars_task_main(const struct mars_task_args *task_args)
{
	uint64_t queue_ea = task_args->type.u64[0];
	uint64_t state_ea = task_args->type.u64[1];
	uint64_t counter_ea = task_args->type.u64[2];
	int i, ret;

	uint64_t tmp;
	/*
	 * Wait for queue being filled
	 */
	mars_test_counter_wait(counter_ea, QUEUE_DEPTH);
	/*
	 * Then the pusher should be blocked just after state goes
	 * STATE_START_PUSH.  If something wrong, the following will
	 * not be satisfied forever.  Then timeout detected.
	 */
	mars_test_counter_wait(state_ea, STATE_START_PUSH);
	/*
	 * If the last push of the pusher is not blocked, the state
	 * or counter is in wrong value.
	 * DISCLAIMER: If the last push operation takes too long time and is not
	 * blocked, the following ASSERT may be satisfied.  If you want to
	 * detect that, insert sleep() here.
	 */
	tmp = mars_test_int32_get(state_ea);
	MARS_TEST_ASSERT_EQUAL(tmp, STATE_START_PUSH);
	tmp = mars_test_int32_get(counter_ea);
	MARS_TEST_ASSERT_EQUAL(tmp, QUEUE_DEPTH);

	/*
	 * check the queue contents including the last item
	 */
	for (i = 0; i < QUEUE_DEPTH + 1; i++) {
		ret = mars_task_queue_pop(queue_ea, queue_item);

		MARS_TEST_ASSERT(queue_item[0] == i && queue_item[1] == i);
	}

	/*
	 * Now queue shoud be empty. Check it
	 */
	ret = mars_task_queue_try_pop(queue_ea, queue_item);
	MARS_TEST_ASSERT_ERROR(ret, MARS_ERROR_BUSY);

	mars_task_exit(0);

	return 0;
}
