/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>
#include "host_and_mpu.h"

extern spe_program_handle_t mpu1_prog, mpu2_prog;

static struct mars_context *mars;
static struct mars_task_id pusher_task_id, poper_task_id;
static struct mars_task_args task_args;
static uint64_t queue;

static uint32_t	state __attribute__((aligned(16)));
static uint32_t	depth_counter __attribute__((aligned(16)));

int main(void)
{
	int exit_code, ret;

	ret = mars_context_create(&mars, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/*
	 * Create two tasks
	 * mpu1: pusher
	 * mpu2: popper
	 */
	ret = mars_task_create(
		mars,
		&pusher_task_id,
		"pusher",
		mpu1_prog.elf_image,
		MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(
		mars,
		&poper_task_id,
		"popper",
		mpu2_prog.elf_image,
		MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/*
	 * Create queue
	 */
	ret = mars_task_queue_create(
		mars,
		&queue,
		QUEUE_ITEM_SIZE,
		QUEUE_DEPTH,
		MARS_TASK_QUEUE_MPU_TO_MPU);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	task_args.type.u64[0] = queue;
	task_args.type.u64[1] = mars_ptr_to_ea(&state);
	task_args.type.u64[2] = mars_ptr_to_ea(&depth_counter);

	/*
	 * Start tasks
	 */
	ret = mars_task_schedule(&pusher_task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_schedule(&poper_task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/*
	 * Wait for comletion
	 */
	ret = mars_task_wait(&pusher_task_id, &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_EQUAL(exit_code, 0);

	ret = mars_task_wait(&poper_task_id, &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_EQUAL(exit_code, 0);

	/*
	 * Clean up
	 */
	ret = mars_task_queue_destroy(queue);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_destroy(&pusher_task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	ret= mars_task_destroy(&poper_task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
