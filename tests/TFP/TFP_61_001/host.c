/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

#define WAIT_SEC 1

extern spe_program_handle_t mpu1_prog;
extern spe_program_handle_t mpu2_prog;

static struct mars_context *mars_ctx;
static struct mars_task_id task1_id, task2_id;
static struct mars_task_args task1_args;
static uint32_t counter __attribute__((aligned(16)));

int main(void)
{
	int ret;
	int32_t exit_code;

	ret = mars_context_create(&mars_ctx, 1, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(mars_ctx, &task1_id, NULL,
			       mpu1_prog.elf_image,
			       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	task1_args.type.u64[0] = mars_ptr_to_ea(&counter);
	ret = mars_task_schedule(&task1_id, &task1_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/* see if the task1 is blocked and in sleep state */
	mars_test_counter_wait(&counter, 1);
	ret = mars_task_create(mars_ctx, &task2_id, NULL,
			       mpu2_prog.elf_image,
			       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_schedule(&task2_id, &task1_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_wait(&task2_id, &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

	ret = mars_task_destroy(&task2_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	MARS_TEST_ASSERT_EQUAL_INT(mars_test_counter_get(&counter), 1);

	/* send a signal */
	ret = mars_task_signal_send(&task1_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	mars_test_counter_wait(&counter, 2);
	mars_test_counter_add(&counter, 1);

	ret = mars_task_wait(&task1_id, &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

	ret = mars_task_destroy(&task1_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

