/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

struct queue_entry {
	uint64_t val1;
	uint64_t val2;
};
#define Q_DEPTH		(MARS_TASK_QUEUE_WAIT_MAX)

#define NUM_LOOP 5
static uint32_t depth_size[] =
{
	1,
	Q_DEPTH / 2,
	Q_DEPTH,
	Q_DEPTH,
	Q_DEPTH
};
extern spe_program_handle_t mpu_prog;

static struct mars_context *mars_ctx;
static struct mars_task_args task_args[Q_DEPTH], thread_args[Q_DEPTH];
static struct mars_task_id task_id[Q_DEPTH];

static uint64_t mpu_to_host[NUM_LOOP];
static uint32_t notify __attribute((aligned(128)));
static uint32_t counter __attribute((aligned(128)));
static pthread_barrier_t barrier;
static struct queue_entry data[Q_DEPTH];

static pthread_t thread[Q_DEPTH];
static void *thread_proc(void *ptr)
{
	int ret;
	struct mars_task_args *task_args = (struct mars_task_args *)ptr;
	uint32_t id = task_args->type.u32[0];
	uint32_t num = task_args->type.u32[1];

	pthread_barrier_wait(&barrier);
	mars_test_counter_wait(&notify, 1);

	ret = mars_task_queue_peek(mpu_to_host[id], &data[num]);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/* check the peeked data */
	MARS_TEST_ASSERT_EQUAL(data[num].val1, id);

	return NULL;
}

int main(void)
{
	int ret, i;
	int32_t exit_code;
	uint32_t num, count;
	int num_mpu;

	num_mpu = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < NUM_LOOP; i++) {
		mars_test_counter_set(&notify, 0);
		mars_test_counter_set(&counter, 0);

		ret = pthread_barrier_init(&barrier, NULL, depth_size[i]);
		MARS_TEST_ASSERT(ret == 0);

		ret = mars_task_queue_create(
			mars_ctx,
			&mpu_to_host[i],
			sizeof(struct queue_entry),
			depth_size[i],
			MARS_TASK_QUEUE_MPU_TO_HOST);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);


		for (num = 0; num < depth_size[i]; num++) {
			/* create push task */
			ret = mars_task_create(mars_ctx, &task_id[num], NULL,
				mpu_prog.elf_image,
				MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			task_args[num].type.u32[0] = i;
			task_args[num].type.u64[1] = mpu_to_host[i];
			task_args[num].type.u64[2] = mars_ptr_to_ea(&counter);
			task_args[num].type.u64[3] = mars_ptr_to_ea(&notify);
			ret = mars_task_schedule(&task_id[num], &task_args[num], 0);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			/* create peek thread */
			/* all threads will peek the same queue */
			thread_args[num].type.u32[0] = i;
			thread_args[num].type.u32[1] = num;
			pthread_create(&thread[num], NULL,
					thread_proc, &thread_args[num]);
		}

		/* start tasks at the same time */
		if (num_mpu < depth_size[i])
			mars_test_counter_wait(&counter, num_mpu);
		else
			mars_test_counter_wait(&counter, depth_size[i]);
		mars_test_counter_set(&notify, 1);

		for (num = 0; num < depth_size[i]; num++) {
			ret = mars_task_wait(&task_id[num], &exit_code);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
			MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

			ret = mars_task_destroy(&task_id[num]);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			pthread_join(thread[num], NULL);
		}
		ret = mars_task_queue_count(mpu_to_host[i], &count);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_EQUAL(count, depth_size[i]);

		ret = mars_task_queue_destroy(mpu_to_host[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = pthread_barrier_destroy(&barrier);
		MARS_TEST_ASSERT(ret == 0);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

