/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>
#include "common.h"

extern spe_program_handle_t mpu1_prog;
extern spe_program_handle_t mpu2_prog;

static uint32_t patterns[] = {
	0x00000001,
	0xDEADBEEF,
	0xFFFFFFFF,
};

#define NUM_PATTERNS (sizeof(patterns) / sizeof(patterns[0]))
#define EVENT_FLAG_LENGTH (sizeof(uint32_t) * 8)

static struct mars_context *mars_ctx;
static struct mars_task_id task1_id, task2_id;
static struct mars_task_args task1_args;
static uint64_t eventflag;
static uint32_t counter __attribute__((aligned(128)));

int main(void)
{
	int ret, i, j, bits;
	int32_t exit_code;

	/*
	 * Create mars context which uses just _ONE_ mpu
	 */
	ret = mars_context_create(&mars_ctx, 1, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < NUM_PATTERNS; i++) {
		/*
		 * Create an event flag
		 */
		ret = mars_task_event_flag_create(
			mars_ctx,
			&eventflag,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*
		 * Invoke a task which will wait for the specified pattern
		 * with AND operation
		 */
		ret = mars_task_create(mars_ctx, &task1_id, NULL,
				       mpu1_prog.elf_image,
				       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		task1_args.type.u32[0] = patterns[i];
		task1_args.type.u64[1] = eventflag;
		task1_args.type.u64[2] = mars_ptr_to_ea(&counter);

		ret = mars_task_schedule(&task1_id, &task1_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*
		 * Wait for the schedule of the task
		 */
		mars_test_counter_wait(&counter, 1);

		/*
		 * Set one bit of the event flag each
		 * and see if the task is still in waiting state
		 * by invoking another task.
		 */
		bits = 0;
		for (j = 0; j < EVENT_FLAG_LENGTH; j++) {
			if (patterns[i] & (1 << j))
				bits ++;
		}
		if (!bits)
			continue;

		for (j = 0; j < EVENT_FLAG_LENGTH; j++) {
			if (patterns[i] & (1 << j)) {
				/*
				 * Set the bit of the flag
				 */
				ret = mars_task_event_flag_set(eventflag,
							       1 << j);
				MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

				if (--bits) {
					/*
					 * If the task which has called wait() is in
					 * waiting state, the new task will be scheduled
					 * soon and exit.
					 */
					ret = mars_task_create(mars_ctx, &task2_id, NULL,
							       mpu2_prog.elf_image,
							       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
					MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

					ret = mars_task_schedule(&task2_id, NULL, 0);
					MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

					ret = mars_task_wait(&task2_id, &exit_code);
					MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
					MARS_TEST_ASSERT_ERROR(exit_code, MAGIC_INT32);
					/*
					 * See if the counter is still 1
					 */
					MARS_TEST_ASSERT_EQUAL_INT(mars_test_counter_get(&counter),
								   1);

					ret = mars_task_destroy(&task2_id);
					MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
				} else {
					/*
					 * last bit set, so the task should be
					 * unblocked and increment the counter
					 * and exit.
					 */
					ret = mars_task_wait(&task1_id, &exit_code);
					MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
					MARS_TEST_ASSERT_ERROR(exit_code, MAGIC_INT32);
					MARS_TEST_ASSERT_EQUAL_INT(mars_test_counter_get(&counter),
								   2);
					break;
				}
			}
		}

		ret = mars_task_destroy(&task1_id);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_event_flag_destroy(eventflag);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		mars_test_counter_set(&counter, 0);
	}


	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

