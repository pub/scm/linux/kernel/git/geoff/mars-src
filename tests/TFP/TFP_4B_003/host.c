/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

struct queue_entry {
	uint64_t val1;
	uint64_t val2;
};
#define Q_DEPTH		(MARS_TASK_QUEUE_WAIT_MAX)

#define TEST_DIFF 1

#define NUM_LOOP 5
static uint32_t depth_size[] =
{
	1,
	Q_DEPTH / 2,
	Q_DEPTH,
	Q_DEPTH,
	Q_DEPTH
};
extern spe_program_handle_t mpu_prog;

static struct mars_context *mars_ctx;
static struct mars_task_args task_args;
static struct mars_task_id task_id[Q_DEPTH];

static uint64_t host_to_mpu;
static uint32_t counter __attribute((aligned(128)));
static uint32_t notify __attribute((aligned(128)));
static struct queue_entry data;

static void queue_push(uint64_t id, uint64_t count)
{
	int ret, i;

	for (i = 0; i < count; i++) {
		data.val1 = i;
		data.val2 = i + TEST_DIFF;
		ret = mars_task_queue_push(host_to_mpu, &data);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}
}

int main(void)
{
	int ret, i;
	int32_t exit_code, chk_exit;
	uint32_t num;
	uint32_t chk_bit, count;
	int num_mpu;

	num_mpu = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < NUM_LOOP; i++) {
		mars_test_counter_set(&counter, 0);
		mars_test_counter_set(&notify, 0);
		chk_bit = 0;

		ret = mars_task_queue_create(
			mars_ctx,
			&host_to_mpu,
			sizeof(struct queue_entry),
			depth_size[i],
			MARS_TASK_QUEUE_HOST_TO_MPU);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		queue_push(i, depth_size[i]);

		/* create pop task */
		for (num = 0; num < depth_size[i]; num++) {
			chk_bit += (1 << num);

			/* create pop task */
			ret = mars_task_create(mars_ctx, &task_id[num], NULL,
			mpu_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			task_args.type.u64[0] = host_to_mpu;
			task_args.type.u64[1] = mars_ptr_to_ea(&notify);
			task_args.type.u64[2] = mars_ptr_to_ea(&counter);
			ret = mars_task_schedule(&task_id[num],
					&task_args, 0);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		}

		/* start mpu tasks at the same time */
		if (num_mpu < depth_size[i])
			mars_test_counter_wait(&counter, num_mpu);
		else
			mars_test_counter_wait(&counter, depth_size[i]);

		mars_test_counter_set(&notify, 1);

		chk_exit = 0;
		for (num = 0; num < depth_size[i]; num++) {
			ret = mars_task_wait(&task_id[num], &exit_code);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
			chk_exit |= exit_code;

			ret = mars_task_destroy(&task_id[num]);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		}
		MARS_TEST_ASSERT_EQUAL((uint32_t)chk_exit, chk_bit);

		/* check queue count */
		ret = mars_task_queue_count(host_to_mpu, &count);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_EQUAL(count, 0);

		ret = mars_task_queue_destroy(host_to_mpu);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

