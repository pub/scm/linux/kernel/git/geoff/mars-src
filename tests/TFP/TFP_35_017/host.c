/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

#define TASK_MAX MARS_TASK_EVENT_FLAG_WAIT_MAX

extern spe_program_handle_t mpu_prog;

static uint32_t pattern[] = {
	0x00000001UL,
	0x0000CAFEUL,
	0x10203040UL,
	0x5A5A5A5AUL,
	0xFFFFFFFFUL
};
#define NUM_PATTERNS (sizeof(pattern)/sizeof(pattern[0]))
static struct mars_context *mars_ctx;
static struct mars_task_id task_id;
static struct mars_task_args task_args, thread_args;
static uint64_t eventflag;
static uint32_t counter __attribute__((aligned(128)));
static uint32_t unblocked __attribute__((aligned(128)));
static pthread_t thread[TASK_MAX];

static void *thread_proc(void *ptr)
{
	int ret;
	struct mars_task_args *task_args = (struct mars_task_args *)ptr;
	uint32_t pattern = task_args->type.u32[0];

	mars_test_counter_add(&counter, 1);

	ret = mars_task_event_flag_wait(eventflag, pattern,
					MARS_TASK_EVENT_FLAG_MASK_AND, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	mars_test_counter_add(&unblocked, 1);

	return NULL;
}

int main(void)
{
	int ret, i, count;
	int32_t exit_code;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < NUM_PATTERNS; i++) {
	/* use CLEAR_MANUAL sice there is no wait queue for host-side waits */
		ret = mars_task_event_flag_create(
			mars_ctx,
			&eventflag,
			MARS_TASK_EVENT_FLAG_MPU_TO_HOST,
			MARS_TASK_EVENT_FLAG_CLEAR_MANUAL);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/* create ppe threads */
		for (count = 0; count < TASK_MAX; count++) {
			thread_args.type.u32[0] = pattern[i];
			pthread_create(&thread[count], NULL, thread_proc,
				       &thread_args);
		}

		/* check if all threads are waiting */
		mars_test_counter_wait(&counter, TASK_MAX);
		MARS_TEST_ASSERT_EQUAL(mars_test_counter_get(&unblocked), 0);

		/* create mars task and set event flag */
		ret = mars_task_create(mars_ctx, &task_id, NULL,
				       mpu_prog.elf_image,
				       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		task_args.type.u64[1] = eventflag;
		task_args.type.u32[0] = pattern[i];
		ret = mars_task_schedule(&task_id, &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_wait(&task_id, &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

		ret = mars_task_destroy(&task_id);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/* check if all waiting conditon is satisfied */
		for (count = 0; count < TASK_MAX; count++)
			pthread_join(thread[count], NULL);

		MARS_TEST_ASSERT_EQUAL(mars_test_counter_get(&unblocked), TASK_MAX);

		ret = mars_task_event_flag_destroy(eventflag);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		mars_test_counter_set(&counter, 0);
		mars_test_counter_set(&unblocked, 0);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

