/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

struct queue_entry {
	uint64_t val1;
	uint64_t val2;
};
#define Q_DEPTH		(MARS_TASK_QUEUE_WAIT_MAX)

#define TEST_DIFF 1

#define NUM_LOOP 5
static uint32_t depth_size[] =
{
	1,
	Q_DEPTH / 2,
	Q_DEPTH,
	Q_DEPTH,
	Q_DEPTH
};
extern spe_program_handle_t mpu1_prog;
extern spe_program_handle_t mpu2_prog;

static struct mars_context *mars_ctx;
static struct mars_task_args task1_args, task2_args;
static struct mars_task_id task1_id[Q_DEPTH], task2_id;

static uint64_t mpu_to_mpu;
static uint32_t notify __attribute((aligned(128)));
static uint32_t counter __attribute((aligned(128)));

static void queue_push(uint64_t id)
{
	int ret;
	int32_t exit_code;

	/* create push task */
	ret = mars_task_create(mars_ctx, &task2_id, NULL,
		mpu2_prog.elf_image,
		MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	task2_args.type.u64[0] = mpu_to_mpu;
	task2_args.type.u64[1] = (uint64_t) depth_size[id];
	ret = mars_task_schedule(&task2_id, &task2_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_wait(&task2_id, &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

	ret = mars_task_destroy(&task2_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
}

int main(void)
{
	int ret, i;
	int32_t exit_code, chk_exit;
	uint32_t num;
	uint32_t chk_bit, count;
	int num_mpu;

	num_mpu = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);
	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < NUM_LOOP; i++) {
		mars_test_counter_set(&notify, 0);
		mars_test_counter_set(&counter, 0);
		chk_bit = 0;

		ret = mars_task_queue_create(
			mars_ctx,
			&mpu_to_mpu,
			sizeof(struct queue_entry),
			depth_size[i],
			MARS_TASK_QUEUE_MPU_TO_MPU);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		queue_push(i);

		/* create pop task */
		for (num = 0; num < depth_size[i]; num++) {
			chk_bit += (1 << num);

			/* create pop task */
			ret = mars_task_create(mars_ctx, &task1_id[num], NULL,
			mpu1_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			task1_args.type.u64[0] = mpu_to_mpu;
			task1_args.type.u64[1] = mars_ptr_to_ea(&notify);
			task1_args.type.u64[2] = mars_ptr_to_ea(&counter);
			ret = mars_task_schedule(&task1_id[num],
						 &task1_args, 0);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		}

		/* start pop tasks at the same time */
		if (num_mpu < depth_size[i])
			mars_test_counter_wait(&counter, num_mpu);
		else
			mars_test_counter_wait(&counter, depth_size[i]);
		mars_test_counter_set(&notify, 1);

		chk_exit = 0;
		for (num = 0; num < depth_size[i]; num++) {
			ret = mars_task_wait(&task1_id[num], &exit_code);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
			chk_exit |= exit_code;

			ret = mars_task_destroy(&task1_id[num]);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		}
		MARS_TEST_ASSERT_EQUAL((uint32_t)chk_exit, chk_bit);

		/* check queue count */
		ret = mars_task_queue_count(mpu_to_mpu, &count);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_EQUAL(count, 0);

		ret = mars_task_queue_destroy(mpu_to_mpu);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

