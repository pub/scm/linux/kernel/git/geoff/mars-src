/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <unistd.h>
#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>
#include "common.h"

#define THREAD_COUNT 16

extern spe_program_handle_t mpu_prog;

static struct mars_context *mars;
static struct mars_task_id task_id;
static struct mars_task_args task_args;
static pthread_t threads[THREAD_COUNT];
static int finished[THREAD_COUNT];

static uint32_t notify __attribute((aligned(128)));

void *thread_proc(void *arg)
{
	int exit_code =0 , ret;

	/*
	 * If the task exits, the return code should be MAGIC_INT32
	 */
	ret = mars_task_wait(&task_id, &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_EQUAL(exit_code, MAGIC_INT32);

	finished[mars_ptr_to_ea(arg)] = 1;

	return NULL;
}

int main(void)
{
	int i, ret;
	uint32_t tb_freq;

	ret = mars_test_get_timebase_freq(&tb_freq);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_context_create(&mars, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/*
	 * Create a task.
	 */
	ret = mars_task_create(mars, &task_id, "14002", mpu_prog.elf_image, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/*
	 * Start the task before threads start.
	 * As the state of the task that is just created and has been
	 * never run is FINISHED,  we can not start host threads before
	 * the task actually runs.
	 */
	task_args.type.u32[0] = mars_ptr_to_ea(&notify);
	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/*
	 * wait for the task being running
	 */
	mars_test_counter_wait(&notify, 1);

	/*
	 * Now start threads
	 */
	for (i = 0; i < THREAD_COUNT; i++) {
		finished[i] = 0;

		pthread_create(
			&threads[i],
			NULL,
			thread_proc,
			(void *)(uintptr_t)i);
	}

	/*
	 * As the task has been running and it should
	 * exit soon after, these threads exits in short time
	 * as well
	 */
	for (i = 0; i < THREAD_COUNT; i++) {
		MARS_TEST_ASSERT(finished[i]);

		pthread_join(threads[i], NULL);
	}

	/*
	 * Clean up
	 */
	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
