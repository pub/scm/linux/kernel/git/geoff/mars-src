/*
 * Copyright (C) 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>
#include <unistd.h>
#include "cmd.h"

#define FAST_DMA_ALIGN	128
#define NUM_TASK	32

#define TASK_STATE_SLEEPING	0
#define TASK_STATE_RUNNING	1

extern spe_program_handle_t mpu_prog;

static struct mars_context *mars_ctx;
static struct mars_task_id task_id[NUM_TASK];
static struct mars_task_args task_args;

struct aligned_int {
	int32_t val;
	int8_t pad[FAST_DMA_ALIGN - sizeof(int32_t)];
};

struct aligned_int cmd[NUM_TASK] __attribute__((aligned(FAST_DMA_ALIGN)));
struct aligned_int res[NUM_TASK] __attribute__((aligned(FAST_DMA_ALIGN)));

int main(void)
{
	int i, ret, num_mpu, yield_target,
		running_tasks, changed_tasks;
	int state[2][NUM_TASK]; /* 0: before yield request, 1:after request */
	int32_t exit_code;

	/*
	 * Create and run more tasks than the number of actual
	 * MPUs so that the task which would call yield() can really yield
	 * the execution to the other task.
	 */
	num_mpu = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);
	MARS_TEST_ASSERT(num_mpu > 0);
	MARS_TEST_ASSERT_PRINTF(num_mpu < NUM_TASK, 
				"usable MPUs are %d and should be less than %d\n",
				num_mpu, NUM_TASK);

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_EQUAL(ret, MARS_SUCCESS);


	for (i = 0; i < NUM_TASK; i++) {
		ret = mars_task_create(mars_ctx, &task_id[i], NULL,
			mpu_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_EQUAL(ret, MARS_SUCCESS);

		cmd[i].val = CMD_HELLO;
		res[i].val = CMD_NOP;

		task_args.type.u64[0] = mars_ptr_to_ea(&cmd[i].val);
		task_args.type.u64[1] = mars_ptr_to_ea(&res[i].val);
		ret = mars_task_schedule(&task_id[i], &task_args, 0);
		/*
		 * In this test, we can not continue if this
		 * function fails.
		 */
		MARS_TEST_ASSERT_EQUAL(ret, MARS_SUCCESS);
	}

	/*
	 * Check the state of each tasks.  Then pick
	 * one of running tasks up to be told to yield.
	 * As w can assume mars_task_schedule() works as defined,
	 * running tasks should be equal to num_mpu.
	 * Wait until the number of running tasks reaches as num_mpu
	 * If something goes wrong and this loop does not exit,
	 * the test framework will detect the timeout and abort us.
	 */
	yield_target = 0;
	do {
		running_tasks = 0;
		for (i = 0; i < NUM_TASK; i++) {
			if (res[i].val == CMD_HELLO) {
				state[0][i] = TASK_STATE_RUNNING;
				running_tasks++;
				yield_target = i;
			} else 
				state[0][i] = TASK_STATE_SLEEPING;
		}
	} while (running_tasks < num_mpu);

	/*
	 * See if MARS puts tasks runnable more than num_mpus.
	 * This test is a bit excessive because we already assumed
	 * mars_task_schedule() worked fine above...
	 */
	MARS_TEST_ASSERT_EQUAL(running_tasks, num_mpu);

	/*
	 * clear answers
	 */
	for (i = 0; i < NUM_TASK; i++) {
		res[i].val = CMD_NOP;
		cmd[i].val = CMD_NOP;
	}
	/*
	 * Ask say hello again to all except the target
	 */
	for (i = 0; i < NUM_TASK; i++) {
		if (i == yield_target)
			cmd[i].val = CMD_YIELD;
		else
			cmd[i].val = CMD_HELLO;
	}
	mars_test_lwsync();

	/*
	 * Wait for scheduling being occured
	 */
	do {
		running_tasks = 0;
		for (i = 0; i < NUM_TASK; i++) {
			if (res[i].val == CMD_HELLO) {
				state[1][i] = TASK_STATE_RUNNING;
				running_tasks++;
			} else
				state[1][i] = TASK_STATE_SLEEPING;
		}
	} while (running_tasks < num_mpu);
	/*
	 * The res[yield_target] remains CMD_NOP
	 * even if the task which calls yield(), so
	 * if the invocation of yield() just returned without
	 * acutuall yielding, that is, if the task is still running
	 * after yield(), the task will be counted as sleeping.
	 * If the above occurs, the loop above will not end.
	 */
	MARS_TEST_ASSERT_EQUAL(running_tasks, num_mpu);

	/*
	 * Check the result
	 */

	/*
	 * The task issued yield should be in sleep state
	 */
	MARS_TEST_ASSERT(state[1][yield_target] == TASK_STATE_SLEEPING);
	/*
	 * One of the tasks which have been running is in sleep state
	 */
	changed_tasks = 0;
	for (i = 0; i < NUM_TASK; i++) {
		if (i != yield_target && state[0][i] != state[1][i])
			changed_tasks++;
	}
	MARS_TEST_ASSERT_EQUAL(changed_tasks, 1);

	/* Command all tasks to exit */
	for (i = 0; i < NUM_TASK; i++)
		cmd[i].val = CMD_EXIT;
	mars_test_lwsync();

	/* Join all tasks */
	for (i = 0; i < NUM_TASK; i++) {
		ret = mars_task_wait(&task_id[i], &exit_code);
		MARS_TEST_ASSERT_EQUAL(ret, 0);
		MARS_TEST_ASSERT_EQUAL(exit_code, 0);

		ret = mars_task_destroy(&task_id[i]);
		MARS_TEST_ASSERT_EQUAL(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_EQUAL(ret, MARS_SUCCESS);

	return 0;
}
