/*
 * Copyright (C) 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <spu_mfcio.h>
#include <mars_task_test.h>
#include "cmd.h"

#define FAST_DMA_ALIGN	128
#define MFC_TAG_NO	0

static int32_t cmd __attribute__((aligned(FAST_DMA_ALIGN)));
static int32_t res __attribute__((aligned(FAST_DMA_ALIGN)));

int mars_task_main(const struct mars_task_args *task_args)
{
	int ret;
	int32_t cmd_copy;
	uint64_t cmd_ea = task_args->type.u64[0];
	uint64_t res_ea = task_args->type.u64[1];

	for (;;) {
		mfc_get(&cmd, cmd_ea, sizeof(int32_t), MFC_TAG_NO, 0, 0);
		mfc_write_tag_mask(1 << MFC_TAG_NO);
		mfc_read_tag_status_all();

		cmd_copy = cmd;
		/*
		 * clear command
		 */
		if (cmd != CMD_NOP) {
			cmd = CMD_NOP;
			mfc_put(&cmd, cmd_ea, sizeof(int32_t), MFC_TAG_NO, 0, 0);
			mfc_write_tag_mask(1 << MFC_TAG_NO);
			mfc_read_tag_status_all();
			mfc_sync(MFC_TAG_NO);
		}

		switch (cmd_copy) {
		case CMD_NOP:
			break;
		case CMD_EXIT:
			goto done;
			break;
		case CMD_YIELD:
			/*
			 * OK, we will yield.
			 */
			ret = mars_task_yield();
			/*
			 * yield() should not return error except
			 * no context save area reserved
			 */
			MARS_TEST_ASSERT_EQUAL(ret, MARS_SUCCESS);
			break;
		case CMD_HELLO:
			/*
			 * host asked me to say hello
			 */
			res = CMD_HELLO;
			mfc_put(&res, res_ea, sizeof(int32_t), MFC_TAG_NO, 0, 0);
			mfc_write_tag_mask(1 << MFC_TAG_NO);
			mfc_read_tag_status_all();
			mfc_sync(MFC_TAG_NO);
			break;
		default:
			/*
			 * the host should not issue commands except
			 * listed above
			 */
			MARS_TEST_ASSERT_PRINTF(0, "unknown CMD %d\n",
						cmd);
			break;
		}
	}

done:
	return 0;
}
