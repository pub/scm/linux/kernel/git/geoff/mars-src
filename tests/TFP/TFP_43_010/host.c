/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>


#define Q_SIZE 128
struct queue_entry {
	char text[Q_SIZE];
};
#define Q_DEPTH		1024

#define NUM_LOOP 3
static uint32_t depth_size[] =
{
	1,
	Q_DEPTH / 2,
	Q_DEPTH
};
extern spe_program_handle_t mpu1_prog;
extern spe_program_handle_t mpu2_prog;

static struct mars_context *mars_ctx;
static struct mars_task_args task1_args, task2_args;
static struct mars_task_id task1_id, task2_id;

static uint64_t host_to_mpu[NUM_LOOP];

static struct queue_entry data;

int main(void)
{
	int ret, i, num;
	int32_t exit_code;
	uint32_t count;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < NUM_LOOP; i++) {
		ret = mars_task_queue_create(
			mars_ctx,
			&host_to_mpu[i],
			sizeof(struct queue_entry),
			depth_size[i],
			MARS_TASK_QUEUE_HOST_TO_MPU);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_queue_count(host_to_mpu[i], &count);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_EQUAL(count, 0);

		/* push queue full */
		for (num = 0; num < depth_size[i]; num++) {
			ret = mars_task_queue_push(host_to_mpu[i], &data);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		}

		/* check queue count in host side */
		ret = mars_task_queue_count(host_to_mpu[i], &count);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_EQUAL(count, depth_size[i]);

		/* create pop task */
		ret = mars_task_create(mars_ctx, &task1_id, NULL,
			mpu1_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		task1_args.type.u64[0] = host_to_mpu[i];
		task1_args.type.u64[1] = (uint64_t)depth_size[i];
		ret = mars_task_schedule(&task1_id, &task1_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_wait(&task1_id, &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

		/* queue must be decremented */
		ret = mars_task_queue_count(host_to_mpu[i], &count);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_EQUAL(count, depth_size[i] - 1);

		/* create pop all task */
		ret = mars_task_create(mars_ctx, &task2_id, NULL,
			mpu2_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		task2_args.type.u64[0] = host_to_mpu[i];
		task2_args.type.u64[1] = (uint64_t)(depth_size[i] - 1);
		ret = mars_task_schedule(&task2_id, &task2_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_wait(&task2_id, &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

		/* queue must be 0 */
		ret = mars_task_queue_count(host_to_mpu[i], &count);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_EQUAL(count, 0);

		ret = mars_task_destroy(&task1_id);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_destroy(&task2_id);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_queue_destroy(host_to_mpu[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

