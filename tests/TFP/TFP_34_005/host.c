/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

#define EVENT_FLAG_SIZE 32

extern spe_program_handle_t mpu_prog;

static uint32_t patterns[] = {
	0x00000001UL,
	0x0000CAFEUL,
	0x01010101UL,
	0x5A5A5A5AUL,
};
#define NUM_PATTERNS (sizeof(patterns)/sizeof(patterns[0]))
static struct mars_context *mars_ctx;
static struct mars_task_id task_id[EVENT_FLAG_SIZE];
static struct mars_task_args task_args[EVENT_FLAG_SIZE];
static uint64_t mpu_to_host;
static uint64_t barrier;

int main(void)
{
	int ret, i, j, bits;
	uint32_t pattern, val;
	int32_t exit_code;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < NUM_PATTERNS; i++) {
		ret = mars_task_event_flag_create(
			mars_ctx,
			&mpu_to_host,
			MARS_TASK_EVENT_FLAG_MPU_TO_HOST,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*
		 * Count bits asserted
		 */
		pattern = patterns[i];
		bits = 0;
		while (pattern) {
			if (pattern & 1)
				bits ++;
			pattern >>= 1;
		}

		if (!bits || (MARS_TASK_BARRIER_WAIT_MAX < bits))
			continue;

		/*
		 * Create barrier
		 */
		ret = mars_task_barrier_create(mars_ctx, &barrier, bits);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*
		 * Create and run tasks
		 */
		for (j = 0; j < EVENT_FLAG_SIZE; j++) {
			if (!(patterns[i] & (1 << j)))
				continue;

			ret = mars_task_create(mars_ctx, &task_id[j], NULL,
					       mpu_prog.elf_image,
					       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			task_args[j].type.u32[0] = 1 << j;
			task_args[j].type.u64[1] = mpu_to_host;
			task_args[j].type.u64[2] = barrier;

			ret = mars_task_schedule(&task_id[j], &task_args[j], 0);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		}
		/*
		 * Wait for the completion of the tasks
		 */
		for (j = 0; j < EVENT_FLAG_SIZE; j++) {
			if (!(patterns[i] & (1 << j)))
				continue;

			ret = mars_task_wait(&task_id[j], &exit_code);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
			MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

			ret = mars_task_destroy(&task_id[j]);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		}

		/*
		 * See if the flag is actually set
		 */
		ret = mars_task_event_flag_try_wait(mpu_to_host,
						    patterns[i],
						    MARS_TASK_EVENT_FLAG_MASK_AND,
						    &val);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_EQUAL(val, patterns[i]);
		/*
		 * Clean up for the next iteration
		 */
		ret = mars_task_event_flag_destroy(mpu_to_host);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_barrier_destroy(barrier);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

