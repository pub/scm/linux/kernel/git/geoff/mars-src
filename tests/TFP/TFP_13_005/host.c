/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>

#define MAX_TASK_COUNT 256

extern spe_program_handle_t mpu_prog;

static struct mars_context *mars;
static struct mars_task_id task_ids[MAX_TASK_COUNT];

int main(void)
{
	int i, exit_code, ret;

	int spe_cnt = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);

	MARS_TEST_ASSERT(spe_cnt <= MAX_TASK_COUNT);

	ret = mars_context_create(&mars, spe_cnt, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < spe_cnt; i++) {
		ret = mars_task_create(
			mars,
			&task_ids[i],
			"13005",
			mpu_prog.elf_image,
			0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	for (i = 0; i < spe_cnt; i++) {
		ret = mars_task_schedule(&task_ids[i], NULL, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	for (i = 0; i < spe_cnt; i++) {
		ret = mars_task_wait(&task_ids[i], &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	for (i = 0; i < spe_cnt; i++) {
		ret = mars_task_destroy(&task_ids[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
