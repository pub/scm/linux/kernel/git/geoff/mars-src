/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <mars_task_test.h>

int mars_task_main(const struct mars_task_args *task_args)
{
	int ret;
	uint32_t wait_pattern = task_args->type.u32[0];
	uint32_t expected_pattern = task_args->type.u32[1];
	uint64_t eventflag = task_args->type.u64[1];
	uint64_t notify = task_args->type.u64[2];
	uint32_t returned_pattern;


	mars_test_counter_set(notify, 1);
	mars_test_counter_wait(notify, 2);

	ret = mars_task_event_flag_wait(
		eventflag,
		wait_pattern,
		MARS_TASK_EVENT_FLAG_MASK_OR, &returned_pattern);
	 MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	 MARS_TEST_ASSERT_EQUAL(returned_pattern, expected_pattern);

	return 0;
}
