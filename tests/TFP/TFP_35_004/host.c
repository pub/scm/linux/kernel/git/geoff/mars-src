/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

extern spe_program_handle_t mpu_prog;

static uint32_t pattern[] = {
	0x000000F0UL,
	0x0000CAFEUL,
	0xFFFFFFFFUL
};

#define NUM_PATTERNS (sizeof(pattern)/sizeof(pattern[0]))
#define EVENT_FLAG_SIZE (sizeof(uint32_t) * 8)

static struct mars_context *mars_ctx;
static struct mars_task_id task_id;
static struct mars_task_args task_args;
static uint64_t eventflag;
static uint32_t notify __attribute__((aligned(128)));

int main(void)
{
	int ret, i, j;
	int32_t exit_code;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < NUM_PATTERNS; i++) {
		ret = mars_task_event_flag_create(
			mars_ctx,
			&eventflag,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		for (j = 0; j < EVENT_FLAG_SIZE; j++) {
			if (!(pattern[i] & (1 << j)))
				continue;
			/*
			 * create a waiting task
			 */
			ret = mars_task_create(mars_ctx, &task_id,
					       NULL, mpu_prog.elf_image,
					       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			task_args.type.u32[0] = pattern[i];
			task_args.type.u32[1] = 1 << j;
			task_args.type.u64[1] = eventflag;
			task_args.type.u64[2] = mars_ptr_to_ea(&notify);

			ret = mars_task_schedule(&task_id, &task_args, 0);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			/*
			 * Wait for the schedule of the task
			 */
			mars_test_counter_wait(&notify, 1);

			/*
			 * Set one bit of the event flag
			 */
			ret = mars_task_event_flag_set(eventflag, 1 << j);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			/*
			 * continue the task
			 */
			mars_test_counter_add(&notify, 1);

			/*
			 * Wait for the completion of the task
			 */
			ret = mars_task_wait(&task_id, &exit_code);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			ret = mars_task_destroy(&task_id);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			mars_test_counter_set(&notify, 0);
		}

		ret = mars_task_event_flag_destroy(eventflag);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

