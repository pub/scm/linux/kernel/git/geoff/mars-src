/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <mars_task_test.h>

int mars_task_main(const struct mars_task_args *task_args)
{
	int ret;
	uint64_t host_to_mpu_ea = task_args->type.u64[0];
	uint32_t flags = (uint32_t)task_args->type.u64[1];
	uint32_t chk_bit;

	/* wait condition OR */
	ret = mars_task_event_flag_wait(
		host_to_mpu_ea,
		flags,
		MARS_TASK_EVENT_FLAG_MASK_OR, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/* check if all bits are cleard */
	chk_bit = 0x01;
	while (flags) {
		if (flags & 0x01) {
			ret = mars_task_event_flag_try_wait(
				host_to_mpu_ea,
				chk_bit,
				MARS_TASK_EVENT_FLAG_MASK_OR, NULL);
			/* if bit == 0, try_wait returns BUSY */
			MARS_TEST_ASSERT_ERROR(ret, MARS_ERROR_BUSY);
		}
		flags >>= 1;
		chk_bit <<= 1;
	}
	return 0;
}
