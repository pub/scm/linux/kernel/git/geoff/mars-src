/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

#define EVENT_FLAG_SIZE 32

extern spe_program_handle_t mpu_prog;

static uint32_t patterns[] = {
	0x00000001UL,
	0x0000CAFEUL,
	0x01010101UL,
	0x5A5A5A5AUL,
	0xFFFFFFFFUL
};

#define NUM_PATTERNS (sizeof(patterns)/sizeof(patterns[0]))

static struct mars_context *mars_ctx;
static struct mars_task_id task_id;
static struct mars_task_args task_args;
static uint64_t host_to_mpu;
static uint32_t shared_resource __attribute((aligned(16)));
static pthread_barrier_t barrier;
static pthread_t thread[EVENT_FLAG_SIZE];

struct thread_arg {
	uint32_t flag_bit;
};

struct thread_arg thread_args[EVENT_FLAG_SIZE];

static void *thread_proc(void *ptr)
{
	int ret;
	struct thread_arg *args = ptr;

	pthread_barrier_wait(&barrier);

	ret = mars_task_event_flag_set(host_to_mpu, args->flag_bit);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return NULL;
}

int main(void)
{
	int ret, i, bits, j;
	uint32_t pattern;
	int32_t exit_code;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < NUM_PATTERNS; i++) {
		shared_resource = 0;
		ret = mars_task_event_flag_create(
			mars_ctx,
			&host_to_mpu,
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*
		 * Count asserted bits
		 */
		bits = 0;
		pattern = patterns[i];
		while (pattern) {
			if (pattern & 1)
				bits++;
			pattern >>= 1;
		};

		if (!bits)
			continue;
		/*
		 * Create a pthread barrier to synchronize the threads
		 */
		ret = pthread_barrier_init(&barrier, NULL, bits);
		MARS_TEST_ASSERT_EQUAL_INT(ret, 0);

		/*
		 * Create pthreads which will set the event flag
		 */
		for (j = 0; j < EVENT_FLAG_SIZE; j++) {
			if (patterns[i] & (1 << j)) {
				thread_args[j].flag_bit = 1 << j;

				pthread_create(&thread[j], NULL, thread_proc,
					       &thread_args[j]);
			}
		}

		/*
		 * Wait for the completion of the pthreads
		 */
		for (j = 0; j < EVENT_FLAG_SIZE; j++) {
			if (patterns[i] & (1 << j))
				pthread_join(thread[j], NULL);
		}

		/*
		 * See if the condition will be met
		 */
		ret = mars_task_create(mars_ctx, &task_id, NULL,
				       mpu_prog.elf_image,
				       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		task_args.type.u64[1] = host_to_mpu;
		task_args.type.u32[0] = patterns[i];

		ret = mars_task_schedule(&task_id, &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_wait(&task_id, &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

		ret = mars_task_destroy(&task_id);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*
		 * Cleanup for the next iteration
		 */
		ret = pthread_barrier_destroy(&barrier);
		MARS_TEST_ASSERT_EQUAL_INT(ret, 0);

		ret = mars_task_event_flag_destroy(host_to_mpu);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

