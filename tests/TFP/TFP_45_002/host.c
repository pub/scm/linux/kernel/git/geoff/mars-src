/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>
#include "host_and_mpu.h"

extern spe_program_handle_t mpu1_prog, mpu2_prog;

static struct mars_context *mars;
static struct mars_task_id task1_id, task2_id;
static struct mars_task_args task_args;
static uint64_t queue;
static uint64_t queue_item[2] __attribute__((aligned(16)));
static uint32_t counter __attribute__((aligned(128)));

int main(void)
{
	int i, exit_code, ret;

	/*
	 * Make mars context with one usable mpu
	 */
	ret = mars_context_create(&mars, 1, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_create(
		mars,
		&queue,
		16,
		QUEUE_DEPTH,
		MARS_TASK_QUEUE_MPU_TO_HOST);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/*
	 * Create pushing task
	 */
	ret = mars_task_create(
		mars,
		&task1_id,
		"45002",
		mpu1_prog.elf_image,
		MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	task_args.type.u64[0] = queue;
	task_args.type.u64[1] = mars_ptr_to_ea(&counter);
	ret = mars_task_schedule(&task1_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/*
	 * Wait for QUEUE_DEPTH items of data pushed
	 */
	mars_test_counter_wait(&counter, 1);

	/*
	 * Tell the task to push the last item
	 */
	mars_test_counter_add(&counter, 1);

	/*
	 * Schedule another task and see if it will be done
	 */
	ret = mars_task_create(
		mars,
		&task2_id,
		"45002",
		mpu2_prog.elf_image,
		MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_schedule(&task2_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_wait(&task2_id, &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_EQUAL(exit_code, 0);

	ret = mars_task_destroy(&task2_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/*
	 * Start pop
	 */
	for (i = 0; i < QUEUE_DEPTH+1; i++) {
		ret = mars_task_queue_pop(queue, queue_item);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		MARS_TEST_ASSERT(queue_item[0] == i && queue_item[1] == i);
	}

	/*
	 * Wait for the pusher's exit
	 */
	ret = mars_task_wait(&task1_id, &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_EQUAL(exit_code, 0);

	ret = mars_task_destroy(&task1_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_destroy(queue);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
