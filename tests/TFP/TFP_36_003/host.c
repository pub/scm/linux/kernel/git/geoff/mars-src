/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

extern spe_program_handle_t mpu1_prog;
extern spe_program_handle_t mpu2_prog;
#define NUM_PATTERNS 3
static uint32_t pattern[NUM_PATTERNS][2] = {
	{  8, 0x000000FFUL },
	{ 16, 0x0000FFFFUL },
	{ 32, 0xFFFFFFFFUL }
};
static struct mars_context *mars_ctx;
static struct mars_task_id task1_id, task2_id;
static struct mars_task_args task1_args, task2_args;
static uint64_t mpu_to_mpu[NUM_PATTERNS];
static uint32_t shared_resource __attribute__((aligned(16)));

int main(void)
{
	int ret, i, bits;
	uint32_t chk_bit;
	int32_t exit_code;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < NUM_PATTERNS; i++) {
		shared_resource = 0;
		ret = mars_task_event_flag_create(
			mars_ctx,
			&mpu_to_mpu[i],
			MARS_TASK_EVENT_FLAG_MPU_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_create(mars_ctx, &task1_id, NULL,
			mpu1_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		task1_args.type.u64[0] = mpu_to_mpu[i];
		task1_args.type.u64[1] = (uint64_t)pattern[i][1];
		task1_args.type.u64[2] = mars_ptr_to_ea(&shared_resource);
		ret = mars_task_schedule(&task1_id, &task1_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		chk_bit = 0x01;
		for (bits = 0; bits < pattern[i][0]; bits++) {
			/* create flag_set task */
			ret = mars_task_create(mars_ctx, &task2_id, NULL,
				mpu2_prog.elf_image,
				MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			/* mpu tasks will set event_flags */
			task2_args.type.u64[0] = mpu_to_mpu[i];
			task2_args.type.u64[1] = (uint64_t)chk_bit;
			ret = mars_task_schedule(&task2_id, &task2_args, 0);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			ret = mars_task_wait(&task2_id, &exit_code);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
			MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

			ret = mars_task_destroy(&task2_id);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			/* check if wait task doesn't returen MARS_SUCCESS */
			if (bits != (pattern[i][0] - 1))
				MARS_TEST_ASSERT_EQUAL(shared_resource, 0);

			chk_bit <<= 1;
		}

		/* wait task return  MARS_SUCCESS and finished */
		ret = mars_task_wait(&task1_id, &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

		ret = mars_task_destroy(&task1_id);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_event_flag_destroy(mpu_to_mpu[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

