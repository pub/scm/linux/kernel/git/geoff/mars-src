/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <string.h>
#include <mars/task.h>
#include "mars_task_test.h"
#include "host_and_mpu.h"

#define LOOP_COUNT (QUEUE_DEPTH/(TASK_COUNT*2))

static uint64_t queue_item[2] __attribute__((aligned(16)));

int mars_task_main(const struct mars_task_args *task_args)
{
	uint64_t queue_ea = task_args->type.u64[0];
	int i, ret;

	for (i = 0; i < LOOP_COUNT; i++) {
		ret = mars_task_queue_pop(queue_ea, queue_item);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_queue_peek(queue_ea, queue_item);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	mars_task_exit(0);

	return 0;
}
