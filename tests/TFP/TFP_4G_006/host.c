/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

struct queue_entry {
	uint64_t val;
	uint64_t pad;
};
#define Q_DEPTH		(256 * 1024)

#define NUM_LOOP 2
static uint32_t depth_size[] =
{
	1,
	Q_DEPTH
};
extern spe_program_handle_t mpu1_prog;
extern spe_program_handle_t mpu2_prog;

static struct mars_context *mars_ctx;
static struct mars_task_args task_args;
static struct mars_task_id task1_id, task2_id;

static uint64_t host_to_mpu;
static struct queue_entry data;

static uint32_t notify __attribute__((aligned(16)));

int main(void)
{
	int ret, i;
	int32_t exit_code;

	ret = mars_context_create(&mars_ctx, 1, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < NUM_LOOP; i++) {
		mars_test_counter_set(&notify, 0);

		ret = mars_task_queue_create(
			mars_ctx,
			&host_to_mpu,
			sizeof(struct queue_entry),
			depth_size[i],
			MARS_TASK_QUEUE_HOST_TO_MPU);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/* create peek task */
		ret = mars_task_create(mars_ctx, &task1_id, NULL,
			mpu1_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		task_args.type.u64[0] = host_to_mpu;
		task_args.type.u64[1] = mars_ptr_to_ea(&notify);
		task_args.type.u64[2] = (uint64_t)i;
		ret = mars_task_schedule(&task1_id, &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/* check if queue peek is blocked in mpu task */
		mars_test_counter_wait(&notify, 1);

		ret = mars_task_create(mars_ctx, &task2_id, NULL,
				       mpu2_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		ret = mars_task_schedule(&task2_id, NULL, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_wait(&task2_id, &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

		ret = mars_task_destroy(&task2_id);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		MARS_TEST_ASSERT_EQUAL(mars_test_counter_get(&notify), 1);

		/* push one */
		data.val = i;
		ret = mars_task_queue_push(host_to_mpu, &data);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/* check if mpu task is successfly finished */
		ret = mars_task_wait(&task1_id, &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

		ret = mars_task_destroy(&task1_id);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_queue_destroy(host_to_mpu);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

