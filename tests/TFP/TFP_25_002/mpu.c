/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <mars_task_test.h>
#include "common.h"

int mars_task_main(const struct mars_task_args *task_args)
{
	int i, ret;
	uint32_t notify_num = task_args->type.u32[0];
	uint64_t barrier_ea = task_args->type.u64[1];
	uint64_t counter_ea = task_args->type.u64[2];

	/*
	 * Notify in requested times.
	 */
	for (i = 0; i < notify_num; i++) {
		ret = mars_task_barrier_notify(barrier_ea);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		mars_test_counter_add(counter_ea, 1);
	}

	/*
	 * Try notify if we didn't notify() above.
	 * It should be going to fail with MARS_ERROR_BUSY
	 */
	if (!notify_num) {
		ret = mars_task_barrier_try_notify(barrier_ea);
		MARS_TEST_ASSERT_ERROR(ret, MARS_ERROR_BUSY);
	}

	return MAGIC_UINT32;
}
