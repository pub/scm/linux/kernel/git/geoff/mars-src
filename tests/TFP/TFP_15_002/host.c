/*
 * Copyright (C) 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

extern spe_program_handle_t mpu_prog;

static struct mars_context *mars_ctx;
static struct mars_task_id task_id;
static struct mars_task_args task_args;

static uint32_t shared_resource __attribute__((aligned(128)));

int main(void)
{
	int ret;
	int32_t exit_code;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_EQUAL(ret, MARS_SUCCESS);

	ret = mars_task_create(mars_ctx, &task_id, NULL,
		mpu_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_EQUAL(ret, MARS_SUCCESS);

	task_args.type.u64[0] = mars_ptr_to_ea(&shared_resource);
	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_EQUAL(ret, MARS_SUCCESS);

	mars_test_counter_wait(&shared_resource, 1);

	/* Does mars_task_try_wait work correctly againt a task alive? */
	ret = mars_task_try_wait(&task_id, &exit_code);
	MARS_TEST_ASSERT_EQUAL(ret, MARS_ERROR_BUSY);

	mars_test_counter_set(&shared_resource, 2);

	ret = mars_task_wait(&task_id, &exit_code);
	MARS_TEST_ASSERT_EQUAL(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_EQUAL(exit_code, 0);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_EQUAL(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_EQUAL(ret, MARS_SUCCESS);

	return 0;
}


