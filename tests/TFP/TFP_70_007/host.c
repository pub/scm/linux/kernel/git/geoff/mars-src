/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <libspe2.h>
#include <mars/task.h>

#include "mars_test.h"
#include "common.h"

#define TASK_COUNT	54

extern spe_program_handle_t mpu_prog;

static uint32_t counter = 0;
static uint32_t counter_in = 0;

static int perm(int n)
{
	int ret;
	for (ret = 0; n; n--)
		ret += n;
	return ret;
}

int host_callback(const struct mars_callback_args *in,
			struct mars_callback_args *out)
{
	(void)out;

	mars_test_counter_add(&counter, 1);
	mars_test_counter_add(&counter_in, in->type.u32[0]);

	return MARS_SUCCESS;
}

int main(void)
{
	int ret, task_ret;
	struct mars_context *mars_context;
	struct mars_task_id task_id;
	struct mars_task_args task_args;

	mars_test_counter_set(&counter, 0);
	mars_test_counter_set(&counter_in, 0);

	ret = mars_context_create(&mars_context, 0, 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(mars_context, &task_id, NULL,
		mpu_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	task_args.type.u64[0] = mars_ptr_to_ea(host_callback);

	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_wait(&task_id, &task_ret);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_ERROR(task_ret, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_context);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	MARS_TEST_ASSERT_EQUAL(counter, LOOP_COUNT);
	MARS_TEST_ASSERT_EQUAL(counter_in, perm(LOOP_COUNT));

	return 0;
}
