/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <libspe2.h>
#include <mars/task.h>

#include "mars_test.h"
#include "common.h"

extern spe_program_handle_t mpu1_prog, mpu2_prog, mpu3_prog;

static uint32_t counter = 0;
static struct mars_context *mars_context;

int task_create(const struct mars_callback_args *in,
		struct mars_callback_args *out)
{
	int ret;
	void *mpu_prog_elf_image = mars_ea_to_ptr(in->type.u64[2]);
	uint32_t context_save_size = in->type.u32[1];
	struct mars_task_id task_id;
	uint64_t task_id_ea;

	ret = mars_task_create(mars_context, &task_id, NULL,
		mpu_prog_elf_image, context_save_size);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	task_id_ea = mars_ea_memalign(16, sizeof(struct mars_task_id));
	MARS_TEST_ASSERT(task_id_ea);

	mars_ea_put(task_id_ea, &task_id, sizeof(struct mars_task_id));

	out->type.u64[0] = task_id_ea;

	return MARS_SUCCESS;
}

int task_destroy(const struct mars_callback_args *in,
		struct mars_callback_args *out)
{
	int ret;
	struct mars_task_id task_id;
	uint64_t task_id_ea = in->type.u64[1];

	mars_ea_get(task_id_ea, &task_id, sizeof(struct mars_task_id));

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	mars_ea_free(task_id_ea);

	return MARS_SUCCESS;
}

int task_barrier_create(const struct mars_callback_args *in,
		struct mars_callback_args *out)
{
	int ret;
	uint64_t barrier_ea;

	ret = mars_task_barrier_create(
		mars_context,
		&barrier_ea,
		NOTIFY_TASK_COUNT);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	out->type.u64[0] = barrier_ea;

	return MARS_SUCCESS;
}

int task_barrier_destroy(const struct mars_callback_args *in,
		struct mars_callback_args *out)
{
	int ret;
	uint64_t barrier_ea = in->type.u64[1];

	ret = mars_task_barrier_destroy(barrier_ea);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return MARS_SUCCESS;
}

int host_callback(const struct mars_callback_args *in,
			struct mars_callback_args *out)
{
	switch (in->type.u32[0]) {
	case TASK_CREATE:
		return task_create(in, out);
	case TASK_DESTROY:
		return task_destroy(in, out);
	case TASK_BARRIER_CREATE:
		return task_barrier_create(in, out);
	case TASK_BARRIER_DESTROY:
		return task_barrier_destroy(in, out);
	}

	return 0;
}

int main(void)
{
	int ret, task_ret;
	struct mars_task_id task_id;
	struct mars_task_args task_args;

	mars_test_counter_set(&counter, 0);

	ret = mars_context_create(&mars_context, 0, 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(mars_context, &task_id, NULL,
		mpu1_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	task_args.type.u64[0] = mars_ptr_to_ea(&counter);
	task_args.type.u64[1] = mars_ptr_to_ea(host_callback);
	task_args.type.u64[2] = mars_ptr_to_ea(mpu2_prog.elf_image);
	task_args.type.u64[3] = mars_ptr_to_ea(mpu3_prog.elf_image);

	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_wait(&task_id, &task_ret);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_ERROR(task_ret, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_context);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
