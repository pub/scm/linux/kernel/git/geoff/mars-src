/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>

#include "mars_task_test.h"
#include "common.h"

int mars_task_main(const struct mars_task_args *task_args)
{
	int ret, task_ret, i;
	uint64_t counter_ea = task_args->type.u64[0];
	uint64_t host_callback_ea = task_args->type.u64[1];
	uint64_t mpu2_prog_elf_image_ea = task_args->type.u64[2];
	uint64_t mpu3_prog_elf_image_ea = task_args->type.u64[3];
	uint64_t barrier_ea;
	uint64_t wait_task_id_ea;
	uint64_t notify_task_id_ea[NOTIFY_TASK_COUNT];
	struct mars_callback_args in, out;
	struct mars_task_id wait_task_id;
	struct mars_task_id notify_task_id[NOTIFY_TASK_COUNT];
	struct mars_task_args task_arg;

	in.type.u32[0] = TASK_BARRIER_CREATE;
	in.type.u32[1] = NOTIFY_TASK_COUNT;
	ret = mars_task_call_host(host_callback_ea, &in, &out);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	barrier_ea = out.type.u64[0];

	in.type.u32[0] = TASK_CREATE;
	in.type.u32[1] = MARS_TASK_CONTEXT_SAVE_SIZE_MAX;

	in.type.u64[2] = mpu3_prog_elf_image_ea;
	ret = mars_task_call_host(host_callback_ea, &in, &out);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	wait_task_id_ea = out.type.u64[0];
	mfc_get(&wait_task_id, wait_task_id_ea, 
		sizeof(struct mars_task_id), 0,0,0);
	mfc_write_tag_mask(1 << 0);
	mfc_read_tag_status_all();

	in.type.u64[2] = mpu2_prog_elf_image_ea;
	for (i = 0; i < NOTIFY_TASK_COUNT; i++) {
		ret = mars_task_call_host(host_callback_ea, &in, &out);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		notify_task_id_ea[i] = out.type.u64[0];
		mfc_get(&notify_task_id[i], notify_task_id_ea[i], 
			sizeof(struct mars_task_id), 0,0,0);
		mfc_write_tag_mask(1 << 0);
		mfc_read_tag_status_all();
	}

	task_arg.type.u64[0] = counter_ea;
	task_arg.type.u64[1] = barrier_ea;
	ret = mars_task_schedule(&wait_task_id, &task_arg, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < NOTIFY_TASK_COUNT; i++) {
		ret = mars_task_schedule(&notify_task_id[i], &task_arg, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	for (i = 0; i < NOTIFY_TASK_COUNT; i++) {
		ret = mars_task_wait(&notify_task_id[i], &task_ret);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(task_ret, MARS_SUCCESS);
	}

	ret = mars_task_wait(&wait_task_id, &task_ret);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_ERROR(task_ret, MARS_SUCCESS);

	in.type.u32[0] = TASK_DESTROY;

	for (i = 0; i < NOTIFY_TASK_COUNT; i++) {
		in.type.u64[1] = notify_task_id_ea[i];

		ret = mars_task_call_host(host_callback_ea, &in, NULL);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	in.type.u64[1] = wait_task_id_ea;
	ret = mars_task_call_host(host_callback_ea, &in, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	in.type.u32[0] = TASK_BARRIER_DESTROY;
	in.type.u64[1] = barrier_ea;
	ret = mars_task_call_host(host_callback_ea, &in, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return MARS_SUCCESS;
}
