/*
 * Copyright (C) 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>
#include "common.h"

extern spe_program_handle_t mpu_prog;

static struct mars_context *mars_ctx;
static struct mars_task_id task_id;
static struct mars_task_args task_args;
static uint32_t shared_resource __attribute__((aligned(128)));

int main(void)
{
	int ret;
	int32_t exit_code;
	int done;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_EQUAL(ret, MARS_SUCCESS);

	/*
	 * Create a task
	 */
	ret = mars_task_create(mars_ctx, &task_id, "17001",
		mpu_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_EQUAL(ret, MARS_SUCCESS);

	task_args.type.u64[0] = mars_ptr_to_ea(&shared_resource);

	/*
	 * Start the task
	 */
	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_EQUAL(ret, MARS_SUCCESS);

	/*
	 * Wait for the task being running
	 */
	mars_test_counter_wait(&shared_resource, 1);

	/*
	 * Try to wait for the task's finish
	 * If the task runs forever, the test framework will detect
	 * as a timeout.
	 */
	done = 0;
	exit_code = 0;
	do {
		ret = mars_task_try_wait(&task_id, &exit_code);
		switch (ret) {
		case MARS_SUCCESS:
			/*
			 * Most likely happened
			 */
			done = 1;
			break;
		case MARS_ERROR_BUSY:
			/*
			 * the task is still running
			 */
			break;
		default:
			/*
			 * Other code should be error
			 */
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
			break;
		}
	} while (!done);


	/*
	 * check the assumption
	 */
	MARS_TEST_ASSERT_EQUAL(exit_code, MAGIC_INT32);
	MARS_TEST_ASSERT_EQUAL(shared_resource, 1);

	/*
	 * clean up
	 */
	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_EQUAL(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_EQUAL(ret, MARS_SUCCESS);

	return 0;
}


