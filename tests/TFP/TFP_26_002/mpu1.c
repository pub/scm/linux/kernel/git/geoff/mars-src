/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <mars_task_test.h>
#include "common.h"

int mars_task_main(const struct mars_task_args *task_args)
{
	int ret;
	uint32_t iteration = task_args->type.u32[0];
	uint64_t barrier_ea = task_args->type.u64[1];
	uint64_t notify_ea = task_args->type.u64[2];
	uint64_t counter_ea = task_args->type.u64[3];

	/*
	 * Tell we've started
	 */
	mars_test_counter_add(counter_ea, 1);

	/*
	 * Wait for the peer's ready
	 */
	mars_test_counter_wait(counter_ea, 2);

	/*
	 * do try_wait() repeatedly until the condition satisfied
	 */
	while (1) {
		ret = mars_task_barrier_try_wait(barrier_ea);

		switch (ret) {
		case MARS_ERROR_BUSY:
			break;

		case MARS_SUCCESS:
			/*
			 * See if the condition is really met
			 */
			MARS_TEST_ASSERT_EQUAL_INT(mars_test_counter_get(notify_ea),
						   iteration);
			goto done;
			break;

		default:
			/*
			 * should not occured
			 */
			MARS_TEST_ASSERT_ERROR(ret, 0);
			break;
		}
	}
done:
	return MAGIC_INT32;
}
