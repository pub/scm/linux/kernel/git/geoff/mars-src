/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>
#include "common.h"

extern spe_program_handle_t mpu1_prog;
extern spe_program_handle_t mpu2_prog;

static struct mars_context *mars_ctx;
static struct mars_task_id task1_id, task2_id;
static struct mars_task_args task_args;
static uint64_t barrier;

static uint32_t counter __attribute((aligned(16)));
static uint32_t notify __attribute((aligned(16)));

static uint32_t barrier_wait_num[] = {
	MARS_TASK_BARRIER_WAIT_MAX,
	MARS_TASK_BARRIER_WAIT_MAX / 2,
	1,
};

#define ARRYSIZE(a) (sizeof(a)/sizeof(a[0]))

int main(void)
{
	int ret, i;
	int32_t exit_code;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < ARRYSIZE(barrier_wait_num); i++) {

		mars_test_counter_set(&counter, 0);
		mars_test_counter_set(&notify, 0);

		/*
		 * Create a barrier with specified count of waiter
		 */
		ret = mars_task_barrier_create(mars_ctx,
					       &barrier,
					       barrier_wait_num[i]);

		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*
		 * Create a waiter task
		 */
		ret = mars_task_create(mars_ctx,
				       &task1_id,
				       NULL,
				       mpu1_prog.elf_image,
				       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		task_args.type.u32[0] = barrier_wait_num[i];
		task_args.type.u64[1] = barrier;
		task_args.type.u64[2] = mars_ptr_to_ea(&notify);
		task_args.type.u64[3] = mars_ptr_to_ea(&counter);

		/*
		 * Start the task
		 */
		ret = mars_task_schedule(&task1_id, &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*
		 * Create a notiy task
		 */
		ret = mars_task_create(mars_ctx,
				       &task2_id,
				       NULL,
				       mpu2_prog.elf_image,
				       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*
		 * Start the task
		 */
		ret = mars_task_schedule(&task2_id, &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*
		 * Wait for the completion of the tasks
		 */
		ret = mars_task_wait(&task1_id, &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MAGIC_INT32);

		ret = mars_task_wait(&task2_id, &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MAGIC_INT32);

		/*
		 * Cleanup for the next iteration
		 */
		ret = mars_task_destroy(&task1_id);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		ret = mars_task_destroy(&task2_id);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_barrier_destroy(barrier);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

