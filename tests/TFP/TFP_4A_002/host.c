/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <sys/types.h>
#include <linux/unistd.h>
#include <errno.h>
#include <pthread.h>
#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>

#define QUEUE_DEPTH 16

extern spe_program_handle_t mpu_prog;

static struct mars_context *mars;
static struct mars_task_id task_id;
static struct mars_task_args task_args;
static uint64_t queue;
static uint64_t	queue_item[2] __attribute__((aligned(16)));
static uint32_t	notify __attribute__((aligned(16)));

static pthread_t thread;
static pid_t thread_pid;
/*
 * N O T E:
 *  This test uses Linux specific feature, in other word, you can not
 * run this test program on the system other than Linux.
 */

int is_thread_blocked(pid_t tid)
{
	FILE *tstat;
	char path[128];
	char buf[1024];
	int n;

	snprintf(path, sizeof(path) - 1, "/proc/%d/status", tid);
	tstat = fopen(path, "r");
	MARS_TEST_ASSERT(tstat != NULL);

	while ((n = fscanf(tstat, "State:\t%s\n", buf)) != EOF) {
		if (n == 1)
			break;
		else
			fgetc(tstat);
	}
	fclose(tstat);

	switch (buf[0]) {
	case 'R': /* running */
	case 'D': /* disk sleep */
		return 0;
		break;
	case 'S': /* sleep */
		return 1;
		break;
	case 'T': /* trace stopped / stopped */
	case 'Z': /* zonbie */
	case 'X': /* dead */
	default:
		MARS_TEST_ASSERT(0);
		return 0;
		break;
	};
}

pid_t linux_get_thread_id(void)
{
	return syscall(__NR_gettid);
};

static void *thread_proc(void *ptr)
{
	int ret;

	thread_pid = linux_get_thread_id();
	mars_test_counter_set(&notify, 1);

	/* the last one push should be blocked */
	ret = mars_task_queue_pop(queue, queue_item);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return NULL;
}

int main(void)
{
	int exit_code, ret;
	uint32_t tb_freq;

	ret = mars_test_get_timebase_freq(&tb_freq);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = mars_context_create(&mars, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_create(
		mars,
		&queue,
		16,
		QUEUE_DEPTH,
		MARS_TASK_QUEUE_MPU_TO_HOST);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	pthread_create(&thread, NULL,
		       thread_proc, NULL);

	/* wait for the thread being blocked */
	mars_test_counter_wait(&notify, 1);
	while (!is_thread_blocked(thread_pid))
		;

	ret = mars_task_create(
		mars,
		&task_id,
		"4A002",
		mpu_prog.elf_image,
		MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	task_args.type.u64[0] = queue;
	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_wait(&task_id, &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_EQUAL(exit_code, 0);

	/* check if thread is finished for queue_push  */
	pthread_join(thread, NULL);

	ret = mars_task_queue_destroy(queue);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
