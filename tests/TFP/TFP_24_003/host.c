/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>
#include "common.h"

enum {
	WAITER_TASK,
	NOTIFIER_TASK,
	TASKS
};

static struct mars_context *mars_ctx;
extern spe_program_handle_t mpu_prog;
static struct mars_task_id task_id[TASKS];
static struct mars_task_args task_args;

static uint32_t barrier_notify_num[] = {
	MARS_TASK_BARRIER_WAIT_MAX,
	MARS_TASK_BARRIER_WAIT_MAX / 2,
	1,
};

static uint64_t barrier;

static uint32_t counter __attribute((aligned(16)));

#define ARRYSIZE(a) (sizeof(a)/sizeof(a[0]))

int main(void)
{
	int ret, i;
	int32_t exit_code;

	/*
	 * Create context
	 */
	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < ARRYSIZE(barrier_notify_num); i++) {
		/*
		 * (Re-)create barrier
		 */
		ret = mars_task_barrier_create(mars_ctx,
					       &barrier,
					       barrier_notify_num[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*
		 * (Re-)Create a task that will notify (N-1) times
		 * and wait.
		 */
		mars_test_counter_set(&counter, 0);

		ret = mars_task_create(mars_ctx,
				       &task_id[WAITER_TASK],
				       NULL,
				       mpu_prog.elf_image,
				       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*
		 * Run the task
		 */
 		task_args.type.u32[0] = barrier_notify_num[i] - 1;
 		task_args.type.u32[1] = 1; /* call wait() */
		task_args.type.u64[1] = barrier;
		task_args.type.u64[2] = mars_ptr_to_ea(&counter);

		ret = mars_task_schedule(&task_id[WAITER_TASK],
					 &task_args, 0);

		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*
		 * See if the task has called notify() (N-1) times
		 * if something goes wrong, the following will not
		 * be satisfied forever, then the framework will detect
		 * it as a timeout
		 */
		mars_test_counter_wait(&counter, barrier_notify_num[i] - 1);

		/*
		 * (Re-)create a task that will notify just ONCE
		 */
		ret = mars_task_create(mars_ctx,
				       &task_id[NOTIFIER_TASK],
				       "notifier",
				       mpu_prog.elf_image,
				       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*
		 * Run it
		 */
		task_args.type.u32[0] = 1; /* call notify() once */
		task_args.type.u32[1] = 0; /* do not call wait() */

		ret = mars_task_schedule(&task_id[NOTIFIER_TASK],
					 &task_args, 0);

		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*
		 * Then the two tasks should be going to exit.
		 * If something goes wrong, the following will not
		 * be satisfied forever, then the framework will detect
		 * it as a timeout.
		 */
		ret = mars_task_wait(&task_id[WAITER_TASK], &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MAGIC_UINT32);

		ret = mars_task_wait(&task_id[NOTIFIER_TASK], &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MAGIC_UINT32);

		/*
		 * Cleanup for the next iteration
		 */
		ret = mars_task_destroy(&task_id[WAITER_TASK]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		ret = mars_task_destroy(&task_id[NOTIFIER_TASK]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_barrier_destroy(barrier);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

