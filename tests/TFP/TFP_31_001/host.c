/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

extern spe_program_handle_t mpu_prog;

static struct mars_context *mars_ctx;
static struct mars_task_id task_id;
static struct mars_task_args task_args;
/*
 * 0: MARS_TASK_EVENT_FLAG_CLEAR_AUTO	MARS_TASK_EVENT_FLAG_MASK_AND
 * 1: MARS_TASK_EVENT_FLAG_CLEAR_AUTO	MARS_TASK_EVENT_FLAG_MASK_OR
 * 2: MARS_TASK_EVENT_FLAG_CLEAR_MANUAL	MARS_TASK_EVENT_FLAG_MASK_AND
 * 3: MARS_TASK_EVENT_FLAG_CLEAR_MANUAL	MARS_TASK_EVENT_FLAG_MASK_OR
 */
static uint64_t host_to_mpu[4];

#define EVENT_FLAG_SIZE 32

static void check_event_task(uint64_t ev_and, uint64_t ev_or)
{
	int ret;
	int32_t exit_code;

	ret = mars_task_create(mars_ctx, &task_id, NULL,
		mpu_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	task_args.type.u64[0] = ev_and;
	task_args.type.u64[1] = ev_or;
	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_wait(&task_id, &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
}

int main(void)
{
	int ret, i;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < 2; i++) {
		/* CLEAR_AUTO */
		ret = mars_task_event_flag_create(
			mars_ctx,
			&host_to_mpu[i],
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/* CLEAR_MANUAL */
		ret = mars_task_event_flag_create(
			mars_ctx,
			&host_to_mpu[i + 2],
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_MANUAL);
	}

	check_event_task(host_to_mpu[0], host_to_mpu[1]);
	check_event_task(host_to_mpu[2], host_to_mpu[3]);

	for (i = 0; i < 2; i++) {
		ret = mars_task_event_flag_destroy(host_to_mpu[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		ret = mars_task_event_flag_destroy(host_to_mpu[i + 2]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

