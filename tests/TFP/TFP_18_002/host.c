/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <unistd.h>
#include <sys/time.h>
#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>
#include "common.h"

#define MAX_TASK_COUNT	256

extern spe_program_handle_t mpu_prog;

static struct mars_context *mars;
static struct mars_task_id task_ids[MAX_TASK_COUNT];
static struct mars_task_args task_args;
static uint32_t notify __attribute__((aligned(128)));
static uint32_t counter __attribute__((aligned(128)));

static int spe_cnt;

/*
 * select the task which does 'yield()'
 */
uint64_t select_role(int task_id)
{
	return task_id == (spe_cnt / 2);
};

int main(void)
{
	int i, exit_code, ret;

	spe_cnt = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);
	MARS_TEST_ASSERT(spe_cnt <= MAX_TASK_COUNT);

	ret = mars_context_create(&mars, spe_cnt, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/*
	 * Create tasks as many as physical SPEs
	 */
	for (i = 0; i < spe_cnt; i++) {
		ret = mars_task_create(
			mars,
			&task_ids[i],
			"18002",
			mpu_prog.elf_image,
			MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	task_args.type.u64[0] = mars_ptr_to_ea(&notify);
	task_args.type.u64[1] = mars_ptr_to_ea(&counter);

	/*
	 * Start task
	 */
	for (i = 0; i < spe_cnt; i++) {
		/*
		 * Set the role for the task
		 */
		task_args.type.u64[2] = select_role(i);
		ret = mars_task_schedule(&task_ids[i], &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}
	/*
	 * As each tasks increments the counter,
	 * wait for the counter being spe_cnt
	 */
	mars_test_counter_wait(&counter, spe_cnt);

	/*
	 * Tell the tasks do the role
	 */
	mars_test_counter_set(&notify, 1);

	/*
	 * all tasks should terminate
	 */
	for (i = 0; i < spe_cnt; i++) {
		ret = mars_task_wait(&task_ids[i], &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_EQUAL(exit_code, MAGIC_INT32);
	}
	/*
	 * if all tasks exit as expected, counter should be zero
	 */
	MARS_TEST_ASSERT_EQUAL(counter, 0);

	/*
	 * Clean up
	 */
	for (i = 0; i < spe_cnt; i++) {
		ret = mars_task_destroy(&task_ids[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
