/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <pthread.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

#define EVENT_FLAG_SIZE 32

extern spe_program_handle_t mpu_prog;
#define NUM_PATTERNS 3
static uint32_t pattern[NUM_PATTERNS] = {
	0x00000001UL,
	0x0000CAFEUL,
	0xFFFFFFFFUL
};
static struct mars_context *mars_ctx;
static struct mars_task_id task_id[NUM_PATTERNS][EVENT_FLAG_SIZE];
static struct mars_task_args task_args;
static struct mars_task_args thread_args[NUM_PATTERNS][EVENT_FLAG_SIZE];
static uint64_t mpu_to_host[NUM_PATTERNS][EVENT_FLAG_SIZE];
static pthread_t thread[NUM_PATTERNS][EVENT_FLAG_SIZE];
static uint32_t counter __attribute__((aligned(128)));

static void *thread_proc(void *ptr)
{
	int ret;
	struct mars_task_args *task_args = (struct mars_task_args *)ptr;
	uint32_t i = (uint32_t)task_args->type.u32[0];
	uint32_t bits = (uint32_t)task_args->type.u32[1];
	uint32_t chk_bit = task_args->type.u32[2];

	mars_test_counter_add(&counter, 1);
	ret = mars_task_event_flag_wait(
		mpu_to_host[i][bits],
		chk_bit,
		MARS_TASK_EVENT_FLAG_MASK_OR, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return NULL;
}

int main(void)
{
	int ret, i, bits, threads;
	uint32_t chk_bit, pat;
	int32_t exit_code;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < NUM_PATTERNS; i++) {
		/* create wait threads */
		mars_test_counter_set(&counter, 0);
		pat = pattern[i];
		threads = 0;
		for (bits = 0; pat ; bits++, pat >>= 1) {
			if (!(pat & 0x01))
				continue;
			ret = mars_task_event_flag_create(
				mars_ctx,
				&mpu_to_host[i][bits],
				MARS_TASK_EVENT_FLAG_MPU_TO_HOST,
				MARS_TASK_EVENT_FLAG_CLEAR_AUTO);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			thread_args[i][bits].type.u32[0] = (uint32_t)i;
			thread_args[i][bits].type.u32[1] = (uint32_t)bits;
			thread_args[i][bits].type.u32[2] = pattern[i];
			pthread_create(&thread[i][bits], NULL,
				thread_proc, &thread_args[i][bits]);
			threads++;
		}

		mars_test_counter_wait(&counter, threads);

		/* run mpu task */
		chk_bit = 0x01;
		pat = pattern[i];
		for (bits = 0; pat ; bits++, pat >>= 1, chk_bit <<= 1) {
			if (!(pat & 0x01))
				continue;
			ret = mars_task_create(mars_ctx, &task_id[i][bits],
				NULL,
				mpu_prog.elf_image,
				MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			/* mpu tasks will set event_flags */
			task_args.type.u64[0] = mpu_to_host[i][bits];
			task_args.type.u64[1] = (uint64_t)chk_bit;
			ret = mars_task_schedule(&task_id[i][bits],
						&task_args, 0);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			ret = mars_task_wait(&task_id[i][bits], &exit_code);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
			MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);
		}
		/* check all threads done */
		pat = pattern[i];
		for (bits = 0; pat ; bits++, pat >>= 1) {
			if (pat & 0x01)
				pthread_join(thread[i][bits], NULL);
		}
		pat = pattern[i];
		for (bits = 0; pat; bits++, pat >>= 1) {
			if (pat & 0x01) {
				ret = mars_task_destroy(&task_id[i][bits]);
				MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

				ret = mars_task_event_flag_destroy(
					mpu_to_host[i][bits]);
				MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
			}
		}
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

