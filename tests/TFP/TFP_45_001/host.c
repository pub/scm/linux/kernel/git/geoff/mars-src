/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <sys/types.h>
#include <linux/unistd.h>
#include <errno.h>
#include <pthread.h>
#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>
#include "host_and_mpu.h"

extern spe_program_handle_t mpu_prog;

static struct mars_context *mars;
static struct mars_task_id task_id;
static struct mars_task_args task_args;
static uint64_t queue;
static uint64_t	queue_item[2] __attribute__((aligned(16)));

static pid_t thread_pid;
static uint32_t thread_start;

/*
 * N O T E:
 *  This test uses Linux specific feature, in other world, you can not
 * run this test program on the system other than Linux.
 */

int is_thread_blocked(pid_t tid)
{
	FILE *tstat;
	char path[128];
	char buf[1024];
	int n;

	snprintf(path, sizeof(path) - 1, "/proc/%d/status", tid);
	tstat = fopen(path, "r");
	MARS_TEST_ASSERT(tstat != NULL);

	while ((n = fscanf(tstat, "State:\t%s\n", buf)) != EOF) {
		if (n == 1)
			break;
		else
			fgetc(tstat);
	}
	fclose(tstat);

	switch (buf[0]) {
	case 'R': /* running */
	case 'D': /* disk sleep */
		return 0;
		break;
	case 'S': /* sleep */
		return 1;
		break;
	case 'T': /* trace stopped / stopped */
	case 'Z': /* zonbie */
	case 'X': /* dead */
	default:
		MARS_TEST_ASSERT(0);
		return 0;
		break;
	};
}

pid_t linux_get_thread_id(void)
{
	return syscall(__NR_gettid);
};

void * last_pusher(void *arg)
{
	int ret;

	thread_pid = linux_get_thread_id(); /* Linux specific */
	mars_test_counter_set(&thread_start, 1);

	ret = mars_task_queue_push(queue, queue_item);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

int main(void)
{
	int i, exit_code, ret;
	pthread_t pusher;

	ret = mars_context_create(&mars, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_queue_create(
		mars,
		&queue,
		16,
		QUEUE_DEPTH,
		MARS_TASK_QUEUE_HOST_TO_MPU);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/*
	 * push QUEUE_DEPTH of data to queue
	 */
	for (i = 0; i < QUEUE_DEPTH; i++) {
		queue_item[0] = queue_item[1] = i;

		ret = mars_task_queue_push(queue, queue_item);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	queue_item[0] = queue_item[1] = QUEUE_DEPTH;
	ret = pthread_create(&pusher, NULL, last_pusher, NULL);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	mars_test_counter_wait(&thread_start, 1);
	/*
	 * Do busy waiting for the thread state being S (sleeping).
	 * The thread should be going to be blocked in mars_task_queue_push()
	 * so the loop should exit;
	 */
	while (!is_thread_blocked(thread_pid))
		;
	/*
	 * Invoke the popper
	 */
	ret = mars_task_create(
		mars,
		&task_id,
		"45001",
		mpu_prog.elf_image,
		MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	task_args.type.u64[0] = queue;
	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_wait(&task_id, &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_EQUAL(exit_code, 0);

	ret = mars_task_queue_destroy(queue);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = pthread_join(pusher, NULL);
	MARS_TEST_ASSERT(ret == 0);

	ret = mars_context_destroy(mars);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
