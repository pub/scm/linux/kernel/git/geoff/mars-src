/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

#define NUM_TASKS MARS_TASK_BARRIER_WAIT_MAX
/* maximum number of usable mpu in one system is 16 (8 SPU x 2 Cell) */
#define DUMMY_TASKS 16
extern spe_program_handle_t mpu1_prog;
extern spe_program_handle_t mpu2_prog;
extern spe_program_handle_t mpu3_prog;

static struct mars_context *mars_ctx;
static struct mars_task_id task1_id, task2_id;
static struct mars_task_id dummy_task_id[DUMMY_TASKS];
static struct mars_task_args task_args, dummy_task_args;
static uint64_t barrier;
static uint32_t counter __attribute__((aligned(16)));
static uint32_t dummy_task_counter __attribute__((aligned(16)));
static uint32_t dummy_notify __attribute__((aligned(16)));
#define CHK_NUM 3
static uint32_t chk_num[] = {
	1,
	NUM_TASKS / 2,
	NUM_TASKS,
};

/*
 * See if there are 'mpus' number of SPU usable.
 * If there are less than the specified, this function blocks
 * forever and then the test framework will detect as timeout.
 */
void test_usable_mpus(int mpus)
{
	int ret, i;
	int32_t exit_code;

	MARS_TEST_ASSERT(mpus <= DUMMY_TASKS);

	mars_test_counter_set(&dummy_task_counter, 0);
	mars_test_counter_set(&dummy_notify, 0);

	dummy_task_args.type.u64[0] = mars_ptr_to_ea(&dummy_task_counter);
	dummy_task_args.type.u64[1] = mars_ptr_to_ea(&dummy_notify);

	for (i = 0; i < mpus; i++) {
		ret = mars_task_create(mars_ctx, &dummy_task_id[i], NULL,
				       mpu3_prog.elf_image,
				       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_schedule(&dummy_task_id[i], &dummy_task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}
	/*
	 * If the number of usable mpus is less than specified,
	 * the following is never returned.
	 */
	mars_test_counter_wait(&dummy_task_counter, mpus);

	mars_test_counter_set(&dummy_notify, 1);

	for (i = 0; i < mpus; i++) {
		ret = mars_task_wait(&dummy_task_id[i], &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

		ret = mars_task_destroy(&dummy_task_id[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}
}

int main(void)
{
	int ret, i;
	int32_t exit_code;
	int num_mpu;

	num_mpu = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < CHK_NUM; i++) {
		mars_test_counter_set(&counter, 0);

		ret = mars_task_barrier_create(mars_ctx, &barrier, chk_num[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/* create wait task */
		ret = mars_task_create(mars_ctx, &task1_id, NULL,
				       mpu1_prog.elf_image,
				       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		task_args.type.u64[1] = barrier;
		task_args.type.u64[2] = mars_ptr_to_ea(&counter);
		ret = mars_task_schedule(&task1_id, &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/* create notifier task */
		ret = mars_task_create(mars_ctx, &task2_id, NULL,
				       mpu2_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		task_args.type.u32[0] = chk_num[i] - 1;
		ret = mars_task_schedule(&task2_id, &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_wait(&task2_id, &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

		/* check if mpu task is waiting */
		mars_test_counter_wait(&counter, 1);
		test_usable_mpus(num_mpu);
		ret = mars_task_try_wait(&task1_id, &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_ERROR_BUSY);

		/* notify last one time */
		task_args.type.u32[0] = 1;
		ret = mars_task_schedule(&task2_id, &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_wait(&task2_id, &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

		/* check if wait task will exit */
		mars_test_counter_wait(&counter, 2);
		mars_test_counter_add(&counter, 1);

		ret = mars_task_wait(&task1_id, &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

		ret = mars_task_destroy(&task1_id);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_destroy(&task2_id);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_barrier_destroy(barrier);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

