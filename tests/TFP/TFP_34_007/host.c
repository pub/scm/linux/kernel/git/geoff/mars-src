/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

extern spe_program_handle_t mpu_prog;

static uint32_t patterns[] = {
	0x00000001UL,
	0x0000CAFEUL,
	0x10203040UL,
	0x5A5A5A5AUL,
	0xFFFFFFFFUL
};

#define NUM_PATTERNS (sizeof(patterns)/sizeof(patterns[0]))
#define EVENT_FLAG_SIZE (sizeof(uint32_t) * 8)

static struct mars_context *mars_ctx;
static struct mars_task_id task_id[EVENT_FLAG_SIZE];
static struct mars_task_args task_args[EVENT_FLAG_SIZE];
static uint64_t eventflag;
static uint32_t counter __attribute__((aligned(128)));
static uint32_t notify __attribute__((aligned(128)));

int main(void)
{
	int ret, i, bits, num_mpu;
	int32_t exit_code;
	uint32_t returned_bits;

	num_mpu = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < NUM_PATTERNS; i++) {
		/*
		 * Create eventflag
		 */
		ret = mars_task_event_flag_create(
			mars_ctx,
			&eventflag,
			MARS_TASK_EVENT_FLAG_MPU_TO_HOST,
			MARS_TASK_EVENT_FLAG_CLEAR_MANUAL);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*
		 * Invoke the number of EVENT_FLAG_SIZE tasks
		 * and class them into two categories, setters and resetters,
		 * according to the specified flag patterns.
		 */
		for (bits = 0; bits < EVENT_FLAG_SIZE; bits++) {
			ret = mars_task_create(mars_ctx, &task_id[bits],
					       NULL, mpu_prog.elf_image,
					       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			if (patterns[i] & (1 << bits))
				task_args[bits].type.u32[0] = 1; /* set task */
			else
				task_args[bits].type.u32[0] = 0; /* clear task */
			task_args[bits].type.u32[1] = 1 << bits;
			task_args[bits].type.u64[1] = eventflag;
			task_args[bits].type.u64[2] = mars_ptr_to_ea(&counter);
			task_args[bits].type.u64[3] = mars_ptr_to_ea(&notify);

			ret = mars_task_schedule(&task_id[bits], &task_args[bits], 0);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		}
		/*
		 * Trigger the task to do the job
		 * As there are less usable SPUS than
		 * EVENT_FLAG_SIZE in usual systems, treat that case
		 */
		if (num_mpu < EVENT_FLAG_SIZE)
			mars_test_counter_wait(&counter, num_mpu);
		else
			mars_test_counter_wait(&counter, EVENT_FLAG_SIZE);
		mars_test_counter_set(&notify, 1);

		/*
		 * Wait for the completion of set/clear tasks
		 */
		for (bits = 0; bits < EVENT_FLAG_SIZE; bits++) {
			ret = mars_task_wait(&task_id[bits], &exit_code);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
			MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);
		}

		/*
		 * See if the event flag is actually set as desired.
		 * First, check asserte bits with AND operation
		 * Second, deasserted bits witn OR operation with nagated mask
		 */
		ret = mars_task_event_flag_try_wait(eventflag,
						    patterns[i],
						    MARS_TASK_EVENT_FLAG_MASK_AND,
						    &returned_bits);
		/* the condition should be satisfied */
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_event_flag_try_wait(eventflag,
						    ~patterns[i],
						    MARS_TASK_EVENT_FLAG_MASK_OR,
						    &returned_bits);
		/* the condition should _NOT_ be satisfied */
		MARS_TEST_ASSERT_ERROR(ret, MARS_ERROR_BUSY);

		/*
		 * Cleanup for the next iteration
		 */
		for (bits = 0; bits < EVENT_FLAG_SIZE; bits++) {
			ret = mars_task_destroy(&task_id[bits]);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		}

		ret = mars_task_event_flag_destroy(eventflag);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		mars_test_counter_set(&counter, 0);
		mars_test_counter_set(&notify, 0);
	}
	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

