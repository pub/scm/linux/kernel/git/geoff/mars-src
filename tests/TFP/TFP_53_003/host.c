/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

#define NUM_TASKS		32
#define SEMAPHORE_COUNTS	5

extern spe_program_handle_t mpu1_prog, mpu2_prog, mpu3_prog, mpu4_prog;

static struct mars_context *mars_ctx;
static struct mars_task_id task_id[NUM_TASKS], task3_id[NUM_TASKS], task2_id, task4_id;
static struct mars_task_args task_args;

static uint64_t semaphore;
static uint32_t notify __attribute__((aligned(128)));
static uint32_t waiter __attribute__((aligned(128)));

int main(void)
{
	int ret, i;
	int32_t exit_code;
	int num_mpu;

	MARS_TEST_ASSERT(MARS_TASK_SEMAPHORE_WAIT_MAX >= NUM_TASKS);

	num_mpu = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);
	MARS_TEST_ASSERT(num_mpu < NUM_TASKS);

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_semaphore_create(
		mars_ctx,
		&semaphore,
		NUM_TASKS * SEMAPHORE_COUNTS);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/* acquire the semaphore as much as possible */
	mars_test_counter_set(&notify, 0);
	task_args.type.u64[0] = semaphore;
	task_args.type.u64[1] = mars_ptr_to_ea(&notify);
	task_args.type.u64[2] = SEMAPHORE_COUNTS;
	for (i = 0; i < NUM_TASKS; i++) {
		ret = mars_task_create(mars_ctx, &task_id[i], NULL,
				       mpu1_prog.elf_image,
				       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_schedule(&task_id[i], &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	/* run them synchronously as much as possible */
	mars_test_counter_set(&notify, 1);

	for (i = 0; i < NUM_TASKS; i++) {
		ret = mars_task_wait(&task_id[i], &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);
	}

	/* check semaphore count :
	 * 1: Run tasks (mpu3) which will do just busy wait as many as number of
	 * (num_mpu - 1).
	 * 2: Run a task (mpu2) which should be going to be blocked
	 * with semaphore_aquire() because it try to aquire the semaphore count more
	 * than the number specified in the creation time.
	 * 3: Try to run a new task (mpu4).  As mpu2 should be in ready state,
	 * mpu3 will run and exit soon.
	 */

	/* step 1 */
	mars_test_counter_set(&notify, 0);
	task_args.type.u64[0] = mars_ptr_to_ea(&notify);
	task_args.type.u64[1] = mars_ptr_to_ea(&waiter);
	for (i = 0; i < num_mpu - 1; i++) {
		ret = mars_task_create(mars_ctx, &task3_id[i], NULL,
				       mpu3_prog.elf_image,
				       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		ret = mars_task_schedule(&task3_id[i], &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}
	mars_test_counter_wait(&notify, num_mpu - 1);

	/* step 2 */
	mars_test_counter_set(&notify, 0);
	task_args.type.u64[0] = semaphore;
	task_args.type.u64[1] = mars_ptr_to_ea(&notify);
	ret = mars_task_create(mars_ctx, &task2_id, NULL,
			       mpu2_prog.elf_image,
			       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	ret = mars_task_schedule(&task2_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	mars_test_counter_wait(&notify, 1);

	/* step 3 */
	ret = mars_task_create(mars_ctx, &task4_id, NULL,
			       mpu4_prog.elf_image,
			       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	ret = mars_task_wait(&task4_id, &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

	exit(0);
}

