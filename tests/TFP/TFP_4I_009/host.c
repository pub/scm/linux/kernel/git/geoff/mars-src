/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

struct queue_entry {
	uint64_t val1;
	uint64_t val2;
};
#define Q_DEPTH		(256)

#define TASK_MAX 16
#define NUM_LOOP 5
static uint32_t tasks[] =
{
	1,
	TASK_MAX / 4,
	TASK_MAX / 2,
	TASK_MAX,
	TASK_MAX,
};
extern spe_program_handle_t mpu1_prog;
extern spe_program_handle_t mpu2_prog;

static struct mars_context *mars_ctx;
static struct mars_task_args task_args;
static struct mars_task_id task1_id[TASK_MAX], task2_id;

static uint64_t mpu_to_mpu;
static uint32_t counter __attribute__((aligned(128)));

int main(void)
{
	int ret, i;
	int32_t exit_code;
	uint32_t num, count, task_num;
	int spe_cnt = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < NUM_LOOP; i++) {
		mars_test_counter_set(&counter, 0);

		task_num = tasks[i] > spe_cnt - 1 ? spe_cnt - 1 : tasks[i];

		ret = mars_task_queue_create(
			mars_ctx,
			&mpu_to_mpu,
			sizeof(struct queue_entry),
			Q_DEPTH,
			MARS_TASK_QUEUE_MPU_TO_MPU);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		task_args.type.u64[0] = mpu_to_mpu;
		task_args.type.u64[1] = (uint64_t)i;
		task_args.type.u64[2] = mars_ptr_to_ea(&counter);

		for (num = 0; num < task_num; num++) {
			/* create peek tasks */
			ret = mars_task_create(mars_ctx, &task1_id[num], NULL,
			mpu1_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			ret = mars_task_schedule(&task1_id[num],
				&task_args, 0);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		}

		mars_test_counter_wait(&counter, task_num);

		/* check if all tasks are still running */
		for (num = 0; num < task_num; num++) {
			ret = mars_task_try_wait(&task1_id[num], &exit_code);
			MARS_TEST_ASSERT_ERROR(ret, MARS_ERROR_BUSY);
		}

		/* create and run push task */
		ret = mars_task_create(mars_ctx, &task2_id, NULL,
			mpu2_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_schedule(&task2_id, &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		for (num = 0; num < task_num; num++) {
			ret = mars_task_wait(&task1_id[num], &exit_code);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
			MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

			ret = mars_task_destroy(&task1_id[num]);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		}

		ret = mars_task_wait(&task2_id, &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

		ret = mars_task_destroy(&task2_id);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_queue_count(mpu_to_mpu, &count);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_EQUAL(count, 1);

		ret = mars_task_queue_destroy(mpu_to_mpu);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

