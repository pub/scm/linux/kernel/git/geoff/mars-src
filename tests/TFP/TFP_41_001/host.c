/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>


#define Q_SIZE_MIN	16
#define Q_SIZE_MAX	MARS_TASK_QUEUE_ENTRY_SIZE_MAX
#define Q_DEPTH_MIN	1
#define Q_DEPTH_MAX	10000

static struct mars_context *mars_ctx;

#define NUM_TYPE 3
static uint32_t type[NUM_TYPE] = {
	MARS_TASK_QUEUE_HOST_TO_MPU,
	MARS_TASK_QUEUE_MPU_TO_HOST,
	MARS_TASK_QUEUE_MPU_TO_MPU
};
static uint64_t q[NUM_TYPE][4];

int main(void)
{
	int ret, i, j;
	uint32_t count;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < NUM_TYPE; i++) {
		/* DEPTH_MIN, SIZE_MIN */
		ret = mars_task_queue_create(
			mars_ctx,
			&q[i][0],
			sizeof(char) * Q_SIZE_MIN,
			Q_DEPTH_MIN,
			type[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_queue_count(q[i][0], &count);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_EQUAL(count, 0);

		/* DEPTH_MAX, SIZE_MIN */
		ret = mars_task_queue_create(
			mars_ctx,
			&q[i][1],
			sizeof(char) * Q_SIZE_MIN,
			Q_DEPTH_MAX,
			type[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_queue_count(q[i][1], &count);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_EQUAL(count, 0);

		/* DEPTH_MIN, SIZE_MAX */
		ret = mars_task_queue_create(
			mars_ctx,
			&q[i][2],
			sizeof(char) * Q_SIZE_MAX,
			Q_DEPTH_MIN,
			type[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_queue_count(q[i][2], &count);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_EQUAL(count, 0);

		/* DEPTH_MAX, SIZE_MAX */
		ret = mars_task_queue_create(
			mars_ctx,
			&q[i][3],
			sizeof(char) * Q_SIZE_MAX,
			Q_DEPTH_MAX,
			type[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_queue_count(q[i][3], &count);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_EQUAL(count, 0);

		for (j = 0; j < 4; j++) {
			ret = mars_task_queue_destroy(q[i][j]);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		}
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

