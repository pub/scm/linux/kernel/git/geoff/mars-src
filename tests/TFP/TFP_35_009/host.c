/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

extern spe_program_handle_t mpu1_prog;
extern spe_program_handle_t mpu2_prog;
extern spe_program_handle_t mpu3_prog;

static uint32_t patterns[] = {
	0x00000001UL,
	0x0000CAFEUL,
	0x10203040UL,
};

#define NUM_PATTERNS (sizeof(patterns)/sizeof(patterns[0]))

static struct mars_context *mars_ctx;
static struct mars_task_id task_id, task2_id, task3_id;
static struct mars_task_args task_args;
static uint64_t eventflag;
static uint32_t counter __attribute__((aligned(128)));

int main(void)
{
	int ret, i;
	int32_t exit_code;

	/*
	 * Create mars context with ONE usable mpu
	 */
	ret = mars_context_create(&mars_ctx, 1, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < NUM_PATTERNS; i++) {

		/*
		 * Create an event flag
		 */
		ret = mars_task_event_flag_create(
			mars_ctx,
			&eventflag,
			MARS_TASK_EVENT_FLAG_MPU_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		task_args.type.u32[0] = patterns[i];
		task_args.type.u64[1] = eventflag;
		task_args.type.u64[2] = mars_ptr_to_ea(&counter);
		/*
		 * Let the eventflag asserted
		 */
		ret = mars_task_create(mars_ctx, &task_id, NULL,
				       mpu1_prog.elf_image,
				       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_schedule(&task_id, &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_wait(&task_id, &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_destroy(&task_id);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		/*
		 * Run a task which will call flag_wait().
		 * It will do busy wait before the call of flag_wait()
		 * so there's no chance for another task to be scheduled
		 * because there's only one usable mpu in the mars context.
		 * We use this symptom to verify the call of flag_wait() would
		 * not be blocked.
		 */
		ret = mars_task_create(mars_ctx, &task2_id, NULL,
				       mpu2_prog.elf_image,
				       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);


		ret = mars_task_schedule(&task2_id, &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*
		 * Wait for the schedule of the task
		 */
		mars_test_counter_wait(&counter, 1);

		/*
		 * Then, create another task and let it be in runnable state
		 */
		ret = mars_task_create(mars_ctx, &task3_id, NULL,
				       mpu3_prog.elf_image,
				       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		ret = mars_task_schedule(&task3_id, &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*
		 * Then release the waiter task so that it can call mars_task_event_flag_wait
		 */
		mars_test_counter_add(&counter, 1); /* going to be 2 */

		/*
		 * Clean up for the next iternation
		 */
		ret = mars_task_wait(&task2_id, &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		ret = mars_task_destroy(&task2_id);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_wait(&task3_id, &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		ret = mars_task_destroy(&task3_id);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_event_flag_destroy(eventflag);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		mars_test_counter_set(&counter, 0);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

