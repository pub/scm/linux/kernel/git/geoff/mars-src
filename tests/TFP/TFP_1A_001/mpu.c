/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <string.h>
#include <spu_mfcio.h>
#include <mars/task.h>
#include "mars_task_test.h"

static inline void dma_wait(uint32_t tag)
{
	mfc_write_tag_mask(1 << tag);
	mfc_write_tag_update_all();
	mfc_read_tag_status();
	mfc_sync(tag);
}

static inline void dma_get_and_wait(void *ls, uint64_t ea,
					uint32_t size, uint32_t tag)
{
	mfc_get((volatile void *)ls, ea, size, tag, 0, 0);
	dma_wait(tag);
}

static struct mars_task_id task_id __attribute__((aligned(16)));

int mars_task_main(const struct mars_task_args *task_args)
{
	uint64_t task_id_ea = task_args->type.u64[0];

	dma_get_and_wait(&task_id, task_id_ea, sizeof(task_id), 0);

	MARS_TEST_ASSERT(
		memcmp(mars_task_get_id(), &task_id, sizeof(task_id)) == 0);

	mars_task_exit(0);

	return 0;
}
