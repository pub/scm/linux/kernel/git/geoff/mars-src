/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

#define EVENT_FLAG_SIZE 32

extern spe_program_handle_t mpu_prog;

static struct mars_context *mars_ctx;
/*
 * 0: MARS_TASK_EVENT_FLAG_CLEAR_AUTO
 * 1: MARS_TASK_EVENT_FLAG_CLEAR_MANUAL
 */
static struct mars_task_id task_id[EVENT_FLAG_SIZE][2];
static struct mars_task_args task_args;
/*
 * 0: MARS_TASK_EVENT_FLAG_CLEAR_AUTO	MARS_TASK_EVENT_FLAG_MASK_AND
 * 1: MARS_TASK_EVENT_FLAG_CLEAR_AUTO	MARS_TASK_EVENT_FLAG_MASK_OR
 * 2: MARS_TASK_EVENT_FLAG_CLEAR_MANUAL	MARS_TASK_EVENT_FLAG_MASK_AND
 * 3: MARS_TASK_EVENT_FLAG_CLEAR_MANUAL	MARS_TASK_EVENT_FLAG_MASK_OR
 */
static uint64_t mpu_to_host[EVENT_FLAG_SIZE][4];


static void check_flag(uint64_t ev_and, uint64_t ev_or, uint32_t flag)
{
	int ret, i;
	uint32_t chk_bit;

	chk_bit = 1;
	for (i = 0; i < EVENT_FLAG_SIZE; i++) {
		/* MASK_AND */
		ret = mars_task_event_flag_try_wait(
			ev_and,
			chk_bit,
			MARS_TASK_EVENT_FLAG_MASK_AND, NULL);
		if (chk_bit == flag)
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		else
			MARS_TEST_ASSERT_ERROR(ret, MARS_ERROR_BUSY);

		/* MASK_OR */
		ret = mars_task_event_flag_try_wait(
			ev_or,
			chk_bit,
			MARS_TASK_EVENT_FLAG_MASK_OR, NULL);
		if (chk_bit == flag)
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		else
			MARS_TEST_ASSERT_ERROR(ret, MARS_ERROR_BUSY);

		chk_bit <<= 1;
	}

}

int main(void)
{
	int ret, i, j;
	int32_t exit_code;
	uint32_t chk_bit;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < EVENT_FLAG_SIZE; i++) {
		for (j = 0; j < 2; j++) {
			/* CLEAR_AUTO */
			ret = mars_task_event_flag_create(
				mars_ctx,
				&mpu_to_host[i][j],
				MARS_TASK_EVENT_FLAG_MPU_TO_HOST,
				MARS_TASK_EVENT_FLAG_CLEAR_AUTO);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			/* CLEAR_MANUAL */
			ret = mars_task_event_flag_create(
				mars_ctx,
				&mpu_to_host[i][j + 2],
				MARS_TASK_EVENT_FLAG_MPU_TO_HOST,
				MARS_TASK_EVENT_FLAG_CLEAR_MANUAL);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		}
	}

	chk_bit = 1;
	for (i = 0; i < EVENT_FLAG_SIZE; i++) {
		/* AUTO */
		ret = mars_task_create(mars_ctx, &task_id[i][0], NULL,
			mpu_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		task_args.type.u64[0] = mpu_to_host[i][0];
		task_args.type.u64[1] = mpu_to_host[i][1];
		task_args.type.u64[2] = (uint64_t)chk_bit;
		ret = mars_task_schedule(&task_id[i][0], &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/* MANUAL */
		ret = mars_task_create(mars_ctx, &task_id[i][1], NULL,
			mpu_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		task_args.type.u64[0] = mpu_to_host[i][2];
		task_args.type.u64[1] = mpu_to_host[i][3];
		task_args.type.u64[2] = (uint64_t)chk_bit;
		ret = mars_task_schedule(&task_id[i][1], &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		chk_bit <<= 1;
	}

	/* wait for flag set */
	for (i = 0; i < EVENT_FLAG_SIZE; i++) {
		ret = mars_task_wait(&task_id[i][0], &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

		ret = mars_task_wait(&task_id[i][1], &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

		ret = mars_task_destroy(&task_id[i][0]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_destroy(&task_id[i][1]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	chk_bit = 1;
	for (i = 0; i < EVENT_FLAG_SIZE; i++) {
		check_flag(mpu_to_host[i][0],
				mpu_to_host[i][1], chk_bit);
		check_flag(mpu_to_host[i][2],
				mpu_to_host[i][3], chk_bit);

		chk_bit <<= 1;
	}

	for (i = 0; i < EVENT_FLAG_SIZE; i++) {
		for (j = 0; j < 2; j++) {
			ret = mars_task_event_flag_destroy(
				mpu_to_host[i][j]);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
			ret = mars_task_event_flag_destroy(
				mpu_to_host[i][j + 2]);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		}
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

