/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

#define NUM_TASKS		10
#define SEMAPHORE_COUNTS	5

extern spe_program_handle_t mpu1_prog;
extern spe_program_handle_t mpu2_prog;

static struct mars_context *mars_ctx;
static struct mars_task_id task_id[NUM_TASKS];
static struct mars_task_args task_args;
static uint64_t semaphore;
static uint32_t notify[32] __attribute__((aligned(128)));

int main(void)
{
	int ret, i;
	int32_t exit_code;

	MARS_TEST_ASSERT( \
		MARS_TASK_SEMAPHORE_WAIT_MAX >= (NUM_TASKS * SEMAPHORE_COUNTS));
	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_semaphore_create(
		mars_ctx,
		&semaphore,
		NUM_TASKS * SEMAPHORE_COUNTS);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/* acuire all semaphores */
	ret = mars_task_create(mars_ctx, &task_id[0], NULL,
		mpu1_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	task_args.type.u64[0] = semaphore;
	task_args.type.u64[1] = NUM_TASKS * SEMAPHORE_COUNTS;
	ret = mars_task_schedule(&task_id[0], &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_wait(&task_id[0], &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id[0]);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/* main tasks */
	notify[0] = 0;
	for (i = 0; i < NUM_TASKS; i++) {
		char name[MARS_TASK_NAME_LEN_MAX];

		snprintf(name, MARS_TASK_NAME_LEN_MAX, "Task %d", i + 1);
		ret = mars_task_create(mars_ctx, &task_id[i], name,
			mpu2_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		task_args.type.u64[0] = semaphore;
		task_args.type.u64[1] = mars_ptr_to_ea(&notify);
		task_args.type.u64[2] = SEMAPHORE_COUNTS;
		ret = mars_task_schedule(&task_id[i], &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	/* run task */
	mars_test_counter_set(&notify[0], 1);

	for (i = 0; i < NUM_TASKS; i++) {
		ret = mars_task_wait(&task_id[i], &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

		ret = mars_task_destroy(&task_id[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	/* acuire all semaphores */
	ret = mars_task_create(mars_ctx, &task_id[0], NULL,
		mpu1_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	task_args.type.u64[0] = semaphore;
	task_args.type.u64[1] = NUM_TASKS * SEMAPHORE_COUNTS;
	ret = mars_task_schedule(&task_id[0], &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_wait(&task_id[0], &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id[0]);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_semaphore_destroy(semaphore);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

