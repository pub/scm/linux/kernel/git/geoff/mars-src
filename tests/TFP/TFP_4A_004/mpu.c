/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <mars_task_test.h>
#include <spu_mfcio.h>

#define TEST_DIFF 1
struct queue_entry {
	uint64_t val1;
	uint64_t val2;
};
static struct queue_entry data;

int mars_task_main(const struct mars_task_args *task_args)
{
	int ret;
	uint64_t host_to_mpu_ea = task_args->type.u64[0];
	uint64_t notify_ea = task_args->type.u64[1];
	uint64_t go_ea = task_args->type.u64[2];
	uint64_t val;

	mars_test_counter_add(notify_ea, 1);
	mars_test_counter_wait(go_ea, 1);

	ret = mars_task_queue_pop(host_to_mpu_ea, &data);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/* check queue data */
	val = data.val2 - data.val1;
	MARS_TEST_ASSERT_EQUAL(val, TEST_DIFF);

	mars_task_exit(1 << data.val1);

	return 0;
}
