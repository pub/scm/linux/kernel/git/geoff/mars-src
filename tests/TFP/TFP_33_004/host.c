/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

#define EVENT_FLAG_SIZE 32
extern spe_program_handle_t mpu_prog;
#define NUM_PATTERNS 5
static uint32_t pattern[] = {
	0x00000001UL,
	0x0000CAFEUL,
	0x10203040UL,
	0x5A5A5A5AUL,
	0xFFFFFFFFUL
};
static struct mars_context *mars_ctx;
static struct mars_task_id task_id;
static struct mars_task_args task_args;
static struct mars_task_args thread_args[EVENT_FLAG_SIZE];
static uint64_t host_to_mpu[NUM_PATTERNS];

static pthread_t thread[EVENT_FLAG_SIZE];
static pthread_barrier_t barrier;

static void *thread_proc(void *ptr)
{
	int ret;
	struct mars_task_args *task_args = (struct mars_task_args *)ptr;
	uint32_t i = (uint32_t)task_args->type.u32[0];
	uint32_t chk_bit = task_args->type.u32[1];

	/* wait */
	pthread_barrier_wait(&barrier);

	/* clear bits */
	ret = mars_task_event_flag_clear(
		host_to_mpu[i],
		chk_bit
	);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return NULL;
}

int main(void)
{
	int ret, i, bits;
	int32_t exit_code;
	uint32_t chk_bit, pat;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/* clear each bits one bye one */
	for (i = 0; i < NUM_PATTERNS; i++) {
		ret = mars_task_create(mars_ctx, &task_id, NULL,
			mpu_prog.elf_image,
			MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_event_flag_create(
			mars_ctx,
			&host_to_mpu[i],
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/* set all event flag */
		ret = mars_task_event_flag_set(host_to_mpu[i], (uint32_t)~0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/* create barrier */
		pat = pattern[i];
		for (bits = 0; pat; pat >>= 1)
			if (pat & 0x01)
				bits++;
		pthread_barrier_init(&barrier, NULL, bits);

		/* create threads */
		chk_bit = 0x01;
		pat = pattern[i];
		for (bits = 0; pat; bits++, pat >>= 1) {
			if (pat & 0x01) {
				thread_args[bits].type.u32[0] = (uint32_t)i;
				thread_args[bits].type.u32[1] = chk_bit;
				pthread_create(&thread[bits], NULL,
					thread_proc, &thread_args[bits]);
			}
			chk_bit <<= 1;
		}

		pat = pattern[i];
		for (bits = 0; pat; bits++, pat >>= 1) {
			if (pat & 0x01)
				pthread_join(thread[bits], NULL);
		}

		pthread_barrier_destroy(&barrier);

		/* check event flag */
		task_args.type.u64[0] = host_to_mpu[i];
		task_args.type.u64[1] = (uint64_t)pattern[i];
		ret = mars_task_schedule(&task_id, &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_wait(&task_id, &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

		ret = mars_task_destroy(&task_id);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_event_flag_destroy(host_to_mpu[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}
	/* clear same bits from multi threads*/
	for (i = 0; i < NUM_PATTERNS; i++) {
		ret = mars_task_create(mars_ctx, &task_id, NULL,
			mpu_prog.elf_image,
			MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_event_flag_create(
			mars_ctx,
			&host_to_mpu[i],
			MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/* set all event flag */
		ret = mars_task_event_flag_set(host_to_mpu[i], (uint32_t)~0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/* create barrier */
		pthread_barrier_init(&barrier, NULL, EVENT_FLAG_SIZE);

		/* create threads */
		for (bits = 0; bits < EVENT_FLAG_SIZE; bits++) {
			thread_args[bits].type.u32[0] = (uint32_t)i;
			thread_args[bits].type.u32[1] = pattern[i];
			pthread_create(&thread[bits], NULL,
				thread_proc, &thread_args[bits]);
		}

		for (bits = 0; bits < EVENT_FLAG_SIZE; bits++)
			pthread_join(thread[bits], NULL);

		pthread_barrier_destroy(&barrier);

		/* check event flag */
		task_args.type.u64[0] = host_to_mpu[i];
		task_args.type.u64[1] = (uint64_t)pattern[i];
		ret = mars_task_schedule(&task_id, &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_wait(&task_id, &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

		ret = mars_task_destroy(&task_id);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_event_flag_destroy(host_to_mpu[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

