/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>

extern spe_program_handle_t mpu_prog;

static struct mars_context *mars;
static struct mars_task_id task_id;
static struct mars_task_args task_args;

int main(void)
{
	int exit_code, ret;

	ret = mars_context_create(&mars, 1, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(mars, &task_id, "13002", mpu_prog.elf_image, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
#if __BYTE_ORDER == __BIG_ENDIAN
	task_args.type.u64[0] = 0x0123456789ABCDEFULL;
	task_args.type.u64[1] = 0x123456789ABCDEF0ULL;
	task_args.type.u64[2] = 0x23456789ABCDEF01ULL;
	task_args.type.u64[3] = 0x3456789ABCDEF012ULL;
#elif __BYTE_ORDER == __LITTLE_ENDIAN
	task_args.type.u64[0] = 0xEFCDAB8967452301ULL;
	task_args.type.u64[1] = 0xF0DEBC9A78563412ULL;
	task_args.type.u64[2] = 0x01EFCDAB89674523ULL;
	task_args.type.u64[3] = 0x12F0DEBC9A785634ULL;
#endif
	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_wait(&task_id, &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_EQUAL(exit_code, 0x01234567);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
