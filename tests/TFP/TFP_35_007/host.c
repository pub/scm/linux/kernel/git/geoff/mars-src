/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>
#include "common.h"

extern spe_program_handle_t mpu_prog;

static uint32_t patterns[] = {
	0x0000000001,
	0x0000CAFEUL,
	0x10203040UL,
};

static	uint8_t modes[] = {
		MARS_TASK_EVENT_FLAG_MASK_AND,
		MARS_TASK_EVENT_FLAG_MASK_OR,
};

#define ARRYSIZE(x) (sizeof(x) / sizeof(x[0]))

static struct mars_context *mars_ctx;
static struct mars_task_id task_id;
static struct mars_task_args task_args;
static uint64_t eventflag;
static uint32_t notify __attribute((aligned(128)));

uint32_t make_wait_pattern(uint8_t wait_mode, uint32_t asserted_pattern)
{
	int i;
	uint32_t ret;

	if (wait_mode == MARS_TASK_EVENT_FLAG_MASK_AND)
		return asserted_pattern;

	/*
	 * find the first asserted bit
	 */
	ret = 0;
	for (i = 0; i < (sizeof(asserted_pattern) * 8); i++) {
		ret = 1 << i;
		if (asserted_pattern & ret)
			break;
	}
	return ret;
};

int main(void)
{
	int ret, pattern, mode;
	int32_t exit_code;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (mode = 0; mode < ARRYSIZE(modes); mode++) {
		for (pattern = 0; pattern < ARRYSIZE(patterns); pattern++) {
			/*
			 * Create a HOST to MPU event flag
			 * with AUTO clear mode
			 */
			ret = mars_task_event_flag_create(
				mars_ctx,
				&eventflag,
				MARS_TASK_EVENT_FLAG_HOST_TO_MPU,
				MARS_TASK_EVENT_FLAG_CLEAR_AUTO);
			/* If fails, can't continue the test */
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			/*
			 * Create a task which will wait for
			 * the event flag
			 */
			ret = mars_task_create(mars_ctx, &task_id, NULL,
					       mpu_prog.elf_image,
					       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
			/* If fails, can't continue the test */
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			/*
			 * Setup arguments to MPU
			 */
			task_args.type.u64[0] = eventflag;
			task_args.type.u64[1] = (uint64_t)make_wait_pattern(modes[mode],
									    patterns[pattern]);
			task_args.type.u64[2] = mars_ptr_to_ea(&notify);
			task_args.type.u64[3] = (uint64_t)modes[mode]; /* waiting mode */

			/*
			 * Start MPU program
			 */
			ret = mars_task_schedule(&task_id, &task_args, 0);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			/*
			 * Wait for the MPU program to be ready
			 */
			mars_test_counter_wait(&notify, STATE_READY);

			/*
			 * Make the event flag asserted
			 */
			ret = mars_task_event_flag_set(eventflag, patterns[pattern]);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			/*
			 * Tell the MPU program to start waiting the event flag
			 */
			mars_test_counter_set(&notify, STATE_EVENT_SET);

			/*
			 * If everything goes OK, the MPU program exits immediately.
			 * If not, it may never return. The test framework
			 * will detect it as a timeout and kill us
			 */
			ret = mars_task_wait(&task_id, &exit_code);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
			MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

			/*
			 * cleanup
			 */
			ret = mars_task_destroy(&task_id);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			ret = mars_task_event_flag_destroy(eventflag);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		}
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

