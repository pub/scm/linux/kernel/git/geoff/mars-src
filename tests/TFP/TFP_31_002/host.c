/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

extern spe_program_handle_t mpu_prog;

static struct mars_context *mars_ctx;
/*
 * 0: MARS_TASK_EVENT_FLAG_CLEAR_AUTO	MARS_TASK_EVENT_FLAG_MASK_AND
 * 1: MARS_TASK_EVENT_FLAG_CLEAR_AUTO	MARS_TASK_EVENT_FLAG_MASK_OR
 * 2: MARS_TASK_EVENT_FLAG_CLEAR_MANUAL	MARS_TASK_EVENT_FLAG_MASK_AND
 * 3: MARS_TASK_EVENT_FLAG_CLEAR_MANUAL	MARS_TASK_EVENT_FLAG_MASK_OR
 */
static uint64_t mpu_to_host[4];

#define EVENT_FLAG_SIZE 32

static void check_flag(uint64_t ev1, uint64_t ev2)
{
	int ret, i;
	uint32_t chk_bit;

	chk_bit = 1;
	for (i = 0; i < EVENT_FLAG_SIZE; i++) {
		/* MASK_AND */
		ret = mars_task_event_flag_try_wait(
			ev1,
			chk_bit,
			MARS_TASK_EVENT_FLAG_MASK_AND, NULL);
		MARS_TEST_ASSERT_ERROR(ret, MARS_ERROR_BUSY);

		/* MASK_OR */
		ret = mars_task_event_flag_try_wait(
			ev2,
			chk_bit,
			MARS_TASK_EVENT_FLAG_MASK_OR, NULL);
		MARS_TEST_ASSERT_ERROR(ret, MARS_ERROR_BUSY);

		chk_bit <<= 1;
	}
}

int main(void)
{
	int ret, i;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < 2; i++) {
		/* CLEAR_AUTO */
		ret = mars_task_event_flag_create(
			mars_ctx,
			&mpu_to_host[i],
			MARS_TASK_EVENT_FLAG_MPU_TO_HOST,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/* CLEAR_MANUAL */
		ret = mars_task_event_flag_create(
			mars_ctx,
			&mpu_to_host[i + 2],
			MARS_TASK_EVENT_FLAG_MPU_TO_HOST,
			MARS_TASK_EVENT_FLAG_CLEAR_MANUAL);
	}

	/* check event flags */
	check_flag(mpu_to_host[0], mpu_to_host[1]);
	check_flag(mpu_to_host[2], mpu_to_host[3]);

	for (i = 0; i < 2; i++) {
		ret = mars_task_event_flag_destroy(mpu_to_host[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		ret = mars_task_event_flag_destroy(mpu_to_host[i + 2]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

