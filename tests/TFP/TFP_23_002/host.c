/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

#define WAIT_NUM (MARS_TASK_BARRIER_WAIT_MAX / 2)
#define NUM_TASKS (WAIT_NUM * 2)

extern spe_program_handle_t mpu_prog;

static struct mars_context *mars_ctx;
static struct mars_task_id task_id[NUM_TASKS];
static struct mars_task_args task_args;
static uint64_t barrier;
static uint32_t notify __attribute((aligned(128)));
static uint32_t counter __attribute((aligned(16)));


int main(void)
{
	int ret, i, exited, blocked;
	int32_t exit_code;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/*
	 * Create a barrier
	 */
	ret = mars_task_barrier_create(mars_ctx, &barrier, WAIT_NUM);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/*
	 * Create and start tasks
	 */
	for (i = 0; i < NUM_TASKS; i++) {
		ret = mars_task_create(mars_ctx, &task_id[i], NULL,
				       mpu_prog.elf_image,
				       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		task_args.type.u64[0] = barrier;
		task_args.type.u64[1] = mars_ptr_to_ea(&notify);
		task_args.type.u64[2] = mars_ptr_to_ea(&counter);
		ret = mars_task_schedule(&task_id[i], &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	/* tell the tasks to notify barrier */
	mars_test_counter_set(&notify, 1);

	/*
	 * If mars_task_bariier_notify() works as expected,
	 * the number of WAIT_NUM tasked should increment the counter,
	 * and the rest should be blocked.
	 */
	mars_test_counter_wait(&counter, WAIT_NUM);

	/*
	 * Check the state of the tasks
	 */
	do {
		exited = 0;
		blocked = 0;
		for (i = 0; i < NUM_TASKS; i++) {
			ret = mars_task_try_wait(&task_id[i], &exit_code);
			switch (ret) {
			case MARS_SUCCESS:
				exited ++;
				break;
			case MARS_ERROR_BUSY:
				blocked ++;
				break;
			default:
				MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
				break;
			}
		}
	} while (exited != WAIT_NUM || blocked != (NUM_TASKS - WAIT_NUM));

	/* blocking MPU tasks can not be destroyed by host... */
	exit(0);
}

