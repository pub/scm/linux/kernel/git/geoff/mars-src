/*
 * Copyright 2008 Sony Computer Entertainment Inc.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>

#include "mars_task_test.h"
#include "common.h"

int mars_task_main(const struct mars_task_args *task_args)
{
	int ret, task_ret, i;
	uint64_t host_callback_ea = task_args->type.u64[0];
	uint64_t mpu2_prog_elf_image_ea = task_args->type.u64[1];
	uint64_t mpu3_prog_elf_image_ea = task_args->type.u64[2];
	uint64_t queue_ea;
	uint64_t push_task_id_ea[TASK_COUNT];
	uint64_t pop_task_id_ea[TASK_COUNT];
	struct mars_callback_args in, out;
	struct mars_task_id push_task_id[TASK_COUNT];
	struct mars_task_id pop_task_id[TASK_COUNT];
	struct mars_task_args task_arg;

	in.type.u32[0] = TASK_QUEUE_CREATE;
	in.type.u32[1] = TASK_COUNT;
	ret = mars_task_call_host(host_callback_ea, &in, &out);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	queue_ea = out.type.u64[0];

	in.type.u32[0] = TASK_CREATE;
	in.type.u32[1] = MARS_TASK_CONTEXT_SAVE_SIZE_MAX;

	for (i = 0; i < TASK_COUNT; i++) {
		in.type.u64[2] = mpu2_prog_elf_image_ea;
		ret = mars_task_call_host(host_callback_ea, &in, &out);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		push_task_id_ea[i] = out.type.u64[0];
		mfc_get(&push_task_id[i], push_task_id_ea[i], 
			sizeof(struct mars_task_id), 0,0,0);
		mfc_write_tag_mask(1 << 0);
		mfc_read_tag_status_all();

		in.type.u64[2] = mpu3_prog_elf_image_ea;
		ret = mars_task_call_host(host_callback_ea, &in, &out);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		pop_task_id_ea[i] = out.type.u64[0];
		mfc_get(&pop_task_id[i], pop_task_id_ea[i], 
			sizeof(struct mars_task_id), 0,0,0);
		mfc_write_tag_mask(1 << 0);
		mfc_read_tag_status_all();
	}

	task_arg.type.u64[0] = queue_ea;

	for (i = 0; i < TASK_COUNT; i++) {
		ret = mars_task_schedule(&push_task_id[i], &task_arg, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_schedule(&pop_task_id[i], &task_arg, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	for (i = 0; i < TASK_COUNT; i++) {
		ret = mars_task_wait(&push_task_id[i], &task_ret);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(task_ret, MARS_SUCCESS);

		ret = mars_task_wait(&pop_task_id[i], &task_ret);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT_ERROR(task_ret, MARS_SUCCESS);
	}

	in.type.u32[0] = TASK_DESTROY;

	for (i = 0; i < TASK_COUNT; i++) {
		in.type.u64[1] = push_task_id_ea[i];
		ret = mars_task_call_host(host_callback_ea, &in, NULL);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		in.type.u64[1] = pop_task_id_ea[i];
		ret = mars_task_call_host(host_callback_ea, &in, NULL);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	in.type.u32[0] = TASK_QUEUE_DESTROY;
	in.type.u64[1] = queue_ea;
	ret = mars_task_call_host(host_callback_ea, &in, NULL);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return MARS_SUCCESS;
}
