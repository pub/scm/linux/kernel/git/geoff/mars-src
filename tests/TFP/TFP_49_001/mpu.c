/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <mars_task_test.h>

struct queue_entry {
	uint64_t val;
	uint64_t pad;
};
static struct queue_entry data;

int mars_task_main(const struct mars_task_args *task_args)
{
	int ret;
	uint64_t mpu_to_host_ea = task_args->type.u64[0];
	uint32_t num_push = (uint32_t)task_args->type.u64[1];
	uint32_t i;

	for (i = 0; i < num_push; i++) {
		data.val += i;
		ret = mars_task_queue_push(mpu_to_host_ea, &data);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	/* try_push depth + 1 should be BUSY */
	ret = mars_task_queue_try_push_begin(mpu_to_host_ea, &data, 1);
	MARS_TEST_ASSERT_ERROR(ret, MARS_ERROR_BUSY);

	return 0;
}
