/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>

#define THREAD_COUNT 16

static pthread_t thread_handles_1[THREAD_COUNT];
static pthread_t thread_handles_2[THREAD_COUNT];
static struct mars_context *mars_contexts_1[THREAD_COUNT];
static struct mars_context *mars_contexts_2[THREAD_COUNT];

void *thread_proc(void *arg)
{
	int ret;

	ret = mars_context_create((struct mars_context **)arg, 1, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(*((struct mars_context **)arg));
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return NULL;
}

int main(void)
{
	int i;

	for (i = 0; i < THREAD_COUNT; i++)
		pthread_create(
			&thread_handles_1[i],
			NULL,
			thread_proc,
			(void *)&mars_contexts_1[i]);

	for (i = 0; i < THREAD_COUNT; i++)
		pthread_create(
			&thread_handles_2[i],
			NULL,
			thread_proc,
			(void *)&mars_contexts_2[i]);

	for (i = 0; i < THREAD_COUNT; i++)
		pthread_join(thread_handles_1[i], NULL);

	for (i = 0; i < THREAD_COUNT; i++)
		pthread_join(thread_handles_2[i], NULL);

	return 0;
}
