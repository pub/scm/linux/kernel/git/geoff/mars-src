/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>
#include "host_and_mpu.h"

extern spe_program_handle_t mpu_prog;

static struct mars_context *mars;
static struct mars_task_id task_id;
static struct mars_task_args task_args;
static uint64_t queue;
static uint64_t	queue_item[2] __attribute__((aligned(16)));
static uint32_t	mpu_queue_counter __attribute__((aligned(16)));
static uint32_t	mpu_state __attribute__((aligned(16)));

int main(void)
{
	int exit_code, ret, i;

	/* assure queue item size */
	MARS_TEST_ASSERT_EQUAL(sizeof(queue_item), QUEUE_ITEM_SIZE);

	/*
	 * Create context
	 */
	ret = mars_context_create(&mars, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/*
	 * Create task
	 */
	ret = mars_task_create(
		mars,
		&task_id,
		"46001",
		mpu_prog.elf_image,
		MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	/* if this fails, we can not continue no more */
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/*
	 * Create a queue
	 */
	ret = mars_task_queue_create(
		mars,
		&queue,
		QUEUE_ITEM_SIZE,
		QUEUE_DEPTH,
		MARS_TASK_QUEUE_MPU_TO_HOST);
	/* if this fails, we can not continue no more */
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/*
	 * Setup arguments to MPU
	 */
	task_args.type.u64[0] = queue;
	task_args.type.u64[1] = mars_ptr_to_ea(&mpu_queue_counter);
	task_args.type.u64[2] = mars_ptr_to_ea(&mpu_state);
	mars_test_counter_set(&mpu_queue_counter, 0);
	mars_test_counter_set(&mpu_state, STATE_NONE);

	/*
	 * Start the task.  The task will repeat QUEUE_DEPTH+1 times of:
	 * o Set mpu_state to STATE_START_PUSH before every calling
	 *   mars_task_queue_push_begin
	 * o Set mpu_state to STATE_PUSH_DONE after every return of
	 *   mars_task_queue_push_end
	 * o Increment mpu_queue_counter one by one
	 * So, MPU program should blocks after the mpu_queue_counter reaches
	 * to QUEUE_DEPTH.
	 */
	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/*
	 * As we described above, the MPU program will be in sleep state after
	 * the counter reaches QUEUE_DEPTH
	 */
	mars_test_counter_wait(&mpu_queue_counter, QUEUE_DEPTH);
	/*
	 * Then the pusher should be blocked soon after state goes
	 * STATE_START_PUSH.  If something wrong, the following will
	 * not be satisfied forever.  Then timeout detected.
	 */
	mars_test_counter_wait(&mpu_state, STATE_START_PUSH);

	/*
	 * If the last push of the pusher is not blocked, the state
	 * or counter is in wrong value.
	 * DISCLAIMER: If the last push operation takes too long time and is not
	 * blocked, the following ASSERT may be satisfied.  If you want to
	 * detect that, insert sleep() here.
	 */
	MARS_TEST_ASSERT_EQUAL(mpu_state, STATE_START_PUSH);
	MARS_TEST_ASSERT_EQUAL(mpu_queue_counter, QUEUE_DEPTH);

	/*
	 * check the queue contents including the last item
	 */
	for (i = 0; i < QUEUE_DEPTH + 1; i++) {
		ret = mars_task_queue_pop(queue, queue_item);

		MARS_TEST_ASSERT(queue_item[0] == i && queue_item[1] == i);
	}

	/*
	 * Now queue shoud be empty. Check it
	 */
	ret = mars_task_queue_try_pop(queue, queue_item);
	MARS_TEST_ASSERT_ERROR(ret, MARS_ERROR_BUSY);

	/*
	 * Wait for the task exit
	 */
	ret = mars_task_wait(&task_id, &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_EQUAL(exit_code, 0);

	/*
	 * cleanup
	 */
	ret = mars_task_queue_destroy(queue);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
