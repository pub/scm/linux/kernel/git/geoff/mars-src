/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

#define NUM_TASKS MARS_TASK_BARRIER_WAIT_MAX

extern spe_program_handle_t mpu_prog;

static struct mars_context *mars_ctx;
static struct mars_task_id task_id[NUM_TASKS];
static struct mars_task_args task_args[NUM_TASKS];
static uint64_t barrier[NUM_TASKS];

int main(void)
{
	int ret, i, count;
	int32_t exit_code;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < NUM_TASKS; i++) {
		ret = mars_task_barrier_create(mars_ctx, &barrier[i], i + 1);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/* create tasks and notify */
		for (count = 0; count < i + 1; count++) {
			ret = mars_task_create(mars_ctx, &task_id[count], NULL,
			mpu_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

			task_args[count].type.u64[0] = barrier[i];
			ret = mars_task_schedule(
					&task_id[count], &task_args[count], 0);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		}

		for (count = 0; count < i + 1; count++) {
			ret = mars_task_wait(&task_id[count], &exit_code);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
			MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

			ret = mars_task_destroy(&task_id[count]);
			MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		}
		ret = mars_task_barrier_destroy(barrier[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}

