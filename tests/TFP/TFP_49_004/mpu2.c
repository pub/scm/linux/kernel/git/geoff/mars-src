/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <mars_task_test.h>

struct queue_entry {
	uint64_t val1;
	uint64_t val2;
};
static struct queue_entry data;

int mars_task_main(const struct mars_task_args *task_args)
{
	int ret, i;
	uint64_t id = task_args->type.u32[0];
	uint64_t num = task_args->type.u32[1];
	uint64_t mpu_to_mpu_ea = task_args->type.u64[1];
	uint64_t sum, sum_q;
	uint32_t count;

	sum = sum_q = 0;
	for (i = 0; i < num; i++) {
		ret = mars_task_queue_pop(mpu_to_mpu_ea, &data);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		MARS_TEST_ASSERT_EQUAL(data.val1, id);
		sum += i;
		sum_q += data.val2;
	}

	/* check queue data */
	MARS_TEST_ASSERT_EQUAL(sum, sum_q);

	/* queue should be empty */
	ret = mars_task_queue_count(mpu_to_mpu_ea, &count);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_EQUAL(count, 0);

	return 0;
}
