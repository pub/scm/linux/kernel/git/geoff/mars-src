/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <pthread.h>
#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

extern spe_program_handle_t mpu_prog;

static uint32_t patterns[] = {
	0x00000001UL,
	0x10203040UL,
	0x5A5A5A5AUL,
	0xFFFFFFFFUL
};

#define ARRYSIZE(a) (sizeof(a)/sizeof(a[0]))
#define MAX_THREADS (sizeof(uint32_t) * 8)

struct thread_arg {
	uint32_t clear_pattern;
};

static struct mars_context *mars_ctx;
static struct mars_task_id task_id;
static pthread_t thread_id[MAX_THREADS];
static struct mars_task_args task_args;
static struct thread_arg thread_arg[MAX_THREADS];
static uint64_t event_flag;
static pthread_barrier_t pthread_barrier;


void * clear_thread(void * v)
{
	struct thread_arg *arg = v;
	int ret;

	/*
	 * Try to clear the event flag simultaenously with other threads
	 * As the last waited thread will get PTHREAD_SERIAL_THREAD,
	 * we can not test with MARS_TEST_ASSERT_EQUAL_INT(ret, 0)
	 */
	pthread_barrier_wait(&pthread_barrier);

	/*
	 * Clear the specified bit
	 */
	ret = mars_task_event_flag_clear(event_flag, arg->clear_pattern);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return NULL;
};

int main(void)
{
	int ret, i, j;
	int32_t exit_code;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	/*
	 * Try the test with many patterns
	 */
	for (i = 0; i < ARRYSIZE(patterns); i++) {
		int bits;
		uint32_t returned_pattern;

		/*
		 * Create an event flag with the mode of 'MPU_to_HOST'
		 */
		ret = mars_task_event_flag_create(
			mars_ctx,
			&event_flag,
			MARS_TASK_EVENT_FLAG_MPU_TO_HOST,
			MARS_TASK_EVENT_FLAG_CLEAR_AUTO);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*
		 * Set the event flag with the specified pattern,
		 * that is, set the event flag asserted.
		 */
		ret = mars_task_create(mars_ctx, &task_id, NULL,
				       mpu_prog.elf_image,
				       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		task_args.type.u32[0] = patterns[i];
		task_args.type.u64[1] = event_flag;

		ret = mars_task_schedule(&task_id, &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_wait(&task_id, &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		ret = mars_task_destroy(&task_id);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		/*
		 * Counts the asserted bits and clearing bitmasks
		 */
		bits = 0;
		for (j = 0; j < MAX_THREADS; j++) {
			if (patterns[i] & (1 << j)) {
				thread_arg[bits].clear_pattern = 1 << j;
				bits++;
			}
		}
		/*
		 * Create pthread barriers to synchronize pthreads
		 */
		ret = pthread_barrier_init(&pthread_barrier, NULL, bits);
		MARS_TEST_ASSERT_ERROR(ret, 0);

		/*
		 * Create pthreads.
		 * They will clear the event flag each with the specified bit.
		 */
		for (j = 0; j < bits; j++) {
			ret = pthread_create(&thread_id[j], NULL, clear_thread,
					     &thread_arg[j]);
			MARS_TEST_ASSERT_EQUAL_INT(ret, 0);
		}
		/*
		 * Wait for the completion of the pthreads
		 */
		for (j = 0; j < bits; j++) {
			void * value_ptr;
			ret = pthread_join(thread_id[j], &value_ptr);
			MARS_TEST_ASSERT_EQUAL_INT(ret, 0);
		}

		/*
		 * Now the event flag should be reverted to unasserted,
		 * thus the condition is never met.  Test it.
		 */
		ret = mars_task_event_flag_try_wait(event_flag,
						    patterns[i],
						    MARS_TASK_EVENT_FLAG_MASK_OR,
						    &returned_pattern);
		MARS_TEST_ASSERT_ERROR(ret, MARS_ERROR_BUSY);

		/*
		 * Cleanup for the next iternation
		 */
		ret = pthread_barrier_destroy(&pthread_barrier);
		MARS_TEST_ASSERT_EQUAL_INT(ret, 0);

		ret = mars_task_event_flag_destroy(event_flag);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
