/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <libspe2.h>
#include <pthread.h>
#include <mars_test.h>

#define NUM_SPES	4

extern spe_program_handle_t mpu_prog;

void *thread_proc(void *arg)
{
	int ret;
	spe_context_ptr_t spe;
	unsigned int entry = SPE_DEFAULT_ENTRY;
	spe_stop_info_t stopinfo;

	spe = spe_context_create(0, NULL);
	MARS_TEST_ASSERT(spe);

	ret = spe_program_load(spe, &mpu_prog);
	MARS_TEST_ASSERT(ret == 0);

	ret = spe_context_run(spe, &entry, 0,
			      (void *)(uintptr_t)1, (void *)(uintptr_t)2, &stopinfo);
	MARS_TEST_ASSERT(ret == 0);
	MARS_TEST_ASSERT(stopinfo.stop_reason == SPE_EXIT);
	MARS_TEST_ASSERT(stopinfo.result.spe_exit_code == 0);

	ret = spe_context_destroy(spe);
	MARS_TEST_ASSERT(ret == 0);

	return NULL;
}

int main(void)
{
	int ret, i;
	pthread_t threads[NUM_SPES];

	for (i = 0; i < NUM_SPES; i++) {
		ret = pthread_create(&threads[i], NULL, thread_proc, NULL);
		MARS_TEST_ASSERT(ret == 0);
	}

	for (i = 0; i < NUM_SPES; i++) {
		ret = pthread_join(threads[i], NULL);
		MARS_TEST_ASSERT(ret == 0);
	}

	return 0;
}
