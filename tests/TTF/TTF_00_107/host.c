/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

#define NUM_SPES	4
#define NUM_TASKS	4

extern spe_program_handle_t mpu_prog;

static struct mars_task_id task_id[NUM_TASKS];
static struct mars_task_args task_args;

static uint32_t task_time[NUM_TASKS];

int main(void)
{
	int ret, i;
	struct mars_context *mars_ctx;
	uint32_t start, end;

	MARS_TEST_PERF_INIT();

	start = mars_get_ticks(); /* start measurement */
	ret = mars_context_create(&mars_ctx, NUM_SPES, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	for (i = 0; i < NUM_TASKS; i++) {
		char name[MARS_TASK_NAME_LEN_MAX];

		snprintf(name, MARS_TASK_NAME_LEN_MAX, "Task %d", i + 1);
		ret = mars_task_create(mars_ctx, &task_id[i], name,
			mpu_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		task_args.type.u64[0] = i;
		task_args.type.u64[1] = (uintptr_t)&task_time[i];

		ret = mars_task_schedule(&task_id[i], &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	for (i = 0; i < NUM_TASKS; i++) {
		int32_t exit_code;
		ret = mars_task_wait(&task_id[i], &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT(exit_code == 0);

		ret = mars_task_destroy(&task_id[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	end = mars_get_ticks(); /* end measurement */

	MARS_TEST_PERF_RESULT_PRINT("HOST", end - start);

	for (i = 0; i < NUM_TASKS; i++) {
		char name[10];
		sprintf(name, "TASK%u", i);
		MARS_TEST_PERF_RESULT_PRINT(name, task_time[i]);
	}

	return 0;
}


