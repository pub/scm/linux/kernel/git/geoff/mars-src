/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <libspe2.h>
#include <pthread.h>
#include <mars_test.h>

#include "test.h"

extern spe_program_handle_t mpu_prog;

static struct mars_task_id task_id[NUM_TASKS];
static struct mars_task_args task_args;
static pthread_t threads[NUM_THREADS];

static uint32_t notify[32] __attribute__((aligned(128)));

static void *thread_proc(void *arg)
{
	int index = (uintptr_t)arg;

	if (index == 0) {
		fprintf(stderr, "Thread %d: Sleep 3 seconds before incrementing counter.\n",
			index);
		sleep(3);
	}

	mars_test_counter_add(&notify[0], 1);

	mars_test_counter_wait(&notify[0], NUM_TASKS + NUM_THREADS + 2);

	return NULL;
}

int main(void)
{
	int ret, i;
	struct mars_context *mars_ctx;

	ret = mars_context_create(&mars_ctx, NUM_SPES, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	mars_test_counter_set(&notify[0], 1);

	for (i = 0; i < NUM_TASKS; i++) {
		char name[MARS_TASK_NAME_LEN_MAX];

		snprintf(name, MARS_TASK_NAME_LEN_MAX, "Task %d", i + 1);
		ret = mars_task_create(mars_ctx, &task_id[i], name,
			mpu_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

		task_args.type.u64[0] = (uintptr_t)&notify;
		ret = mars_task_schedule(&task_id[i], &task_args, 0);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	fprintf(stderr, "Sleep 3 seconds before notification.\n");
	sleep(3);
	mars_test_counter_set(&notify[0], 2);

	fprintf(stderr, "Waiting for notification from tasks.\n");
	mars_test_counter_wait(&notify[0], NUM_TASKS + 2);
	fprintf(stderr, "Done.\n");

	fprintf(stderr, "Starting threads.\n");
	for (i = 0; i < NUM_THREADS; i++) {
		ret = pthread_create(&threads[i], NULL,
				     thread_proc, (void *)(uintptr_t)i);
		MARS_TEST_ASSERT(ret == 0);
	}

	for (i = 0; i < NUM_THREADS; i++) {
		ret = pthread_join(threads[i], NULL);
		MARS_TEST_ASSERT(ret == 0);
	}
	fprintf(stderr, "Done.\n");

	fprintf(stderr, "Sleep 3 seconds before notification.\n");
	sleep(3);
	mars_test_counter_set(&notify[0], NUM_TASKS + NUM_THREADS + 3);

	fprintf(stderr, "Waiting for notification from tasks.\n");
	mars_test_counter_wait(&notify[0], NUM_TASKS + NUM_THREADS + 4);
	fprintf(stderr, "Done.\n");

	for (i = 0; i < NUM_TASKS; i++) {
		int32_t exit_code;
		ret = mars_task_wait(&task_id[i], &exit_code);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
		MARS_TEST_ASSERT(exit_code == 0);

		ret = mars_task_destroy(&task_id[i]);
		MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	}

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}


