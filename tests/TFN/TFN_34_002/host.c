/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

#define MARS_TASK_EVENT_FLAG_SIZE       128
#define MARS_TASK_EVENT_FLAG_ALIGN      128
#define MARS_TASK_EVENT_FLAG_ALIGN_MASK     0x7f

struct mars_task_event_flag_t {
    uint32_t lock;
    uint32_t bits;
    uint8_t direction;
    uint8_t clear_mode;
    uint16_t wait_count;
    uint16_t wait_id[MARS_TASK_EVENT_FLAG_WAIT_MAX + 1];
    uint32_t wait_mask[MARS_TASK_EVENT_FLAG_WAIT_MAX];
    uint8_t wait_mask_mode[MARS_TASK_EVENT_FLAG_WAIT_MAX + 1];
    uint64_t mars_context_ea;
} __attribute__((aligned(MARS_TASK_EVENT_FLAG_ALIGN)));


extern spe_program_handle_t mpu_prog;

static struct mars_context *mars_ctx;
static struct mars_task_id task_id;
static struct mars_task_args task_args;

struct mars_task_event_flag_t *internal_event_flag;

void init_event_flag(struct mars_context *mars_ctx, struct mars_task_event_flag_t *internal_event_flag){
	internal_event_flag->mars_context_ea = mars_ptr_to_ea(mars_ctx);
	internal_event_flag->bits = 0;
	internal_event_flag->direction = MARS_TASK_EVENT_FLAG_HOST_TO_MPU;
	internal_event_flag->clear_mode = MARS_TASK_EVENT_FLAG_CLEAR_MANUAL;
	internal_event_flag->wait_count = 0;
}

int main(void)
{
	int ret, exit_code;
	struct mars_task_event_flag *event_flag_base;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = posix_memalign((void **)&event_flag_base, MARS_TASK_EVENT_FLAG_ALIGN , MARS_TASK_EVENT_FLAG_SIZE*2);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	internal_event_flag = (struct mars_task_event_flag_t *)((char *)event_flag_base+4);
	init_event_flag(mars_ctx,internal_event_flag);

	ret = mars_task_create(mars_ctx, &task_id, NULL, mpu_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	task_args.type.u64[0] = (unsigned long)internal_event_flag;
	ret = mars_task_schedule( &task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_wait(&task_id, &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	internal_event_flag = (struct mars_task_event_flag_t *)((char *)event_flag_base);
	init_event_flag(mars_ctx,internal_event_flag);

	ret = mars_task_event_flag_destroy((unsigned long)internal_event_flag);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
