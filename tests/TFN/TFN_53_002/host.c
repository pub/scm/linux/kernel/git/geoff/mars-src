/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

#define MARS_TASK_SEMAPHORE_ALIGN               128
#define MARS_TASK_SEMAPHORE_SIZE               128

 struct mars_task_semaphore_t {
         uint32_t lock;
         int32_t count;
         uint16_t wait_count;
         uint16_t wait_id[MARS_TASK_SEMAPHORE_WAIT_MAX];
         uint8_t wait_head;
         uint8_t pad;
         uint64_t mars_context_ea;
 } __attribute__((aligned(MARS_TASK_SEMAPHORE_ALIGN)));

extern spe_program_handle_t mpu_prog;

static struct mars_context *mars_ctx;
static struct mars_task_id task_id;
static struct mars_task_args task_args;

struct mars_task_semaphore_t *internal_semaphore;

int init_semaphore(struct mars_context *mars_ctx, struct mars_task_semaphore_t *internal_semaphore, int count)
{
	internal_semaphore->mars_context_ea = mars_ptr_to_ea(mars_ctx);
	internal_semaphore->count = count;
	internal_semaphore->wait_count = 0;
	internal_semaphore->wait_head = 0;

	return 0;
}

int main(void)
{
	int ret, exit_code;
	struct mars_task_semaphore_t *semaphore_base;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(mars_ctx, &task_id, NULL, mpu_prog.elf_image,
			       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = posix_memalign((void **)&semaphore_base, MARS_TASK_SEMAPHORE_ALIGN , MARS_TASK_SEMAPHORE_SIZE*2);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	internal_semaphore = (struct mars_task_semaphore_t *)((char *)semaphore_base+4);

	ret = init_semaphore(mars_ctx, internal_semaphore,2);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	task_args.type.u64[0] = (unsigned long)internal_semaphore;
	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_wait(&task_id, &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	free(semaphore_base);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
