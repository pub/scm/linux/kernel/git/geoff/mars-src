/*
 * Copyright 2008 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <mars/task.h>
#include <libspe2.h>
#include <mars_test.h>

#define MARS_TASK_QUEUE_SIZE            128
#define MARS_TASK_QUEUE_ALIGN           128
//#define MARS_TASK_QUEUE_WAIT_MAX	1024
#define MARS_TASK_QUEUE_BUFFER_ALIGN	128

struct mars_task_queue_t {
    uint32_t lock;
    uint32_t size;
    uint32_t depth;
    uint32_t count;
    uint64_t buffer_ea;
    uint64_t push_ea;
    uint64_t pop_ea;
    uint8_t pad;
    uint8_t direction;
    uint8_t push_wait_head;
    uint8_t pop_wait_head;
    uint16_t push_wait_count;
    uint16_t pop_wait_count;
    uint16_t push_wait_id[MARS_TASK_QUEUE_WAIT_MAX];
    uint16_t pop_wait_id[MARS_TASK_QUEUE_WAIT_MAX];
    uint64_t mars_context_ea;
} __attribute__((aligned(MARS_TASK_QUEUE_ALIGN)));

extern spe_program_handle_t mpu_prog;

static struct mars_context *mars_ctx;
static struct mars_task_id task_id;
static struct mars_task_args task_args;

struct mars_task_queue_t *internal_queue;

int init_queue_buffer(struct mars_context *mars_ctx, struct mars_task_queue_t *internal_queue)
{

	uint64_t buffer_ea;

	buffer_ea = mars_ea_memalign(MARS_TASK_QUEUE_BUFFER_ALIGN,
				     MARS_TASK_QUEUE_ENTRY_SIZE_MAX * 16);

	internal_queue->mars_context_ea = mars_ptr_to_ea(mars_ctx);
	internal_queue->buffer_ea = buffer_ea;
	internal_queue->push_ea = buffer_ea;
	internal_queue->pop_ea = buffer_ea;
	internal_queue->size = MARS_TASK_QUEUE_ENTRY_SIZE_MAX;
	internal_queue->depth = 16;
	internal_queue->direction = MARS_TASK_QUEUE_MPU_TO_HOST;
	internal_queue->count = 0;
	internal_queue->push_wait_count = 0;
	internal_queue->pop_wait_count = 0;
	internal_queue->push_wait_head = 0;
	internal_queue->pop_wait_head = 0;

	return 0;
}


int destroy_queue_buffer(struct mars_context *mars_ctx, struct mars_task_queue_t *internal_queue)
{

	free(internal_queue->buffer_ea);
	return 0;

}


int main(void)
{
	int ret, exit_code;
	struct mars_task_queue_t *queue_base;

	ret = mars_context_create(&mars_ctx, 0, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_create(mars_ctx, &task_id, NULL, mpu_prog.elf_image,
			       MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = posix_memalign((void **)&queue_base, MARS_TASK_QUEUE_ALIGN , MARS_TASK_QUEUE_SIZE*2);
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	internal_queue = (struct mars_task_queue_t *)((char *)queue_base+4);

	ret = init_queue_buffer(mars_ctx, internal_queue);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	task_args.type.u64[0] = (unsigned long)internal_queue;
	ret = mars_task_schedule(&task_id, &task_args, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	ret = mars_task_wait(&task_id, &exit_code);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);
	MARS_TEST_ASSERT_ERROR(exit_code, MARS_SUCCESS);

	ret = mars_task_destroy(&task_id);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	destroy_queue_buffer(mars_ctx, internal_queue);

	free(queue_base);

	ret = mars_context_destroy(mars_ctx);
	MARS_TEST_ASSERT_ERROR(ret, MARS_SUCCESS);

	return 0;
}
