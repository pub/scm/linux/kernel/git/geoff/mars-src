/*
 * Copyright 2009 Sony Corporation
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <libspe2.h>
#include <mars/task.h>
#include <mars_test.h>

static int spe_cnt;

int test_more_than_physical(void)
{
	struct mars_context *mars;
	int ret;

	spe_cnt = spe_cpu_info_get(SPE_COUNT_PHYSICAL_SPES, -1);
	MARS_TEST_ASSERT_EQUAL((spe_cnt >= 0), 1);

	ret = mars_context_create(&mars, spe_cnt + 1, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_ERROR_PARAMS);

	return 0;
}

int test_more_than_usable(void)
{
	struct mars_context *mars;
	int ret;
	int usable_cnt;

	usable_cnt = spe_cpu_info_get(SPE_COUNT_USABLE_SPES, -1);
	MARS_TEST_ASSERT_EQUAL((usable_cnt >= 0), 1);

	/* make sure usable count is not same as physical count */
	if (usable_cnt == spe_cnt)
	{
		// FIXME : Reserve an spe...
	}

	ret = mars_context_create(&mars, usable_cnt + 1, 0);
	MARS_TEST_ASSERT_ERROR(ret, MARS_ERROR_PARAMS);

	return 0;
}

int main(void)
{
	int ret;

	ret = test_more_than_physical();
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	ret = test_more_than_usable();
	MARS_TEST_ASSERT_EQUAL(ret, 0);

	return 0;
}
