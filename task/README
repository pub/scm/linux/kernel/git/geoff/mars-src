/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this Library and associated documentation files (the
 * "Library"), to deal in the Library without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Library, and to
 * permit persons to whom the Library is furnished to do so, subject to
 * the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Library.
 *
 *  If you modify the Library, you may copy and distribute your modified
 *  version of the Library in object code or as an executable provided
 *  that you also do one of the following:
 *
 *   Accompany the modified version of the Library with the complete
 *   corresponding machine-readable source code for the modified version
 *   of the Library; or,
 *
 *   Accompany the modified version of the Library with a written offer
 *   for a complete machine-readable copy of the corresponding source
 *   code of the modified version of the Library.
 *
 *
 * THE LIBRARY IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * LIBRARY OR THE USE OR OTHER DEALINGS IN THE LIBRARY.
 */

MARS Task README

Overview
========

The MARS (Multicore Application Runtime System) task package provides libraries
to manage and create user programs as part of the task workload model.

The main components of the task package are:

  - MARS host task library
  - MARS MPU task library
  - MARS MPU task module

/task
|
|---/include		Include directory
|   |
|   |---/mars		MARS include sub-directory
|	|
|	|---/common	All common header files shared by both host and MPU
|	|
|	|---/host	All host specific header files
|	|
|	|---/mpu	All MPU specific header files
|
|---/src		Source directory
    |
    |---/host		All host source files
    |   |
    |   |---/lib	Source files for host MARS task library
    |
    |---/mpu		All MPU source files
        |
        |---/module	Source files for MPU MARS task module
        |
        |---/lib	Source files for MPU MARS task library


Building
========

* See the source file INSTALL for generic build info.

* The configure scripts require the option --with-mars-platform.  Currently,
only the Cell Broadband Engine (--with-mars-platfrom=cell) is supported.

* Use the prefixes PPU_ and SPU_ on command line variables to the top level
configure to specify variables for the different Cell toolchains.

* When building 64-bit libraries for a powerpc host it is customary to use
the --libdir configure option to install the libraries in a EPREFIX/lib64
directory.  Run configure with the --help option for more info.

For example, when using the Cell SDK toolchains use options similar to these:

32-bit powerpc host
-------------------
  ./configure \
    --with-mars-platform=cell SPU_CC=spu-gcc PPU_CC=ppu-gcc PPU_CFLAGS=-m32

64-bit powerpc host
-------------------
  ./configure \
    --libdir="/usr/lib64" \
    --with-mars-platform=cell SPU_CC=spu-gcc PPU_CC=ppu-gcc PPU_CFLAGS=-m64

Note:
	The configure options above may vary based on Linux distribution or Cell
	toolchain.

	For example, Gentoo on PS3 might use options similar to:

  ./configure --with-mars-platform=cell SPU_CC=spu-elf-gcc SPU_AR=spu-elf-ar
