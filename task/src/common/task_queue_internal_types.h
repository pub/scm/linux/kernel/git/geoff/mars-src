/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this Library and associated documentation files (the
 * "Library"), to deal in the Library without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Library, and to
 * permit persons to whom the Library is furnished to do so, subject to
 * the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Library.
 *
 *  If you modify the Library, you may copy and distribute your modified
 *  version of the Library in object code or as an executable provided
 *  that you also do one of the following:
 *
 *   Accompany the modified version of the Library with the complete
 *   corresponding machine-readable source code for the modified version
 *   of the Library; or,
 *
 *   Accompany the modified version of the Library with a written offer
 *   for a complete machine-readable copy of the corresponding source
 *   code of the modified version of the Library.
 *
 *
 * THE LIBRARY IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * LIBRARY OR THE USE OR OTHER DEALINGS IN THE LIBRARY.
 */

#ifndef MARS_TASK_QUEUE_INTERNAL_TYPES_H
#define MARS_TASK_QUEUE_INTERNAL_TYPES_H

#include <stdint.h>

#define MARS_TASK_QUEUE_SIZE			128
#define MARS_TASK_QUEUE_ALIGN			128
#define MARS_TASK_QUEUE_ALIGN_MASK		0x7f
#define MARS_TASK_QUEUE_ENTRY_SIZE_MASK		0xf
#define MARS_TASK_QUEUE_ENTRY_ALIGN		16
#define MARS_TASK_QUEUE_ENTRY_ALIGN_MASK	0xf
#define MARS_TASK_QUEUE_BUFFER_ALIGN		16
#define MARS_TASK_QUEUE_BUFFER_ALIGN_MASK	0xf

struct mars_task_queue {
	uint32_t lock;
	uint32_t size;
	uint32_t depth;
	uint32_t count;
	uint64_t buffer_ea;
	uint64_t push_ea;
	uint64_t pop_ea;
	uint8_t pad;
	uint8_t direction;
	uint8_t push_wait_head;
	uint8_t pop_wait_head;
	uint16_t push_wait_count;
	uint16_t pop_wait_count;
	uint16_t push_wait_id[MARS_TASK_QUEUE_WAIT_MAX];
	uint16_t pop_wait_id[MARS_TASK_QUEUE_WAIT_MAX];
	uint64_t mars_context_ea;
} __attribute__((aligned(MARS_TASK_QUEUE_ALIGN)));

#endif
