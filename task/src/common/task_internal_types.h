/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this Library and associated documentation files (the
 * "Library"), to deal in the Library without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Library, and to
 * permit persons to whom the Library is furnished to do so, subject to
 * the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Library.
 *
 *  If you modify the Library, you may copy and distribute your modified
 *  version of the Library in object code or as an executable provided
 *  that you also do one of the following:
 *
 *   Accompany the modified version of the Library with the complete
 *   corresponding machine-readable source code for the modified version
 *   of the Library; or,
 *
 *   Accompany the modified version of the Library with a written offer
 *   for a complete machine-readable copy of the corresponding source
 *   code of the modified version of the Library.
 *
 *
 * THE LIBRARY IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * LIBRARY OR THE USE OR OTHER DEALINGS IN THE LIBRARY.
 */

#ifndef MARS_TASK_INTERNAL_TYPES_H
#define MARS_TASK_INTERNAL_TYPES_H

#include <stdint.h>

#include "mars/workload_types.h"

#define MARS_TASK_MODULE_NAME			"MARS TASK"

#define MARS_TASK_CONTEXT_SIZE			MARS_WORKLOAD_CONTEXT_SIZE
#define MARS_TASK_CONTEXT_ALIGN			MARS_WORKLOAD_CONTEXT_ALIGN
#define MARS_TASK_CONTEXT_SAVE_ALIGN		128

#define MARS_TASK_REGISTER_SAVE_AREA_SIZE 	(16 * (127 - 80))

struct mars_task_context {
	uint8_t workload_reserved[MARS_WORKLOAD_RESERVED_SIZE];
	uint8_t pad[MARS_TASK_CONTEXT_SIZE - MARS_WORKLOAD_RESERVED_SIZE - 128];

	uint64_t text_ea;			/* ea of text segment */
	uint64_t data_ea;			/* ea of data segment */
	uint32_t text_vaddr;			/* vaddr of text segment */
	uint32_t data_vaddr;			/* vaddr of data segment */
	uint32_t text_size;			/* size of text segment */
	uint32_t data_size;			/* size of data segment */
	uint32_t bss_size;			/* size of bss segment */
	uint32_t entry;				/* entry address of exec */
	uint32_t stack;				/* stack pointer of exec */
	uint32_t exit_code;			/* exit code */
	uint64_t context_save_area_ea;		/* context save area */
	uint32_t context_save_area_low_size;	/* size of low save area */
	uint32_t context_save_area_high_size;	/* size of high save area */
	struct mars_task_id id;			/* task id */
	struct mars_task_args args;		/* task args */
} __attribute__((aligned(MARS_TASK_CONTEXT_ALIGN)));

#endif
