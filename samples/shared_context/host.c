/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdlib.h>
#include <libspe2.h>
#include <pthread.h>
#include <mars/task.h>

#define INFO	\
"\
MARS Shared Context Sample					\n\
--------------------------					\n\
This program is a simple example to show how to create and	\n\
use the shared MARS context.					\n\
\n"

#define NUM_TASKS	10

extern struct spe_program_handle mpu_task_prog;

static struct mars_context *mars_ctx[NUM_TASKS];
static struct mars_task_id task_id[NUM_TASKS];

static int task_count;
static pthread_mutex_t task_count_lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t task_count_cond = PTHREAD_COND_INITIALIZER;

void *thread_proc(void *arg)
{
	int ret;
	int index = (uintptr_t)arg;

	ret = mars_context_create(&mars_ctx[index], 0, 1);
	if (ret) {
		printf("MARS context create failed! (%d)\n", ret);
		exit(1);
	}

	/* wait until all threads create the shared MARS context */
	pthread_mutex_lock(&task_count_lock);
	task_count++;
	if (task_count == NUM_TASKS)
		pthread_cond_broadcast(&task_count_cond);
	while (task_count != NUM_TASKS)
		pthread_cond_wait(&task_count_cond, &task_count_lock);
	pthread_mutex_unlock(&task_count_lock);

	ret = mars_task_create(mars_ctx[index], &task_id[index], "Task",
		mpu_task_prog.elf_image, 0);
	if (ret) {
		printf("MARS task create failed! (%d)\n", ret);
		exit(1);
	}

	ret = mars_task_schedule(&task_id[index], NULL, 0);
	if (ret) {
		printf("MARS task schedule failed! (%d)\n", ret);
		exit(1);
	}

	ret = mars_task_wait(&task_id[index], NULL);
	if (ret) {
		printf("MARS task wait failed! (%d)\n", ret);
		exit(1);
	}

	ret = mars_task_destroy(&task_id[index]);
	if (ret) {
		printf("MARS task destroy failed! (%d)\n", ret);
		exit(1);
	}

	ret = mars_context_destroy(mars_ctx[index]);
	if (ret) {
		printf("MARS context destroy failed! (%d)\n", ret);
		exit(1);
	}

	return NULL;
}

int main(void)
{
	int ret;
	int i;
	pthread_t threads[NUM_TASKS];

	printf(INFO);

	for (i = 0; i < NUM_TASKS; i++) {
		ret = pthread_create(&threads[i], NULL,
				     thread_proc, (void *)(uintptr_t)i);
		if (ret) {
			printf("pthread create failed! (%d)\n", ret);
			return 1;
		}
	}

	/* wait until all thread create the shared MARS context */
	pthread_mutex_lock(&task_count_lock);
	while (task_count != NUM_TASKS)
		pthread_cond_wait(&task_count_cond, &task_count_lock);
	pthread_mutex_unlock(&task_count_lock);

	/* check if all thread use the same MARS context */
	for (i = 0; i < NUM_TASKS - 1; i++) {
		if (mars_ctx[i] != mars_ctx[i + 1]) {
			printf("MARS context %d and MARS context %d are different!\n",
			       i, i + 1);
			return 1;
		}
	}

	for (i = 0; i < NUM_TASKS; i++) {
		pthread_join(threads[i], NULL);
	}

	return 0;
}


