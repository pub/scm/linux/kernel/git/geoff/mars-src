/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>
#include <libspe2.h>
#include <mars/task.h>

#define INFO	\
"\
MARS Task Signal Sample						\n\
-----------------------						\n\
This program shows the basic usage of the MARS task signal	\n\
synchronization between the host program and multiple MPU	\n\
programs.							\n\
								\n\
This program creates 2 tasks. Task 1 can only begin processing	\n\
after the host program has waited 1 second and signals for	\n\
task 1 to begin. Task 2 can only begin after task 1 has		\n\
completed its processing and signals for task 2	to begin.	\n\
\n"

extern struct spe_program_handle mpu_task1_prog;
extern struct spe_program_handle mpu_task2_prog;

static struct mars_context *mars_ctx;
static struct mars_task_id task1_id __attribute__((aligned(16)));
static struct mars_task_id task2_id __attribute__((aligned(16)));
static struct mars_task_args task_args;

int main(void)
{
	int ret;

	printf(INFO);

	ret = mars_context_create(&mars_ctx, 0, 0);
	if (ret) {
		printf("MARS context create failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_task_create(mars_ctx, &task1_id, "Task 1",
		mpu_task1_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	if (ret) {
		printf("MARS task 1 create failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_task_create(mars_ctx, &task2_id, "Task 2",
		mpu_task2_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	if (ret) {
		printf("MARS task 2 create failed! (%d)\n", ret);
		return 1;
	}

	task_args.type.u64[0] = mars_ptr_to_ea(&task2_id);
	ret = mars_task_schedule(&task1_id, &task_args, 0);
	if (ret) {
		printf("MARS task 1 schedule failed! (%d)\n", ret);
		return 1;
	}

	task_args.type.u64[0] = mars_ptr_to_ea(&task1_id);
	ret = mars_task_schedule(&task2_id, &task_args, 0);
	if (ret) {
		printf("MARS task 2 schedule failed! (%d)\n", ret);
		return 1;
	}

	printf("HOST  : Main() - Wait 1 sec to Send Signal to Start Task 1\n");
	sleep(1);

	ret = mars_task_signal_send(&task1_id);
	if (ret) {
		printf("MARS task 1 signal send failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_task_wait(&task1_id, NULL);
	if (ret) {
		printf("MARS task 1 wait failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_task_wait(&task2_id, NULL);
	if (ret) {
		printf("MARS task 2 wait failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_task_destroy(&task1_id);
	if (ret) {
		printf("MARS task 1 destroy failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_task_destroy(&task2_id);
	if (ret) {
		printf("MARS task 2 destroy failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_context_destroy(mars_ctx);
	if (ret) {
		printf("MARS context destroy failed! (%d)\n", ret);
		return 1;
	}

	return 0;
}


