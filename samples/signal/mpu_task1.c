/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdio.h>
#include <limits.h>
#include <spu_mfcio.h>
#include <mars/task.h>

#define PROCESSING_LOOP INT_MAX

static int garbage;

int mars_task_main(const struct mars_task_args *task_args)
{
	int ret, i;
	struct mars_task_id task2_id;

	printf("MPU(%d): %s - Started\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	mfc_get(&task2_id, task_args->type.u64[0], sizeof(struct mars_task_id), 0,0,0);
	mfc_write_tag_mask(1 << 0);
	mfc_read_tag_status_all();

	printf("MPU(%d): %s - Wait Signal from HOST\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	ret = mars_task_signal_wait();
	if (ret) {
		printf("MARS task signal wait failed! (%d)\n", ret);
		return 1;
	}

	printf("MPU(%d): %s - Processing...\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	for (i = 0; i < PROCESSING_LOOP; i++)
		garbage += i;

	printf("MPU(%d): %s - Send Signal to %s\n",
		mars_task_get_kernel_id(), mars_task_get_name(), task2_id.name);

	ret = mars_task_signal_send(&task2_id);
	if (ret) {
		printf("MARS task signal send failed! (%d)\n", ret);
		return 1;
	}

	printf("MPU(%d): %s - Wait Signal from %s\n",
		mars_task_get_kernel_id(), mars_task_get_name(), task2_id.name);

	ret = mars_task_signal_wait();
	if (ret) {
		printf("MARS task signal wait failed! (%d)\n", ret);
		return 1;
	}

	printf("MPU(%d): %s - Finished\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	return 0;
}
