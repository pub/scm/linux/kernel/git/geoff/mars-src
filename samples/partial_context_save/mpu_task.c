/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <mars/task.h>

#define ITERATIONS 10

int mars_task_main(const struct mars_task_args *task_args)
{
	int i;
	uint32_t kernel_id_start[ITERATIONS], kernel_id_end[ITERATIONS];
	uint32_t ticks_start[ITERATIONS], ticks_end[ITERATIONS];

	if (task_args->type.u32[0]) {
		malloc(task_args->type.u32[0]);
	}

	for (i = 0; i < ITERATIONS; i++) {
		kernel_id_start[i] = mars_task_get_kernel_id();
		ticks_start[i] = mars_task_get_ticks();

		mars_task_yield();

		ticks_end[i] = mars_task_get_ticks();
		kernel_id_end[i] = mars_task_get_kernel_id();
	}

	for (i = 0; i < ITERATIONS; i++) {
		printf("MPU(%d) => MPU(%d): %s: %d ticks%s\n",
		       kernel_id_start[i], kernel_id_end[i],
		       mars_task_get_name(), ticks_end[i] - ticks_start[i],
		       (i == 0 || kernel_id_start[i] != kernel_id_end[i]) ? " (unreliable)" : "");
	}

	return 0;
}
