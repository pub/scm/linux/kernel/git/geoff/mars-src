/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdlib.h>
#include <sys/time.h>
#include <libspe2.h>
#include <mars/task.h>

#define INFO	\
"\
MARS Partial Context Save Sample				\n\
--------------------------------				\n\
This program is a simple example to show how a task's context	\n\
switching is optimized by the task module's partial context	\n\
switching feature. The task module calculates how much of the	\n\
MPU storage is being used by the task and only saves and	\n\
restores the used areas of the MPU storage.			\n\
								\n\
The task being run in this program is a very simple MPU program	\n\
that yields to perform a context switch.			\n\
								\n\
The first time the task is created and run, almost all MPU	\n\
storage is consumed.						\n\
								\n\
The second time the task is created and run, only small part of	\n\
MPU storage is consumed.					\n\
\n"

extern struct spe_program_handle mpu_task_prog;

static struct mars_context *mars_ctx;
static struct mars_task_id task_id;

int run_task_switch(uint32_t dummy_data_size)
{
	int ret;
	struct mars_task_args task_args;

	ret = mars_task_create(mars_ctx, &task_id, "Task",
		mpu_task_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	if (ret) {
		printf("MARS task create failed! (%d)\n", ret);
		return 1;
	}

	task_args.type.u32[0] = dummy_data_size;
	ret = mars_task_schedule(&task_id, &task_args, 0);
	if (ret) {
		printf("MARS task schedule failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_task_wait(&task_id, NULL);
	if (ret) {
		printf("MARS task wait failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_task_destroy(&task_id);
	if (ret) {
		printf("MARS task destroy failed! (%d)\n", ret);
		return 1;
	}

	return 0;
}

int main(void)
{
	int ret;

	printf(INFO);

	ret = mars_context_create(&mars_ctx, 0, 0);
	if (ret) {
		printf("MARS context create failed! (%d)\n", ret);
		return 1;
	}

	printf("\nNearly Full Context Save\n");
	ret = run_task_switch(MARS_TASK_CONTEXT_SAVE_SIZE_MAX - 1024 * 16);
	if (ret)
		return 1;

	printf("\nPartial Context Save\n");
	ret = run_task_switch(0);
	if (ret)
		return 1;

	ret = mars_context_destroy(mars_ctx);
	if (ret) {
		printf("MARS context destroy failed! (%d)\n", ret);
		return 1;
	}

	return 0;
}


