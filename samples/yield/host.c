/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <libspe2.h>
#include <mars/task.h>

#define INFO	\
"\
MARS Task Yield Sample						\n\
----------------------						\n\
This program shows a simple example of how MARS tasks can	\n\
yield the execution right of the MPU program to let other	\n\
MPU programs execute on the MPUs.				\n\
\n\
This program creates the MARS context to only use 1 MPU		\n\
in order to easily show the task yielding.			\n\
\n\
This program creates 2 tasks. Task 1 needs to do some		\n\
processing 3 times, and task 2 needs to do some processing	\n\
2 times. In between each processing, each task will yield	\n\
to allow for the other process to execute and continue with	\n\
their processing.						\n\
\n"

extern struct spe_program_handle mpu_task_prog;

static struct mars_context *mars_ctx;
static struct mars_task_id task1_id;
static struct mars_task_id task2_id;
static struct mars_task_args task_args;

int main(void)
{
	int ret;

	printf(INFO);

	printf("HOST  : Main() - Initializing MARS with 1 MPU\n");

	ret = mars_context_create(&mars_ctx, 1, 0);
	if (ret) {
		printf("MARS context intialize failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_task_create(mars_ctx, &task1_id, "Task 1",
		mpu_task_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	if (ret) {
		printf("MARS task 1 create failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_task_create(mars_ctx, &task2_id, "Task 2",
		mpu_task_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
	if (ret) {
		printf("MARS task 2 create failed! (%d)\n", ret);
		return 1;
	}

	task_args.type.u32[0] = 3;
	ret = mars_task_schedule(&task1_id, &task_args, 0);
	if (ret) {
		printf("MARS task 1 schedule failed! (%d)\n", ret);
		return 1;
	}

	task_args.type.u32[0] = 2;
	ret = mars_task_schedule(&task2_id, &task_args, 0);
	if (ret) {
		printf("MARS task 2 schedule failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_task_wait(&task1_id, NULL);
	if (ret) {
		printf("MARS task 1 wait failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_task_wait(&task2_id, NULL);
	if (ret) {
		printf("MARS task 2 wait failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_task_destroy(&task1_id);
	if (ret) {
		printf("MARS task 1 destroy failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_task_destroy(&task2_id);
	if (ret) {
		printf("MARS task 2 destroy failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_context_destroy(mars_ctx);
	if (ret) {
		printf("MARS context destroy failed! (%d)\n", ret);
		return 1;
	}

	return 0;
}


