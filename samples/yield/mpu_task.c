/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdio.h>
#include <limits.h>
#include <mars/task.h>

#define PROCESSING_LOOP INT_MAX/10

static int garbage;

int mars_task_main(const struct mars_task_args *task_args)
{
	int ret, i, j;

	printf("MPU(%d): %s - Started\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	for (i = 0; i < (int)task_args->type.u32[0]; i++) {
		printf("MPU(%d): %s - Processing %d...\n",
			mars_task_get_kernel_id(), mars_task_get_name(), i);

		for (j = 0; j < PROCESSING_LOOP; j++)
			garbage += j;

		printf("MPU(%d): %s - Yielding\n",
			mars_task_get_kernel_id(), mars_task_get_name());

		ret = mars_task_yield();
		if (ret) {
			printf("MARS task yield failed! (%d)\n", ret);
			return 1;
		}
	}

	printf("MPU(%d): %s - Finished\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	return 0;
}
