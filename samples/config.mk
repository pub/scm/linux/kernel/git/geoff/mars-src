## config.mk
#
#  Copyright 2008 Sony Corporation of America
#
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#  THE SOFTWARE.
#

PPU_CROSS = ppu-
PPU_CC = $(PPU_CROSS)gcc
PPU_CPPFLAGS =
PPU_CFLAGS = $(CFLAGS) -Wall -O3 -funroll-loops
PPU_LDFLAGS = $(LDFLAGS) -lmars_task -lmars_base -lspe2
PPU_EMBEDSPU = $(PPU_CROSS)embedspu

SPU_CROSS = spu-
SPU_CC = $(SPU_CROSS)gcc
SPU_CFLAGS = -Wall -O3 -funroll-loops
SPU_LDFLAGS = -lmars_base -Wl,-gc-sections

SPU_WM_CFLAGS = $(SPU_CFLAGS)
SPU_WM_LDFLAGS = \
	-Wl,--defsym=__stack=0x3ff0 \
	-Wl,--section-start,.init=0x3000 \
	-Wl,--entry,mars_module_entry -Wl,-u,mars_module_entry \
	$(SPU_LDFLAGS)

SPU_TASK_CFLAGS = $(SPU_CFLAGS)
SPU_TASK_LDFLAGS = \
	-lmars_task \
	-Wl,--section-start,.init=0x4000 \
	$(SPU_LDFLAGS)
