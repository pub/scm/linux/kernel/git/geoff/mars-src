/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdio.h>
#include <spu_mfcio.h>
#include <mars/task.h>

int mars_task_main(const struct mars_task_args *task_args)
{
	uint64_t semaphore_ea = task_args->type.u64[0];
	uint64_t shared_resource_ea = task_args->type.u64[1];
	uint32_t shared_resource __attribute__((aligned(16)));
	int ret;

	printf("MPU(%d): %s - Started\n",
	mars_task_get_kernel_id(), mars_task_get_name());

	printf("MPU(%d): %s - Wait for Semaphore\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	ret = mars_task_semaphore_acquire(semaphore_ea);
	if (ret) {
		printf("MARS semaphore acquire failed! (%d)\n", ret);
		return 1;
	}

	printf("MPU(%d): %s - Acquiring Semaphore\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	mfc_get(&shared_resource, shared_resource_ea, sizeof(uint32_t), 0,0,0);
	mfc_write_tag_mask(1 << 0);
	mfc_read_tag_status_all();

	shared_resource++;

	printf("MPU(%d): %s - Increment Shared Resource Counter = %d\n",
		mars_task_get_kernel_id(), mars_task_get_name(),
		shared_resource);

	mfc_put(&shared_resource, shared_resource_ea, sizeof(uint32_t), 0,0,0);
	mfc_write_tag_mask(1 << 0);
	mfc_read_tag_status_all();

	printf("MPU(%d): %s - Releasing Semaphore\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	ret = mars_task_semaphore_release(semaphore_ea);
	if (ret) {
		printf("MARS task semaphore release failed! (%d)\n", ret);
		return 1;
	}

	printf("MPU(%d): %s - Finished\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	return 0;
}
