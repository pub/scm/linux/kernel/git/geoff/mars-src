/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <libspe2.h>
#include <mars/task.h>

#define INFO	\
"\
MARS Task Semaphore Sample					\n\
--------------------------					\n\
This program shows the basic usage of the MARS semaphore	\n\
synchronization to protect a shared resource from being		\n\
accessed by multiple MPU programs at once.			\n\
								\n\
This program creates %d tasks and creates a single		\n\
semaphore to protect access of a shared resource integer	\n\
counter located in main storage. As each task runs, it tries	\n\
to obtain the semaphore and increments the shared resource	\n\
counter before releasing the semaphore. Since the shared	\n\
resource is protected from concurrent accesses, the resulting	\n\
value of the counter should equal to the number of total	\n\
tasks, %d, when the program has completed.			\n\
\n", NUM_TASKS, NUM_TASKS

#define NUM_TASKS	10

extern struct spe_program_handle mpu_task_prog;

static struct mars_context *mars_ctx;
static struct mars_task_id task_id[NUM_TASKS];
static struct mars_task_args task_args;
static uint64_t semaphore_ea;

int main(void)
{
	uint32_t shared_resource __attribute__((aligned(16)));
	int ret, i;

	printf(INFO);

	ret = mars_context_create(&mars_ctx, 0, 0);
	if (ret) {
		printf("MARS context create failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_task_semaphore_create(mars_ctx, &semaphore_ea, 1);
	if (ret) {
		printf("MARS task semaphore create failed! (%d)\n", ret);
		return 1;
	}

	shared_resource = 0;

	printf("HOST  : Main() - Shared Resource Counter = %d\n",
		shared_resource);

	for (i = 0; i < NUM_TASKS; i++) {
		char name[MARS_TASK_NAME_LEN_MAX];

		snprintf(name, MARS_TASK_NAME_LEN_MAX, "Task %d", i);
		ret = mars_task_create(mars_ctx, &task_id[i], name,
			mpu_task_prog.elf_image, MARS_TASK_CONTEXT_SAVE_SIZE_MAX);
		if (ret) {
			printf("MARS task create failed! (%d)\n", ret);
			return 1;
		}

		task_args.type.u64[0] = semaphore_ea;
		task_args.type.u64[1] = mars_ptr_to_ea(&shared_resource);
		ret = mars_task_schedule(&task_id[i], &task_args, 0);
		if (ret) {
			printf("MARS task schedule failed! (%d)\n", ret);
			return 1;
		}
	}

	for (i = 0; i < NUM_TASKS; i++) {
		ret = mars_task_wait(&task_id[i], NULL);
		if (ret) {
			printf("MARS task wait failed! (%d)\n", ret);
			return 1;
		}

		ret = mars_task_destroy(&task_id[i]);
		if (ret) {
			printf("MARS task destroy failed! (%d)\n", ret);
			return 1;
		}
	}

	printf("HOST  : Main() - Shared Resource Counter = %d\n",
		shared_resource);

	ret = mars_task_semaphore_destroy(semaphore_ea);
	if (ret) {
		printf("MARS task semaphore destroy failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_context_destroy(mars_ctx);
	if (ret) {
		printf("MARS context destroy failed! (%d)\n", ret);
		return 1;
	}

	return 0;
}
