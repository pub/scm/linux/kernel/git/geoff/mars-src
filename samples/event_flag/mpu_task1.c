/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdio.h>
#include <limits.h>
#include <mars/task.h>

#define PROCESSING_LOOP INT_MAX

static int garbage;

int mars_task_main(const struct mars_task_args *task_args)
{
	int ret, i;
	uint64_t host_to_mpu_ea = task_args->type.u64[0];
	uint64_t mpu_to_mpu_ea = task_args->type.u64[1];

	printf("MPU(%d): %s - Started\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	printf("MPU(%d): %s - Wait for HOST to MPU event\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	ret = mars_task_event_flag_wait(host_to_mpu_ea, 0x1,
				MARS_TASK_EVENT_FLAG_MASK_AND, NULL);
	if (ret) {
		printf("MARS task event flag wait failed! (%d)\n", ret);
		return 1;
	}

	printf("MPU(%d): %s - Received HOST to MPU event\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	printf("MPU(%d): %s - Processing...\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	for (i = 0; i < PROCESSING_LOOP; i++)
		garbage += i;

	printf("MPU(%d): %s - Set MPU to MPU event\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	ret = mars_task_event_flag_set(mpu_to_mpu_ea, 0x1);
	if (ret) {
		printf("MARS event flag set failed! (%d)\n", ret);
		return 1;
	}

	printf("MPU(%d): %s - Finished\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	return 0;
}
