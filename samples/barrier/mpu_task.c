/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdio.h>
#include <limits.h>
#include <mars/task.h>

#define ITERATIONS 3
#define PROCESSING_LOOP ( INT_MAX / 2 )

static int garbage;

void pre_barrier_process(int task_index)
{
	/* dummy work  */
	int i;

	for (i = 0; i <= (task_index & 1); i++) {
		int j;
		for (j = 0; j < PROCESSING_LOOP; j++)
			garbage += j;
	}
}

void post_barrier_process(int task_index)
{
	/* dummy work  */
	int i;

	for (i = 0; i <= ((task_index >> 1) & 1); i++) {
		int j;
		for (j = 0; j < PROCESSING_LOOP; j++)
			garbage += j;
	}
}

int mars_task_main(const struct mars_task_args *task_args)
{
	int ret, i;
	uint64_t barrier_ea = task_args->type.u64[0];
	uint32_t task_index = task_args->type.u32[2];

	for (i = 0; i < ITERATIONS; i++) {
		printf("Iteration %d: MPU(%d): %s - Pre Processing...\n",
		       i, mars_task_get_kernel_id(), mars_task_get_name());
		pre_barrier_process(task_index);

		printf("Iteration %d: MPU(%d): %s - Notify Barrier\n",
		       i, mars_task_get_kernel_id(), mars_task_get_name());

		ret = mars_task_barrier_notify(barrier_ea);
		if (ret) {
			printf("MARS task barrier notify failed! (%d)\n", ret);
			return 1;
		}

		printf("Iteration %d: MPU(%d): %s - Wait for barrier\n",
		       i, mars_task_get_kernel_id(), mars_task_get_name());

		ret = mars_task_barrier_wait(barrier_ea);
		if (ret != MARS_SUCCESS) {
			printf("MARS task barrier waiting failed! (%d)\n", ret);
			return 1;
		}

		printf("Iteration %d: MPU(%d): %s - Post Processing...\n",
		       i, mars_task_get_kernel_id(), mars_task_get_name());
		post_barrier_process(task_index);
	}

	return 0;
}
