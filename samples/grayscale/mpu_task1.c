/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdio.h>
#include <stdint.h>
#include <spu_intrinsics.h>
#include <spu_mfcio.h>
#include <mars/task.h>

#define NUM_TASKS	16

#define ALIGN4_UP(x)	(((x) + 0x3) & ~0x3)

typedef struct {
	uint64_t ea_task_id;
	uint64_t ea_event;
	uint64_t ea_queue;
	uint64_t ea_src;
	uint64_t ea_dst;
	uint32_t num;
	uint32_t pad;
} grayscale_params_t;

typedef struct {
	uint64_t ea_event;
	uint64_t ea_src;
	uint64_t ea_dst;
	uint32_t num;
	uint32_t id;
} grayscale_queue_elem_t;

static struct mars_task_id task2_id[NUM_TASKS];
static struct mars_task_args task2_args;

static grayscale_params_t grayscale_params __attribute__((aligned(16)));
static grayscale_queue_elem_t data __attribute__((aligned(16)));

int mars_task_main(const struct mars_task_args *task_args)
{
	int ret, i, tag = 0;
	int num, remain, chunk;
	uint64_t ea_task_id, ea_event, ea_queue;
	uint64_t ea_src, ea_dst;
	uint16_t mask = 0;

	/* Get application parameters */
	mfc_get(&grayscale_params, task_args->type.u64[0], sizeof(grayscale_params_t), tag, 0, 0);
	mfc_write_tag_mask(1 << tag);
	mfc_read_tag_status_all();

	ea_task_id = grayscale_params.ea_task_id;
	ea_event   = grayscale_params.ea_event;
	ea_queue   = grayscale_params.ea_queue;
	ea_src     = grayscale_params.ea_src;
	ea_dst     = grayscale_params.ea_dst;
	num        = grayscale_params.num;

	/* Get sub task ids */
	mfc_get(&task2_id, ea_task_id, sizeof(struct mars_task_id) * NUM_TASKS, tag, 0, 0);
	mfc_write_tag_mask(1 << tag);
	mfc_read_tag_status_all();

	/* Pass queue ea to sub task args */
	task2_args.type.u64[0] = ea_queue;

	/* Schedule sub tasks for execution */
	for (i = 0; i < NUM_TASKS; i++) {
		ret = mars_task_schedule(&task2_id[i], &task2_args, 0);
		if (ret) {
			printf("MARS task 2 schedule failed! (%d)\n", ret);
			return 1;
		}
	}

	remain = num;
	chunk = ALIGN4_UP(num/NUM_TASKS);
	for (i = 0; i < NUM_TASKS; i++) {
		data.ea_event = ea_event;
		data.ea_src   = ea_src;
		data.ea_dst   = ea_dst;
		data.id       = i;
		if (remain > chunk) {
			data.num = ALIGN4_UP(chunk);
		} else {
			data.num = ALIGN4_UP(remain);
		}

		/* Push data to queue */
		ret = mars_task_queue_push_begin(ea_queue, &data, tag);
		if (ret) {
			printf("MARS task queue push failed! (%d)\n", ret);
			return 1;
		}
		ret = mars_task_queue_push_end(ea_queue, tag);
		if (ret) {
			printf("MARS task queue pop failed! (%d)\n", ret);
			return 1;
		}

		remain -= chunk;
		ea_src += (chunk * 4);
		ea_dst += (chunk * 4);

		/* Create event mask */
		mask |= 1 << i;
	}

	/* Wait until specified bits are set to event flag */
	ret = mars_task_event_flag_wait(ea_event, mask, MARS_TASK_EVENT_FLAG_MASK_AND, NULL);
	if (ret) {
		printf("MARS task event flag wait failed! (%d)\n", ret);
		return 1;
	}

	/* Wait for all scheduled sub tasks to complete */
	for (i = 0; i < NUM_TASKS; i++) {
		ret = mars_task_wait(&task2_id[i], NULL);
		if (ret) {
			printf("MARS task 2 wait failed! (%d)\n", ret);
			return 1;
		}
	}

	return 0;
}
