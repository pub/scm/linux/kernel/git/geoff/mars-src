## rules.mk
#
#  Copyright 2008 Sony Corporation of America
#
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#  THE SOFTWARE.
#

.SUFFIXES: .o .mpu_eo .mpu .mpu_o .wm_eo .wm .wm_o .task_eo .task .task_o

.c.o:
	$(PPU_CC) $(PPU_CFLAGS) -c -o $@ $<

.c.mpu_o:
	$(SPU_CC) $(SPU_CFLAGS) -c -o $@ $<
.mpu_o.mpu:
	$(SPU_CC) $(SPU_CFLAGS) -o $@ $< $(SPU_LDFLAGS)
.mpu.mpu_eo:
	$(PPU_EMBEDSPU) $(PPU_CFLAGS) $*_prog $< $@

.c.wm_o:
	$(SPU_CC) $(SPU_WM_CFLAGS) -c -o $@ $<
.wm_o.wm:
	$(SPU_CC) $(SPU_WM_CFLAGS) -o $@ $< $(SPU_WM_LDFLAGS)
.wm.wm_eo:
	$(PPU_EMBEDSPU) $(PPU_CFLAGS) $*_prog $< $@

.c.task_o:
	$(SPU_CC) $(SPU_TASK_CFLAGS) -c -o $@ $<
.task_o.task:
	$(SPU_CC) $(SPU_TASK_CFLAGS) -o $@ $< $(SPU_TASK_LDFLAGS)
.task.task_eo:
	$(PPU_EMBEDSPU) $(PPU_CFLAGS) $*_prog $< $@

.SECONDARY:
.PHONY: all check clean

all: $(sample)

$(sample): $(sample_objs)
	$(PPU_CC) $(PPU_CFLAGS) -o $@ $(sample_objs) $(PPU_LDFLAGS)

check: $(sample)
	./$(sample)

clean:
	-rm -f *.o *.mpu_o *.mpu *.wm_o *.wm *.task_o *.task *.*_eo $(sample)
