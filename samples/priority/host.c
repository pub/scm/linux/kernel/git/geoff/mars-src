/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <libspe2.h>
#include <mars/task.h>

#define INFO	\
"\
MARS Task Priority Sample					\n\
-------------------------					\n\
This program shows that basic usage of the MARS task priority	\n\
can be used to give certain tasks greater or lower priorities	\n\
during task scheduling.						\n\
								\n\
This program creates %d tasks and creates %d sets of %d.	\n\
tasks with each set having a different priority. The resulting	\n\
behavior of which tasks are scheduled to run shows how the	\n\
higher priority tasks are likely to get scheduled before the 	\n\
lower priority tasks.						\n\
\n", NUM_TASKS, NUM_PRIOS, NUM_TASKS_PER_PRIO

#define NUM_PRIOS		3
#define NUM_TASKS_PER_PRIO	10
#define NUM_TASKS		NUM_TASKS_PER_PRIO * NUM_PRIOS

extern struct spe_program_handle mpu_task_prog;

static struct mars_context *mars_ctx;
static struct mars_task_id task_id[NUM_TASKS];
static struct mars_task_args task_args;

int main(void)
{
	int ret, i, j;

	printf(INFO);

	ret = mars_context_create(&mars_ctx, 0, 0);
	if (ret) {
		printf("MARS context create failed! (%d)\n", ret);
		return 1;
	}

	for (i = 0; i < NUM_PRIOS; i++) {
		for (j = 0; j < NUM_TASKS_PER_PRIO; j++) {
			char name[MARS_TASK_NAME_LEN_MAX];

			snprintf(name, MARS_TASK_NAME_LEN_MAX,
				"Task %d", i * NUM_TASKS_PER_PRIO + j);
			ret = mars_task_create(mars_ctx,
				&task_id[i * NUM_TASKS_PER_PRIO + j],
				name, mpu_task_prog.elf_image, 0);
			if (ret) {
				printf("MARS task create failed! (%d)\n", ret);
				return 1;
			}

			task_args.type.u32[0] = NUM_PRIOS - i;
			ret = mars_task_schedule(
				&task_id[i * NUM_TASKS_PER_PRIO + j],
				&task_args, NUM_PRIOS - i);
			if (ret) {
				printf("MARS task schedule failed! (%d)\n", ret);
				return 1;
			}
		}
	}

	for (i = 0; i < NUM_TASKS; i++) {
		ret = mars_task_wait(&task_id[i], NULL);
		if (ret) {
			printf("MARS task wait failed! (%d)\n", ret);
			return 1;
		}

		ret = mars_task_destroy(&task_id[i]);
		if (ret) {
			printf("MARS task destroy failed! (%d)\n", ret);
			return 1;
		}
	}

	ret = mars_context_destroy(mars_ctx);
	if (ret) {
		printf("MARS context destroy failed! (%d)\n", ret);
		return 1;
	}

	return 0;
}


