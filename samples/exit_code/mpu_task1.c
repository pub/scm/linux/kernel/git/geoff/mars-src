/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdio.h>
#include <limits.h>
#include <spu_mfcio.h>
#include <mars/task.h>

#define PROCESSING_LOOP INT_MAX

static int garbage;

int mars_task_main(const struct mars_task_args *task_args)
{
	int ret, i, exit_code;
	struct mars_task_id task2_0_id;
	struct mars_task_id task2_1_id;
	struct mars_task_args args;

	printf("MPU(%d): %s - Started\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	mfc_get(&task2_0_id, task_args->type.u64[0], sizeof(struct mars_task_id), 0,0,0);
	mfc_get(&task2_1_id, task_args->type.u64[1], sizeof(struct mars_task_id), 0,0,0);
	mfc_write_tag_mask(1 << 0);
	mfc_read_tag_status_all();

	printf("MPU(%d): %s - Scheduling Sub Tasks in parallel...\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	printf("MPU(%d): %s - Scheduling Sub Task 1...\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	args.type.u32[0] = 123;
	ret = mars_task_schedule(&task2_0_id, &args, 0);
	if (ret) {
		printf("MARS task 2.0 schedule failed! (%d)\n", ret);
		return 1;
	}

	printf("MPU(%d): %s - Scheduling Sub Task 2...\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	args.type.u32[0] = 321;
	ret = mars_task_schedule(&task2_1_id, &args, 0);
	if (ret) {
		printf("MARS task 2.1 schedule failed! (%d)\n", ret);
		return 1;
	}

	printf("MPU(%d): %s - Processing...\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	for (i = 0; i < PROCESSING_LOOP; i++)
		garbage += i;

	printf("MPU(%d): %s - Waiting for both Sub Tasks to finish...\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	ret = mars_task_wait(&task2_0_id, &exit_code);
	if (ret) {
		printf("MARS task 2.0 wait failed! (%d)\n", ret);
		return 1;
	}

	printf("MPU(%d): %s - Sub Task 1 returned (exit_code = %d)\n",
		mars_task_get_kernel_id(), mars_task_get_name(), exit_code);

	ret = mars_task_wait(&task2_1_id, &exit_code);
	if (ret) {
		printf("MARS task 2.1 wait failed! (%d)\n", ret);
		return 1;
	}

	printf("MPU(%d): %s - Sub Task 2 returned (exit_code = %d)\n",
		mars_task_get_kernel_id(), mars_task_get_name(), exit_code);

	printf("MPU(%d): %s - Finished\n",
		mars_task_get_kernel_id(), mars_task_get_name());

	return 0;
}
