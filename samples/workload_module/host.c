/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdlib.h>
#include <libspe2.h>
#include <elf.h>
#include <mars/base.h>
#include <mars/error.h>
#include <mars/context.h>
#include <mars/workload_queue.h>

#define INFO	\
"\
MARS Workload Module Sample					\n\
---------------------------					\n\
This program is a simple example to show how to create and	\n\
execute a workload module that prints \"Hello\" to stdout and	\n\
exits.	\n\
\n"

extern struct spe_program_handle mpu_workload_module_prog;

static struct mars_context *mars_ctx;

int main(void)
{
	int ret;
	uint16_t workload_id;
	uint64_t workload_ea;

	printf(INFO);

	ret = mars_context_create(&mars_ctx, 0, 0);
	if (ret) {
		printf("MARS context create failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_workload_queue_add_begin(mars_ctx, &workload_id, &workload_ea,
					    mpu_workload_module_prog.elf_image,
					    "Sample Module");
	if (ret != MARS_SUCCESS) {
		printf("MARS workload queue add begin failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_workload_queue_add_end(mars_ctx, workload_id, 0);
	if (ret != MARS_SUCCESS) {
		printf("MARS workload queue add end failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_workload_queue_schedule_begin(mars_ctx, workload_id, 0, 0);
	if (ret != MARS_SUCCESS) {
		printf("MARS workload queue schedule begin failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_workload_queue_schedule_end(mars_ctx, workload_id, 0);
	if (ret != MARS_SUCCESS) {
		printf("MARS workload queue schedule end failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_workload_queue_wait(mars_ctx, workload_id, 0);
	if (ret != MARS_SUCCESS) {
		printf("MARS workload queue wait failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_workload_queue_remove_begin(mars_ctx, workload_id, 0);
	if (ret != MARS_SUCCESS) {
		printf("MARS workload queue remove begin failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_workload_queue_remove_end(mars_ctx, workload_id, 0);
	if (ret != MARS_SUCCESS) {
		printf("MARS workload queue remove end failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_context_destroy(mars_ctx);
	if (ret) {
		printf("MARS context destroy failed! (%d)\n", ret);
		return 1;
	}

	return 0;
}


