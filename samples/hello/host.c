/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdlib.h>
#include <libspe2.h>
#include <mars/task.h>

#define INFO	\
"\
MARS Hello Sample						\n\
-----------------						\n\
This program is a simple example to show how to create and	\n\
execute a task that prints \"Hello\" to stdout and exits.	\n\
\n"

extern struct spe_program_handle mpu_task_prog;

static struct mars_context *mars_ctx;
static struct mars_task_id task_id;

int main(void)
{
	int ret;

	printf(INFO);

	ret = mars_context_create(&mars_ctx, 0, 0);
	if (ret) {
		printf("MARS context create failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_task_create(mars_ctx, &task_id, "Task",
		mpu_task_prog.elf_image, 0);
	if (ret) {
		printf("MARS task create failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_task_schedule(&task_id, NULL, 0);
	if (ret) {
		printf("MARS task schedule failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_task_wait(&task_id, NULL);
	if (ret) {
		printf("MARS task wait failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_task_destroy(&task_id);
	if (ret) {
		printf("MARS task destroy failed! (%d)\n", ret);
		return 1;
	}

	ret = mars_context_destroy(mars_ctx);
	if (ret) {
		printf("MARS context destroy failed! (%d)\n", ret);
		return 1;
	}

	return 0;
}


