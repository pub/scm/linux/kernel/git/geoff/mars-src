## acinclude.m4 -- Common autoconf macro definitions
#
#  Copyright 2008 Sony Corporation of America
#
#
#  Permission is hereby granted, free of charge, to any person obtaining
#  a copy of this Library and associated documentation files (the
#  "Library"), to deal in the Library without restriction, including
#  without limitation the rights to use, copy, modify, merge, publish,
#  distribute, sublicense, and/or sell copies of the Library, and to
#  permit persons to whom the Library is furnished to do so, subject to
#  the following conditions:
#
#   The above copyright notice and this permission notice shall be
#   included in all copies or substantial portions of the Library.
#
#   If you modify the Library, you may copy and distribute your modified
#   version of the Library in object code or as an executable provided
#   that you also do one of the following:
#
#    Accompany the modified version of the Library with the complete
#    corresponding machine-readable source code for the modified version
#    of the Library; or,
#
#    Accompany the modified version of the Library with a written offer
#    for a complete machine-readable copy of the corresponding source
#    code of the modified version of the Library.
#
#
#  THE LIBRARY IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
#  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
#  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
#  LIBRARY OR THE USE OR OTHER DEALINGS IN THE LIBRARY.
#

AC_DEFUN([AC_CONFIG_MARS_PLATFORM],[

AC_ARG_WITH([mars-platform],
	[AS_HELP_STRING([--with-mars-platform=TYPE],
		[specify mars platform TYPE (cell | spursengine)])],
	[],
	[AC_MSG_ERROR([missing option --with-mars-platform=])])
AS_CASE([${with_mars_platform}],
	[cell],[],
	[spursengine],[],
	[AC_MSG_ERROR([invalid option --with-mars-platform=${with_mars_platform}])])
AM_CONDITIONAL([MARS_PLATFORM_CELL],
	[test "x${with_mars_platform}" = "xcell"])
AM_CONDITIONAL([MARS_PLATFORM_SPURSENGINE],
	[test "x${with_mars_platform}" = "xspursengine"])
AC_MSG_RESULT([using mars-platform ${with_mars_platform}])

]) # AC_CONFIG_MARS_PLATFORM


AC_DEFUN([AC_CONFIG_MARS_DISCRETE_SHARED_MEMORY],[
AC_ARG_ENABLE(
	[discrete-shared-memory],
	[AS_HELP_STRING([--enable-discrete-shared-memory],
		[assume discrete shared memory between host and MPU
			even if shared memory is identical to host memory,
			for debugging purposes])])
AC_MSG_RESULT([using enable-discrete-shared-memory ${enable_discrete_shared_memory}])
]) # AC_CONFIG_MARS_DISCRETE_SHARED_MEMORY

AC_DEFUN([AC_CONFIG_MARS_COND_WAIT],[
AC_ARG_ENABLE([cond-wait],
	[AS_HELP_STRING([--enable-cond-wait=TYPE],
		[specify how to wait for conditions (busy | futex)])],
	[],[AS_CASE([$with_mars_platform],
		[cell],[enable_cond_wait=futex],
		[enable_cond_wait=busy])])

AS_CASE([$enable_cond_wait],
	[busy],[
		AC_DEFINE([ENABLE_COND_WAIT_BUSY],[1],
			[Define to 1 if busy loop is used to wait for conditions.])],
	[futex],[
		AS_CASE([$with_mars_platform],
			[cell],[],
			[AC_MSG_ERROR([futex is not supported on other platforms than Cell/B.E.])])
		AC_DEFINE([ENABLE_COND_WAIT_FUTEX],[1],
			[Define to 1 if futex is used to wait for conditions.])],
	[AC_MSG_ERROR([invalid option --enable-cond-wait=$enable_cond_wait])])

AC_MSG_RESULT([using enable-cond-wait $enable_cond_wait])
]) # AC_CONFIG_MARS_COND_WAIT

AC_DEFUN([AC_CONFIG_DEBUG],[

AC_ARG_ENABLE(
	[debug],
	[AS_HELP_STRING([--enable-debug], [build debug versions of mars])])
AM_CONDITIONAL([DEBUG], [test "x${enable_debug}" = "xyes"])
AC_MSG_RESULT([using enable-debug ${enable_debug}])

]) # AC_CONFIG_DEBUG


AC_DEFUN([AC_CONFIG_MARS_POST],[
case "$with_mars_platform" in
     spursengine) enable_discrete_shared_memory=yes;;
esac

if test "x$enable_discrete_shared_memory" = "xyes"; then
	AC_DEFINE([MARS_ENABLE_DISCRETE_SHARED_MEMORY],[1],
		[Define to 1 if shared memory between host and MPU is
			separate from host memory.])
fi
])


AC_DEFUN([AC_CONFIG_MARS],[

AC_CONFIG_MARS_PLATFORM
AC_CONFIG_MARS_DISCRETE_SHARED_MEMORY
AC_CONFIG_MARS_COND_WAIT
AC_CONFIG_DEBUG
AC_CONFIG_MARS_POST

]) # AC_CONFIG_MARS

### Host specific definitions

AC_DEFUN([AC_CONFIG_MARS_NUMA],[
AC_ARG_ENABLE(
	[numa],
	[AS_HELP_STRING([--enable-numa], [enable NUMA support])])

case "$with_mars_platform" in
	cell)
		if test "x$enable_numa" = "x"; then
			AC_CHECK_HEADER([numa.h],[enable_numa=yes])
		fi
		;;
	*)
		enable_numa=no
		;;
esac
AC_MSG_RESULT([using enable-numa ${enable_numa}])
if test "x$enable_numa" = "xyes"; then
	AC_DEFINE([MARS_ENABLE_NUMA],[1],
		[Define to 1 if NUMA support is enabled.])
fi
AM_CONDITIONAL([NUMA], [test "x${enable_numa}" = "xyes"])
]) # AC_CONFIG_MARS_NUMA

AC_DEFUN([AC_CHECK_MARS_ALLOCA],[
AC_CHECK_HEADER([alloca.h],
	[AC_DEFINE([MARS_HAVE_ALLOCA_H],[1],
		[Define to 1 if you have the <alloca.h> header file.])])
AC_CHECK_HEADER([malloc.h],
	[AC_DEFINE([MARS_HAVE_MALLOC_H],[1],
		[Define to 1 if you have the <malloc.h> header file.])])
]) # AC_CHECK_MARS_ALLOCA

AC_DEFUN([AC_CONFIG_MARS_HOST],[
AC_CONFIG_MARS
AC_CONFIG_MARS_NUMA
AC_CHECK_MARS_ALLOCA
]) # AC_CONFIG_MARS_HOST


### MPU specific definitions

AC_DEFUN([AC_CONFIG_MARS_MPU],[
AC_CONFIG_MARS
]) # AC_CONFIG_MARS_MPU
