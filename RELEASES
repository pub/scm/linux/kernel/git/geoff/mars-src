RELEASES
--------
This file lists major changes and known major bugs or issues for each release.

2010-01-12:  Yuji Mano <yuji.mano@am.sony.com>

	Release v1.1.5
	==============
	Minor Update Release

	Major Changes
	-------------
	* add host callback framework API test cases

	Major Bugs
	----------
	* Fedora 12: inconsistencies between the kernel version and libnuma version
	  may cause libnuma to fail resulting in MARS context creation error. This
	  is not a MARS bug and needs to be fixed by the Linux distro maintainers.


2009-07-24:  Yuji Mano <yuji.mano@am.sony.com>

	Release v1.1.4
	==============
	Bug Fix Release

	Major Changes
	-------------
	* kernel scheduler bug fixes
	* host side mutex contention fixes
	* error handling fixes
	* test suite fixes

	Major Bugs
	----------
	* none known


2009-05-28:  Yuji Mano <yuji.mano@am.sony.com>

	Release v1.1.3
	==============
	Complete Test Suite

	Major Changes
	-------------
	* add negative functional tests
	* add stress tests
	* minor bug fixes

	Major Bugs
	----------
	* none known


2009-03-31:  Yuji Mano <yuji.mano@am.sony.com>

	Release v1.1.2
	==============
	Host Callback Framework, Optimizations, Performance Tests

	Major Changes
	-------------
	* add host callback framework API
	* add workload/task unschedule API
	* add performance tests
	* reduce task stack space required for context switch by 768 bytes
	* various performance optimizations and minor bug fixes

	Major Bugs
	----------
	* none known


2009-02-13:  Yuji Mano <yuji.mano@am.sony.com>

	Release v1.1.1
	==============
	Kernel and Task Module Optimization and Code Size Reduction

	Major Changes
	-------------
	* reduce kernel area to 12KB
	* MARS_WORKLOAD_MODULE_BASE_ADDR is now 0x3000
	* reduce task module area to 4KB
	* MARS_TASK_BASE_ADDR is now 0x4000
	* remove mars/mars.h (use mars/task.h instead)
	* MPU-side mutex API moved into workload module API
	* kernel scheduler optimization and bug fixes

	Major Bugs
	----------
	* none known


2008-12-19:  Yuji Mano <yuji.mano@am.sony.com>

	Release v1.1.0
	==============
	Workload Module Framework, NUMA Support with Test Suite

	Major Changes
	-------------
	* add support for NUMA
	* add initial test suite
	* add workload module framework
	* add tick counter API
	* cleanup DMA API
	* remove obsolete MPU-side mars/core.h
	* bug fixes

	Major Bugs
	----------
	* kernel scheduler bug - if more than 16 workloads are created,
	  workloads added to end of queue may never be scheduled if workloads at
	  front of queue are schedulable, regardless of priority or fairness


2008-11-11:  Yuji Mano <yuji.mano@am.sony.com>

	Release v1.0.1
	==============
	Bug Fix Release

	Major Changes
	-------------
	* hide internal implementation details from public API headers
	* bug fixes

	Major Bugs
	----------
	* none known


2008-10-23:  Yuji Mano <yuji.mano@am.sony.com>

	Release v1.0.0
	==============
	Major API Cleanups and Changes

	Major Changes
	-------------
	* change API so objects are dynamically allocated
	* change API to remove structure parameter passing
	* clean up header files and remove mars_* prefix from header file names
	* add support for shared MARS context
	* add support for task partial context save
	* add support to returning task exit code
	* add mars/mars_context.h
	* remove mars_debug.h and mars_timer.h from public headers
	* bug fixes

	Major Bugs
	----------
	* host static library undefined symbols
	* mars_task_destroy() - memory leak
	* mars_task_queue_destroy() - memory leak


2008-09-11:  Yuji Mano <yuji.mano@am.sony.com>

	Release v0.9.2
	==============
	Initial Public Release

	Major Changes
	-------------
	* API changes/additions
	* bug fixes

	Major Bugs
	----------
	* dma list usage bug in 64-bit environment
	* workload queue destroy bug - workload queue destroy function does not
	  properly check for all tasks to be completed
	* workload queue workload initialization bug - incorrect setting of
	  workload state bits
	* mars_task_get_name() does not return NULL when NULL is specified for
	  task name at creation
	* task queue 'try_*' functions incorrectly return MARS_ERROR_FORMAT
	  if task has no context save area
	* internal function workload_signal_try_wait() always returns
	  MARS_ERROR_PARAMS
	* task barrier creation does not return error when total is 0
