/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this Library and associated documentation files (the
 * "Library"), to deal in the Library without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Library, and to
 * permit persons to whom the Library is furnished to do so, subject to
 * the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Library.
 *
 *  If you modify the Library, you may copy and distribute your modified
 *  version of the Library in object code or as an executable provided
 *  that you also do one of the following:
 *
 *   Accompany the modified version of the Library with the complete
 *   corresponding machine-readable source code for the modified version
 *   of the Library; or,
 *
 *   Accompany the modified version of the Library with a written offer
 *   for a complete machine-readable copy of the corresponding source
 *   code of the modified version of the Library.
 *
 *
 * THE LIBRARY IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * LIBRARY OR THE USE OR OTHER DEALINGS IN THE LIBRARY.
 */

#ifndef MARS_CALLBACK_TYPES_H
#define MARS_CALLBACK_TYPES_H

/**
 * \file
 * \ingroup group_mars_callback
 * \brief <b>[host/MPU]</b> MARS Callback Types
 */

#include <stdint.h>

/**
 * \ingroup group_mars_callback
 * \brief Size of callback args structure
 */
#define MARS_CALLBACK_ARGS_SIZE			48

/**
 * \ingroup group_mars_callback
 * \brief Alignment of callback args structure
 */
#define MARS_CALLBACK_ARGS_ALIGN		16

/**
 * \ingroup group_mars_callback
 * \brief MARS callback argument structure
 *
 * This structure is an arbitrary callback argument structure.
 */
struct mars_callback_args {
	union {
		/** array of 48 8-bit unsigned ints */
		uint8_t  u8[48];
		/** array of 24 16-bit unsigned ints */
		uint16_t u16[24];
		/** array of 12 32-bit unsigned ints */
		uint32_t u32[12];
		/** array of 6 64-bit unsigned ints */
		uint64_t u64[6];
	} type;
} __attribute__((aligned(MARS_CALLBACK_ARGS_ALIGN)));

/**
 * \ingroup group_mars_callback
 * \brief MARS callback function
 *
 * This is the type definition of the callback function.
 */
typedef int (*mars_callback)(
	const struct mars_callback_args *in, struct mars_callback_args *out);

#endif
