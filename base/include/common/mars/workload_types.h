/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this Library and associated documentation files (the
 * "Library"), to deal in the Library without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Library, and to
 * permit persons to whom the Library is furnished to do so, subject to
 * the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Library.
 *
 *  If you modify the Library, you may copy and distribute your modified
 *  version of the Library in object code or as an executable provided
 *  that you also do one of the following:
 *
 *   Accompany the modified version of the Library with the complete
 *   corresponding machine-readable source code for the modified version
 *   of the Library; or,
 *
 *   Accompany the modified version of the Library with a written offer
 *   for a complete machine-readable copy of the corresponding source
 *   code of the modified version of the Library.
 *
 *
 * THE LIBRARY IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * LIBRARY OR THE USE OR OTHER DEALINGS IN THE LIBRARY.
 */

#ifndef MARS_WORKLOAD_TYPES_H
#define MARS_WORKLOAD_TYPES_H

/**
 * \file
 * \ingroup group_mars_workload_queue
 * \ingroup group_mars_workload_module
 * \brief <b>[host/MPU]</b> MARS Workload Types
 */

#include <stdint.h>

/**
 * \ingroup group_mars_workload_module
 * \brief Base address of workload module
 */
#define MARS_WORKLOAD_MODULE_BASE_ADDR		0x3000

/**
 * \ingroup group_mars_workload_module
 * \brief Maximum length of workload module name
 */
#define MARS_WORKLOAD_MODULE_NAME_LEN_MAX	23

/**
 * \ingroup group_mars_workload_queue
 * \brief Size of workload module structure
 */
#define MARS_WORKLOAD_RESERVED_SIZE		128

/**
 * \ingroup group_mars_workload_queue
 * \brief Size of workload context structure
 */
#define MARS_WORKLOAD_CONTEXT_SIZE		256

/**
 * \ingroup group_mars_workload_queue
 * \brief Alignment of workload context structure
 */
#define MARS_WORKLOAD_CONTEXT_ALIGN		128

/**
 * \ingroup group_mars_workload_context
 * \brief MARS workload context structure
 *
 * This structure stores information about a specific workload.
 *
 * The first \ref MARS_WORKLOAD_RESERVED_SIZE bytes of the workload context
 * structure is reserved and cannot be modified.
 *
 * The remaining area of the structure can be used by the specific workload
 * model implementation as needed.
 */
struct mars_workload_context {
	/** workload reserved area */
	uint8_t reserved[MARS_WORKLOAD_RESERVED_SIZE];
	/** workload model specific data */
	uint8_t data[MARS_WORKLOAD_CONTEXT_SIZE - MARS_WORKLOAD_RESERVED_SIZE];
} __attribute__((aligned(MARS_WORKLOAD_CONTEXT_ALIGN)));

/**
 * \brief MARS workload query types
 *
 * These are the query types you can pass into \ref mars_module_workload_query
 */
enum mars_workload_query {
	MARS_WORKLOAD_QUERY_IS_MODULE_CACHED = 0,	/**< module cached? */
	MARS_WORKLOAD_QUERY_IS_CONTEXT_CACHED,		/**< context cached? */
	MARS_WORKLOAD_QUERY_IS_INITIALIZED,		/**< is initialized? */
	MARS_WORKLOAD_QUERY_IS_READY,			/**< is ready? */
	MARS_WORKLOAD_QUERY_IS_WAITING,			/**< is waiting? */
	MARS_WORKLOAD_QUERY_IS_RUNNING,			/**< is running? */
	MARS_WORKLOAD_QUERY_IS_FINISHED,		/**< is finished? */
	MARS_WORKLOAD_QUERY_IS_SIGNAL_SET		/**< has signal set? */
};

#endif
