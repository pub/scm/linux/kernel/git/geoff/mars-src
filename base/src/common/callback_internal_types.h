/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this Library and associated documentation files (the
 * "Library"), to deal in the Library without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Library, and to
 * permit persons to whom the Library is furnished to do so, subject to
 * the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Library.
 *
 *  If you modify the Library, you may copy and distribute your modified
 *  version of the Library in object code or as an executable provided
 *  that you also do one of the following:
 *
 *   Accompany the modified version of the Library with the complete
 *   corresponding machine-readable source code for the modified version
 *   of the Library; or,
 *
 *   Accompany the modified version of the Library with a written offer
 *   for a complete machine-readable copy of the corresponding source
 *   code of the modified version of the Library.
 *
 *
 * THE LIBRARY IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * LIBRARY OR THE USE OR OTHER DEALINGS IN THE LIBRARY.
 */

#ifndef MARS_CALLBACK_INTERNAL_TYPES_H
#define MARS_CALLBACK_INTERNAL_TYPES_H

#define MARS_CALLBACK_QUEUE_SIZE	128		/* size of 128 bytes */
#define MARS_CALLBACK_QUEUE_ALIGN	128		/* align to 128 bytes */
#define MARS_CALLBACK_QUEUE_MAX		54		/* max depth of queue */

#define MARS_CALLBACK_QUEUE_FLAG_NONE	0x0		/* no flag set */
#define MARS_CALLBACK_QUEUE_FLAG_EXIT	0x1		/* exit flag */
#define MARS_CALLBACK_QUEUE_FLAG_PUSH   0x2		/* push flag */

/* 128 byte callback queue structure */
struct mars_callback_queue {
	uint32_t lock;
	uint32_t flag;
	uint32_t count;
	uint32_t head;
	uint32_t tail;
	uint16_t workload_id[MARS_CALLBACK_QUEUE_MAX];
} __attribute__((aligned(MARS_CALLBACK_QUEUE_ALIGN)));

#endif
