/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this Library and associated documentation files (the
 * "Library"), to deal in the Library without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Library, and to
 * permit persons to whom the Library is furnished to do so, subject to
 * the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Library.
 *
 *  If you modify the Library, you may copy and distribute your modified
 *  version of the Library in object code or as an executable provided
 *  that you also do one of the following:
 *
 *   Accompany the modified version of the Library with the complete
 *   corresponding machine-readable source code for the modified version
 *   of the Library; or,
 *
 *   Accompany the modified version of the Library with a written offer
 *   for a complete machine-readable copy of the corresponding source
 *   code of the modified version of the Library.
 *
 *
 * THE LIBRARY IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * LIBRARY OR THE USE OR OTHER DEALINGS IN THE LIBRARY.
 */

#ifndef MARS_KERNEL_INTERNAL_TYPES_H
#define MARS_KERNEL_INTERNAL_TYPES_H

#include <stdint.h>

#include "mars/mutex_types.h"

#include "callback_internal_types.h"
#include "workload_internal_types.h"

#define MARS_KERNEL_ID_NONE			0xffff

#define MARS_KERNEL_TICKS_FLAG_SYNC_BEGIN	0x1
#define MARS_KERNEL_TICKS_FLAG_SYNC_END		0x2

#define MARS_KERNEL_DMA_TAG			31

#define MARS_KERNEL_PARAMS_SIZE			128
#define MARS_KERNEL_PARAMS_ALIGN		128

/* mars kernel syscalls */
struct mars_kernel_syscalls {
	uint32_t                       (*get_ticks)(void);
	uint64_t                       (*get_mars_context_ea)(void);
	uint16_t                       (*get_kernel_id)(void);
	uint16_t                       (*get_workload_id)(void);
	struct mars_workload_context * (*get_workload)(void);
	struct mars_workload_context * (*get_workload_by_id)(uint16_t id);

	void (*workload_exit)(uint8_t state);
	int  (*workload_query)(uint16_t id, int query);
	int  (*workload_wait_set)(uint16_t id);
	int  (*workload_wait_reset)(void);
	int  (*workload_signal_set)(uint16_t id);
	int  (*workload_signal_reset)(void);
	int  (*workload_schedule_begin)(uint16_t id, uint8_t priority,
				struct mars_workload_context **workload);
	int  (*workload_schedule_end)(uint16_t id, int cancel);
	int  (*workload_unschedule_begin)(uint16_t id,
				struct mars_workload_context **workload);
	int  (*workload_unschedule_end)(uint16_t id);

	int  (*host_signal_send)(uint64_t watch_point_ea);
	int  (*host_callback_set)(uint64_t callback_ea,
				  const struct mars_callback_args *in);
	int  (*host_callback_reset)(struct mars_callback_args *out);

	int  (*mutex_lock_get)(uint64_t mutex_ea, struct mars_mutex *mutex);
	int  (*mutex_unlock_put)(uint64_t mutex_ea, struct mars_mutex *mutex);

	int  (*dma_get)(void *ls, uint64_t ea, uint32_t size, uint32_t tag);
	int  (*dma_put)(const void *ls, uint64_t ea, uint32_t size,
			uint32_t tag);
	int  (*dma_wait)(uint32_t tag);
};

/* mars kernel ticks */
struct mars_kernel_ticks {
	uint32_t flag;
	uint32_t offset;
};

/* mars kernel parameters */
struct mars_kernel_params {
	struct mars_kernel_ticks kernel_ticks;
	uint16_t kernel_id;
	uint64_t mars_context_ea;
	uint64_t workload_queue_ea;
	uint64_t callback_queue_ea;
	uint8_t pad[MARS_KERNEL_PARAMS_SIZE - 48];
} __attribute__((aligned(MARS_KERNEL_PARAMS_ALIGN)));

/* mars kernel buffer */
union mars_kernel_buffer {
	struct mars_callback_queue callback_queue;
	struct mars_workload_queue_header workload_queue_header;
	struct mars_workload_queue_block workload_queue_block;
};

/* mars kernel mutex */
int mutex_lock_get(uint64_t mutex_ea, struct mars_mutex *mutex);
int mutex_unlock_put(uint64_t mutex_ea, struct mars_mutex *mutex);

/* mars kernel dma */
int dma_get(void *ls, uint64_t ea, uint32_t size, uint32_t tag);
int dma_put(const void *ls, uint64_t ea, uint32_t size, uint32_t tag);
int dma_wait(uint32_t tag);

#endif
