/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this Library and associated documentation files (the
 * "Library"), to deal in the Library without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Library, and to
 * permit persons to whom the Library is furnished to do so, subject to
 * the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Library.
 *
 *  If you modify the Library, you may copy and distribute your modified
 *  version of the Library in object code or as an executable provided
 *  that you also do one of the following:
 *
 *   Accompany the modified version of the Library with the complete
 *   corresponding machine-readable source code for the modified version
 *   of the Library; or,
 *
 *   Accompany the modified version of the Library with a written offer
 *   for a complete machine-readable copy of the corresponding source
 *   code of the modified version of the Library.
 *
 *
 * THE LIBRARY IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * LIBRARY OR THE USE OR OTHER DEALINGS IN THE LIBRARY.
 */

#include <sched.h>
#include <errno.h>
#include <limits.h>

#include "config.h"

#include "mars/base.h"
#include "mars/error.h"

#ifdef ENABLE_COND_WAIT_FUTEX

#include <pthread.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <linux/futex.h>

#define FUTEX_WATCH_POINT_MAX 256

static pthread_mutex_t futex_watch_point_lock = PTHREAD_MUTEX_INITIALIZER;
static uint64_t futex_watch_point_ea[FUTEX_WATCH_POINT_MAX];
static int futex_watch_point_count;

static int futex_wait(void *ptr, int val)
{
	int ret;

	ret = syscall(__NR_futex, ptr, FUTEX_WAIT, val, NULL, NULL, 0);
	if (ret && errno != EWOULDBLOCK)
		return MARS_ERROR_INTERNAL;

	return MARS_SUCCESS;
}

static int futex_wake(void *ptr, int val)
{
	int ret;

	ret = syscall(__NR_futex, ptr, FUTEX_WAKE, val, NULL, NULL, 0);
	if (ret < 0)
		return MARS_ERROR_INTERNAL;

	return MARS_SUCCESS;
}

static int watch_point_register(uint64_t ea)
{
	int i;

	pthread_mutex_lock(&futex_watch_point_lock);

	if (futex_watch_point_count >= FUTEX_WATCH_POINT_MAX) {
		pthread_mutex_unlock(&futex_watch_point_lock);
		return -1;
	}

	for (i = 0; i < FUTEX_WATCH_POINT_MAX; i++) {
		if (!futex_watch_point_ea[i]) {
			futex_watch_point_ea[i] = ea;
			futex_watch_point_count++;
			break;
		}
	}

	pthread_mutex_unlock(&futex_watch_point_lock);

	return i;
}

static void watch_point_unregister(int index)
{
	pthread_mutex_lock(&futex_watch_point_lock);

	futex_watch_point_ea[index] = 0;
	futex_watch_point_count--;

	pthread_mutex_unlock(&futex_watch_point_lock);
}

static int is_watch_point(uint64_t ea)
{
	int i;
	int valid = 0;

	for (i = 0; futex_watch_point_count && i < FUTEX_WATCH_POINT_MAX; i++) {
		if (futex_watch_point_ea[i] == ea)
			return 1;
		else if (futex_watch_point_ea[i] &&
			 futex_watch_point_count == ++valid)
			break;
	}

	return 0;
}

#endif

int mars_ea_cond_wait(uint64_t watch_point_ea,
		      int (*test_cond)(uint32_t , void *),
		      void *test_cond_param)
{
	int ret;
	int index = -1;

	while (1) {
		uint32_t val;

#ifdef ENABLE_COND_WAIT_FUTEX
		if (index < 0)
			index = watch_point_register(watch_point_ea);
#endif

		val = mars_ea_get_uint32(watch_point_ea);

		ret = (*test_cond)(val, test_cond_param);
		if (ret >= 0)
			break;

#ifdef ENABLE_COND_WAIT_FUTEX
		if (index >= 0) {
			ret = futex_wait(mars_ea_to_ptr(watch_point_ea), val);
			if (ret != MARS_SUCCESS)
				break;
		}
		else
#endif
		{
			sched_yield();
		}
	}

#ifdef ENABLE_COND_WAIT_FUTEX
	if (index >= 0)
		watch_point_unregister(index);
#endif

	return ret;
}

int mars_ea_cond_signal(uint64_t watch_point_ea, int broadcast)
{
#ifdef ENABLE_COND_WAIT_FUTEX
	pthread_mutex_lock(&futex_watch_point_lock);

	if (is_watch_point(watch_point_ea))
		futex_wake(mars_ea_to_ptr(watch_point_ea),
			   broadcast ? INT_MAX : 1);

	pthread_mutex_unlock(&futex_watch_point_lock);
#endif

	return MARS_SUCCESS;
}
