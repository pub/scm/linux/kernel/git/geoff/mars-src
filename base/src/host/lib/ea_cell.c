/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this Library and associated documentation files (the
 * "Library"), to deal in the Library without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Library, and to
 * permit persons to whom the Library is furnished to do so, subject to
 * the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Library.
 *
 *  If you modify the Library, you may copy and distribute your modified
 *  version of the Library in object code or as an executable provided
 *  that you also do one of the following:
 *
 *   Accompany the modified version of the Library with the complete
 *   corresponding machine-readable source code for the modified version
 *   of the Library; or,
 *
 *   Accompany the modified version of the Library with a written offer
 *   for a complete machine-readable copy of the corresponding source
 *   code of the modified version of the Library.
 *
 *
 * THE LIBRARY IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * LIBRARY OR THE USE OR OTHER DEALINGS IN THE LIBRARY.
 */

#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <ppu_intrinsics.h>

#include "config.h"

#include "mars/base.h"

#include "numa_internal.h"

#ifdef MARS_ENABLE_NUMA

union mars_numa_ea_block {
	struct {
		void *top;
		size_t size;
	} info;
	unsigned char pad[16];
};


static uint64_t numa_ea_memalign(size_t boundary, size_t size)
{
	uint64_t ea;
	void *top;
	union mars_numa_ea_block *block;
	size_t alloc_size;

	if (boundary < sizeof(*block))
		boundary = sizeof(*block);

	alloc_size = size + sizeof(*block) + boundary - 1;
	top = numa_alloc(alloc_size);
	if (!top) return 0;

	ea = ((uintptr_t)top + sizeof(*block) + boundary - 1) &
		~(uintptr_t)(boundary - 1);
	block = mars_ea_to_ptr(ea) - sizeof(*block);

	block->info.top = top;
	block->info.size = alloc_size;

	return ea;
}

static void numa_ea_free(uint64_t ea)
{
	void *ptr = mars_ea_to_ptr(ea);
	union mars_numa_ea_block *block = ptr - sizeof(*block);

	if (ptr)
		numa_free(block->info.top, block->info.size);
}

#else /* ! MARS_ENABLE_NUMA */

/* These functions must not be called */
static uint64_t numa_ea_memalign(size_t boundary, size_t size)
{
	(void)boundary;
	(void)size;

	return 0;
}

static void numa_ea_free(uint64_t ea)
{
	(void)ea;
}

#endif /* ! MARS_ENABLE_NUMA */

uint64_t mars_ea_memalign(size_t boundary, size_t size)
{
	if (mars_numa_enabled())
		return numa_ea_memalign(boundary, size);
	else
		return mars_ptr_to_ea(memalign(boundary, size));
}

void mars_ea_free(uint64_t ea)
{
	if (mars_numa_enabled())
		numa_ea_free(ea);
	else
		free(mars_ea_to_ptr(ea));
}

/* copy data from EA to host */
void mars_ea_get(uint64_t ea, void *mem, size_t size)
{
	const void *src = mars_ea_to_ptr(ea);
	if (src != mem)
		memcpy(mem, src, size);
}

/* get uint8 value from EA */
uint8_t mars_ea_get_uint8(uint64_t ea)
{
	return *(uint8_t *)mars_ea_to_ptr(ea);
}

/* get uint16 value from EA */
uint16_t mars_ea_get_uint16(uint64_t ea)
{
	return *(uint16_t *)mars_ea_to_ptr(ea);
}

/* get uint32 value from EA */
uint32_t mars_ea_get_uint32(uint64_t ea)
{
	return *(uint32_t *)mars_ea_to_ptr(ea);
}

/* get uint64 value from EA */
uint64_t mars_ea_get_uint64(uint64_t ea)
{
	return *(uint64_t *)mars_ea_to_ptr(ea);
}

/* copy data from host to EA */
void mars_ea_put(uint64_t ea, const void *mem, size_t size)
{
	void *dst = mars_ea_to_ptr(ea);
	if (dst != mem)
		memcpy(dst, mem, size);
}

/* put uint8 value to EA */
void mars_ea_put_uint8(uint64_t ea, uint8_t value)
{
	*(uint8_t *)mars_ea_to_ptr(ea) = value;
}

/* put uint16 value to EA */
void mars_ea_put_uint16(uint64_t ea, uint16_t value)
{
	*(uint16_t *)mars_ea_to_ptr(ea) = value;
}

/* put uint32 value to EA */
void mars_ea_put_uint32(uint64_t ea, uint32_t value)
{
	*(uint32_t *)mars_ea_to_ptr(ea) = value;
}

/* put uint64 value to EA */
void mars_ea_put_uint64(uint64_t ea, uint64_t value)
{
	*(uint64_t *)mars_ea_to_ptr(ea) = value;
}

/* map readonly area on host memory to EA */
uint64_t mars_ea_map(void *ptr, size_t size)
{
#ifdef MARS_ENABLE_DISCRETE_SHARED_MEMORY
	uint64_t copy_ea;

	copy_ea = mars_ea_memalign(16, size); /* FIXME: 16 -> any macro */
	if (!copy_ea)
		return 0;

	mars_ea_put(copy_ea, ptr, size);
	mars_ea_sync();

	return copy_ea;
#else /* !MARS_ENABLE_DISCRETE_SHARED_MEMORY */
	(void)size;
	return mars_ptr_to_ea(ptr);
#endif /* !MARS_ENABLE_DISCRETE_SHARED_MEMORY */
}

/* unmap area mapped by mars_ea_map */
void mars_ea_unmap(uint64_t ea, size_t size)
{
	(void)size;

#ifdef MARS_ENABLE_DISCRETE_SHARED_MEMORY
	mars_ea_free(ea);
#else /* !MARS_ENABLE_DISCRETE_SHARED_MEMORY */
	(void)ea;
#endif /* !MARS_ENABLE_DISCRETE_SHARED_MEMORY */
}

/* sync EA */
void mars_ea_sync(void)
{
	__lwsync();
}
