/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this Library and associated documentation files (the
 * "Library"), to deal in the Library without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Library, and to
 * permit persons to whom the Library is furnished to do so, subject to
 * the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Library.
 *
 *  If you modify the Library, you may copy and distribute your modified
 *  version of the Library in object code or as an executable provided
 *  that you also do one of the following:
 *
 *   Accompany the modified version of the Library with the complete
 *   corresponding machine-readable source code for the modified version
 *   of the Library; or,
 *
 *   Accompany the modified version of the Library with a written offer
 *   for a complete machine-readable copy of the corresponding source
 *   code of the modified version of the Library.
 *
 *
 * THE LIBRARY IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * LIBRARY OR THE USE OR OTHER DEALINGS IN THE LIBRARY.
 */

#ifndef MARS_NUMA_INTERNAL_H
#define MARS_NUMA_INTERNAL_H

#ifdef MARS_ENABLE_NUMA
#include <numa.h>

#if !defined(LIBNUMA_API_VERSION) || (LIBNUMA_API_VERSION <= 1)

typedef nodemask_t numa_bitmask_t;

static inline int numa_bitmask_isbitset(nodemask_t mask, unsigned int n)
{
	return nodemask_isset(&mask, n);
}

static inline int numa_bitmask_equal(nodemask_t a, nodemask_t b)
{
	return nodemask_equal(&a, &b);
}

static inline void numa_bitmask_free(nodemask_t mask)
{
	(void)mask;
	/* do nothing */
}

#else /* LIBNUMA_API_VERSION >= 2 */

typedef struct bitmask *numa_bitmask_t;

#endif /* LIBNUMA_API_VERSION >= 2 */

#endif /* MARS_ENABLE_NUMA */

static inline int mars_numa_enabled(void)
{
#ifdef MARS_ENABLE_NUMA
	return (numa_available() != -1);
#else
	return 0;
#endif
}

#endif
