/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this Library and associated documentation files (the
 * "Library"), to deal in the Library without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Library, and to
 * permit persons to whom the Library is furnished to do so, subject to
 * the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Library.
 *
 *  If you modify the Library, you may copy and distribute your modified
 *  version of the Library in object code or as an executable provided
 *  that you also do one of the following:
 *
 *   Accompany the modified version of the Library with the complete
 *   corresponding machine-readable source code for the modified version
 *   of the Library; or,
 *
 *   Accompany the modified version of the Library with a written offer
 *   for a complete machine-readable copy of the corresponding source
 *   code of the modified version of the Library.
 *
 *
 * THE LIBRARY IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * LIBRARY OR THE USE OR OTHER DEALINGS IN THE LIBRARY.
 */

#ifndef MARS_CONTEXT_INTERNAL_TYPES_H
#define MARS_CONTEXT_INTERNAL_TYPES_H

#include <stdint.h>

#ifdef HAVE_PTHREAD_H
#include <pthread.h>
#endif

#define MARS_SHARED_CONTEXT_MAX		16

#ifdef HAVE_LIBSPE2
#include <libspe2.h>
typedef struct {
	pthread_t mpu_context_thread;
	pthread_t mpu_handler_thread;
	spe_context_ptr_t spe_context;
} mars_mpu_context_t;
#endif

#ifdef HAVE_LIBPTHREAD
typedef pthread_mutex_t mars_host_mutex_t;
typedef pthread_t mars_callback_t;
#endif

struct mars_context {
	/* parameters for the MARS kernel */
	uint64_t kernel_params_ea;
	/* workload queue where workloads are added */
	uint64_t workload_queue_ea;
	/* callback queue where host callback requests are added */
	uint64_t callback_queue_ea;
	/* reference count */
	uint32_t reference_count;
	/* num of mpu context threads */
	uint32_t mpu_context_count;
	/* array of mpu contexts */
	mars_mpu_context_t *mpu_contexts;
	/* callback handler */
	mars_callback_t callback_handler;
};

int mars_mpu_max(uint32_t *num);
int mars_mpu_run(mars_mpu_context_t *mpu, uint64_t params_ea);
int mars_mpu_wait(mars_mpu_context_t *mpu);

int mars_shared_context_get(struct mars_context **mars);
int mars_shared_context_register(struct mars_context *mars);
int mars_shared_context_unregister(struct mars_context *mars);

int mars_workload_queue_create(struct mars_context *mars);
int mars_workload_queue_destroy(struct mars_context *mars);
int mars_workload_queue_exit(struct mars_context *mars);

int mars_callback_queue_create(struct mars_context *mars);
int mars_callback_queue_destroy(struct mars_context *mars);
int mars_callback_queue_exit(struct mars_context *mars);

int mars_host_mutex_lock(mars_host_mutex_t *mutex);
int mars_host_mutex_unlock(mars_host_mutex_t *mutex);

extern mars_host_mutex_t mars_shared_context_lock;

#endif
