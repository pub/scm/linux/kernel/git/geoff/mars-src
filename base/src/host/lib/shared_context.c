/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this Library and associated documentation files (the
 * "Library"), to deal in the Library without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Library, and to
 * permit persons to whom the Library is furnished to do so, subject to
 * the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Library.
 *
 *  If you modify the Library, you may copy and distribute your modified
 *  version of the Library in object code or as an executable provided
 *  that you also do one of the following:
 *
 *   Accompany the modified version of the Library with the complete
 *   corresponding machine-readable source code for the modified version
 *   of the Library; or,
 *
 *   Accompany the modified version of the Library with a written offer
 *   for a complete machine-readable copy of the corresponding source
 *   code of the modified version of the Library.
 *
 *
 * THE LIBRARY IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * LIBRARY OR THE USE OR OTHER DEALINGS IN THE LIBRARY.
 */

#include "config.h"

#include "mars/context.h"
#include "mars/error.h"

#include "context_internal.h"
#include "numa_internal.h"

#ifdef MARS_ENABLE_NUMA

struct mars_numa_shared_context
{
	struct mars_context *mars;
	numa_bitmask_t mask;
};

static int num_numa_shared_contexts;
static struct mars_numa_shared_context
	numa_shared_context_map[MARS_SHARED_CONTEXT_MAX];

static struct mars_context *numa_shared_context_get(void)
{
	int i;
	numa_bitmask_t mask;
	struct mars_context *mars = NULL;

	if (num_numa_shared_contexts == 0)
		return NULL;

	mask = numa_get_run_node_mask();
	for (i = 0; i < MARS_SHARED_CONTEXT_MAX; i++) {
		struct mars_numa_shared_context *entry =
			&numa_shared_context_map[i];
		if (entry->mars && numa_bitmask_equal(mask, entry->mask)) {
			mars = entry->mars;
			break;
		}
	}
	numa_bitmask_free(mask);

	return mars;
}

static int numa_shared_context_register(struct mars_context *mars)
{
	int i;

	for (i = 0; i < MARS_SHARED_CONTEXT_MAX; i++) {
		struct mars_numa_shared_context *entry =
			&numa_shared_context_map[i];
		if (!entry->mars) {
			entry->mask = numa_get_run_node_mask();
			entry->mars = mars;
			num_numa_shared_contexts++;
			return MARS_SUCCESS;
		}
	}

	return MARS_ERROR_LIMIT;
}

static int numa_shared_context_unregister(struct mars_context *mars)
{
	int i;

	if (num_numa_shared_contexts == 0)
		return MARS_SUCCESS;

	for (i = 0; i < MARS_SHARED_CONTEXT_MAX; i++) {
		struct mars_numa_shared_context *entry =
			&numa_shared_context_map[i];
		if (entry->mars == mars) {
			numa_bitmask_free(entry->mask);
			entry->mars = NULL;
			num_numa_shared_contexts--;
			return MARS_SUCCESS;
		}
	}

	return MARS_ERROR_INTERNAL;
}

#else /* ! MARS_ENABLE_NUMA */

/* These functions must not be called */
static struct mars_context *numa_shared_context_get(void)
{
	return NULL;
}

static int numa_shared_context_register(struct mars_context *mars)
{
	(void)mars;

	return MARS_ERROR_INTERNAL;
}

static int numa_shared_context_unregister(struct mars_context *mars)
{
	(void)mars;

	return MARS_ERROR_INTERNAL;
}

#endif /* ! MARS_ENABLE_NUMA */


/* Note that it is assumed that global data access in this file is
 * protected by caller's lock.
 */
static struct mars_context *shared_context;

int mars_shared_context_get(struct mars_context **mars)
{
	if (mars_numa_enabled())
		*mars = numa_shared_context_get();
	else
		*mars = shared_context;

	if (!*mars)
		return MARS_ERROR_INTERNAL;

	return MARS_SUCCESS;
}

int mars_shared_context_register(struct mars_context *mars)
{
	if (mars_numa_enabled())
		return numa_shared_context_register(mars);
	else {
		shared_context = mars;
		return MARS_SUCCESS;
	}
}

int mars_shared_context_unregister(struct mars_context *mars)
{
	if (mars_numa_enabled())
		return numa_shared_context_unregister(mars);
	else {
		if (mars == shared_context)
			shared_context = NULL;
		return MARS_SUCCESS;
	}
}
