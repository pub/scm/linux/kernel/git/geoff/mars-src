/*
 * Copyright 2008 Sony Corporation of America
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this Library and associated documentation files (the
 * "Library"), to deal in the Library without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Library, and to
 * permit persons to whom the Library is furnished to do so, subject to
 * the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Library.
 *
 *  If you modify the Library, you may copy and distribute your modified
 *  version of the Library in object code or as an executable provided
 *  that you also do one of the following:
 *
 *   Accompany the modified version of the Library with the complete
 *   corresponding machine-readable source code for the modified version
 *   of the Library; or,
 *
 *   Accompany the modified version of the Library with a written offer
 *   for a complete machine-readable copy of the corresponding source
 *   code of the modified version of the Library.
 *
 *
 * THE LIBRARY IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * LIBRARY OR THE USE OR OTHER DEALINGS IN THE LIBRARY.
 */

#include <string.h>

#include "config.h"

#include "mars/base.h"
#include "mars/context.h"
#include "mars/error.h"

#include "context_internal.h"
#include "workload_internal_types.h"
#include "numa_internal.h"

#ifdef MARS_ENABLE_NUMA

static int numa_mpu_max(void)
{
	int i;
	int max_node = numa_max_node();
	numa_bitmask_t mask = numa_get_run_node_mask();
	int num = 0;

	for (i = 0; i <= max_node; i++) {
		int ret;
		if (numa_bitmask_isbitset(mask, i)) {
			/* this call assumes assignment of
			 * each node number is identical
			 * between numa API and libspe.
			 */
			ret = spe_cpu_info_get(SPE_COUNT_USABLE_SPES, i);
			if (ret < 0) {
				num = ret;
				break;
			}
			num += ret;
		}
	}
	numa_bitmask_free(mask);

	return num;
}

#else /* ! MARS_ENABLE_NUMA */

/* This function must not be called */
static int numa_mpu_max(void)
{
	return -1;
}

#endif /* ! MARS_ENABLE_NUMA */

extern const unsigned char mars_kernel_entry[];

int mars_mpu_max(uint32_t *num)
{
	int mpu_max;

	if (mars_numa_enabled())
		mpu_max = numa_mpu_max();
	else
		mpu_max = spe_cpu_info_get(SPE_COUNT_USABLE_SPES, -1);

	if (mpu_max < 0)
		return MARS_ERROR_INTERNAL;

	*num = mpu_max;

	return MARS_SUCCESS;
}

#ifdef ENABLE_COND_WAIT_FUTEX

static void *mpu_handler_thread(void *arg)
{
	int ret;
	spe_context_ptr_t spe = (spe_context_ptr_t)arg;

	while (1) {
  		unsigned int ea_h, ea_l;
		uint64_t ea;

		ret = spe_out_intr_mbox_read(spe, &ea_l, 1,
					     SPE_MBOX_ANY_BLOCKING);
		if (ret == 0)
			goto error;
		else if (ret == -1)
			continue;

		do {
			ret = spe_out_mbox_read(spe, &ea_h, 1);
			if (ret == -1 && errno != EINTR)
				goto error;
		} while (ret != 1);

		ea = ((uint64_t)ea_h << 32) | ea_l;
		if (ea == MARS_HOST_SIGNAL_EXIT)
			break;

		mars_ea_cond_signal(ea, 1);
	}

	return (void *)(uintptr_t)MARS_SUCCESS;

error:
	return (void *)(uintptr_t)MARS_ERROR_INTERNAL;
}

#endif /* ENABLE_COND_WAIT_FUTEX */

struct mars_mpu_context_thread_arg {
	spe_context_ptr_t spe_context;
	uint64_t params_ea;
};

static void *mpu_context_thread(void *arg)
{
	int ret;
	unsigned int entry = SPE_DEFAULT_ENTRY;
	spe_program_handle_t prog;
	struct mars_mpu_context_thread_arg *thread_arg =
		(struct mars_mpu_context_thread_arg *)arg;

	memset(&prog, 0, sizeof(prog));
	prog.handle_size = sizeof(prog);
	prog.elf_image = (void *)mars_kernel_entry;

	ret = spe_program_load(thread_arg->spe_context, &prog);
	if (ret)
		return (void *)(uintptr_t)MARS_ERROR_INTERNAL;

	ret = spe_context_run(thread_arg->spe_context, &entry, 0,
			      mars_ea_to_ptr(thread_arg->params_ea), NULL,
			      NULL);
	if (ret)
		return (void *)(uintptr_t)MARS_ERROR_INTERNAL;

	return (void *)(uintptr_t)MARS_SUCCESS;
}

int mars_mpu_run(mars_mpu_context_t *mpu, uint64_t params_ea)
{
	int ret;
	static struct mars_mpu_context_thread_arg thread_arg;

	mpu->spe_context = spe_context_create(0, NULL);
	if (!mpu->spe_context)
		return errno == ENOMEM ? MARS_ERROR_MEMORY :
					 MARS_ERROR_INTERNAL;

#ifdef ENABLE_COND_WAIT_FUTEX
	ret = pthread_create(&mpu->mpu_handler_thread, NULL,
			     mpu_handler_thread, mpu->spe_context);
	if (ret) {
		spe_context_destroy(mpu->spe_context);
		return ret == EAGAIN ? MARS_ERROR_LIMIT : MARS_ERROR_INTERNAL;
	}
#endif /* ENABLE_COND_WAIT_FUTEX */

	thread_arg.spe_context = mpu->spe_context;
	thread_arg.params_ea = params_ea;

	ret = pthread_create(&mpu->mpu_context_thread, NULL,
			     mpu_context_thread, &thread_arg);
	if (ret) {
		spe_context_destroy(mpu->spe_context);
		return ret == EAGAIN ? MARS_ERROR_LIMIT : MARS_ERROR_INTERNAL;
	}

	return MARS_SUCCESS;
}

int mars_mpu_wait(mars_mpu_context_t *mpu)
{
	int ret;
	void *p_ret;

	ret = pthread_join(mpu->mpu_context_thread, &p_ret);
	if (ret || p_ret)
		return MARS_ERROR_INTERNAL;

	ret = pthread_join(mpu->mpu_handler_thread, &p_ret);
	if (ret || p_ret)
		return MARS_ERROR_INTERNAL;

	ret = spe_context_destroy(mpu->spe_context);
	if (ret)
		return MARS_ERROR_INTERNAL;

	return MARS_SUCCESS;
}
