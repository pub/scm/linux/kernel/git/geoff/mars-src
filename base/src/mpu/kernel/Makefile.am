## Makefile.am -- Process this file with automake to produce Makefile.in
#
#  Copyright 2008 Sony Corporation of America
#
#
#  Permission is hereby granted, free of charge, to any person obtaining
#  a copy of this Library and associated documentation files (the
#  "Library"), to deal in the Library without restriction, including
#  without limitation the rights to use, copy, modify, merge, publish,
#  distribute, sublicense, and/or sell copies of the Library, and to
#  permit persons to whom the Library is furnished to do so, subject to
#  the following conditions:
#
#   The above copyright notice and this permission notice shall be
#   included in all copies or substantial portions of the Library.
#
#   If you modify the Library, you may copy and distribute your modified
#   version of the Library in object code or as an executable provided
#   that you also do one of the following:
#
#    Accompany the modified version of the Library with the complete
#    corresponding machine-readable source code for the modified version
#    of the Library; or,
#
#    Accompany the modified version of the Library with a written offer
#    for a complete machine-readable copy of the corresponding source
#    code of the modified version of the Library.
#
#
#  THE LIBRARY IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
#  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
#  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
#  LIBRARY OR THE USE OR OTHER DEALINGS IN THE LIBRARY.
#

extra_cppflags =
extra_cflags =
extra_ldflags =

if DEBUG
 extra_cppflags += "-DDEBUG"
 # debug kernel is 64K
 extra_ldflags += -Wl,--defsym=__stack=0x0fff0
 CFLAGS += -O0
else
 extra_cppflags += "-DNDEBUG"
 # release kernel is 12K
 extra_ldflags += -Wl,--defsym=__stack=0x02ff0
 CFLAGS += -Os
endif

if MARS_PLATFORM_CELL
endif

if MARS_PLATFORM_SPURSENGINE
endif

MAINTAINERCLEANFILES = Makefile.in

AM_CPPFLAGS = \
	$(extra_cppflags) \
	-I$(srcdir)/../../../include/common \
	-I$(srcdir)/../../../include/mpu \
	-I$(srcdir)/../../../src/common

AM_CCASFLAGS = \
	$(extra_cppflags)

AM_CFLAGS = \
	$(extra_cflags) \
	-mfixed-range=80-127 \
	-ffunction-sections \
	-fdata-sections \
	-W \
	-Wall \
	-Wunused \
	-Winline \
	-Wredundant-decls \
	-Wstrict-prototypes \
	-Wmissing-prototypes \
	-Wmissing-declarations

AM_LDFLAGS = \
	$(extra_ldflags) \
	-Wl,-Map -Wl,$@.map -Wl,--cref \
	-Wl,-gc-sections \
	-Wl,--sort-common \
	-Wl,--sort-section -Wl,alignment \
	-Wl,-N \
	-Wl,-s

noinst_PROGRAMS = mars_kernel

mars_kernel_SOURCES = \
	$(srcdir)/../../../src/common/*.h \
	dma.c \
	kernel.c \
	mutex.c \
	switch.S

CLEANFILES = *.map
