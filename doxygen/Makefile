## Makefile
#
#  Copyright 2008 Sony Corporation of America
#
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#  THE SOFTWARE.
#

VERSION		= build
PACKAGE		= mars-doxygen-$(VERSION)

MARS_SRC_DIR	= `cd .. && pwd`
MARS_BASE_DIR	= $(MARS_SRC_DIR)/base
MARS_TASK_DIR	= $(MARS_SRC_DIR)/task

DOXY_PACKAGE	= mars-docs-$(VERSION)
DOXY_LICENSE	= fdl.txt
DOXY_CONFIG	= $(DOXY_SRC_DIR)/Doxyfile
DOXY_HTML_DIR	= $(DOXY_PACKAGE)/html
DOXY_SRC_DIR	= src
DOXY_SRC_IMAGES	= `ls $(DOXY_SRC_DIR)/*.svg | sed 's,.svg,,g'`

RSVG_CONVERT	= rsvg-convert

docdir = /usr/share/doc/mars-$(VERSION)
htmldir = $(docdir)

.PHONY: all build rebuild check html docs docs-dist distdir dist
.PHONY: mostlyclean clean distclean maintainer-clean
.PHONY: install install-data install-exec uninstall installdirs installcheck
.PHONY: install-dvi install-html install-info install-ps install-pdf
.PHONY: dvi info ps pdf
.PHONY: tags ctags

all: build

build: html

html: $(DOXY_CONFIG)
# Create build dir
	@mkdir -p $(DOXY_PACKAGE) $(DOXY_HTML_DIR)

# Copy license file
	@cp -a $(DOXY_SRC_DIR)/$(DOXY_LICENSE) $(DOXY_PACKAGE)

# Convert SVG images to PNG images
	@for img in $(DOXY_SRC_IMAGES); \
		do $(RSVG_CONVERT) -f png -o $$img.png $$img.svg || exit 1; \
	done

# Process doxygen
	@doxygen $(DOXY_CONFIG)

# Remove generated files
	@rm -rf $(DOXY_CONFIG) $(DOXY_SRC_DIR)/*.png

$(DOXY_CONFIG): $(DOXY_CONFIG).in
	@sed \
		-e s,@MARS_BASE_DIR@,$(MARS_BASE_DIR),g \
		-e s,@MARS_TASK_DIR@,$(MARS_TASK_DIR),g \
		-e s,@DOXY_SRC_DIR@,$(DOXY_SRC_DIR),g \
		-e s,@DOXY_HTML_DIR@,$(DOXY_HTML_DIR),g \
		$(DOXY_CONFIG).in > $(DOXY_CONFIG)

rebuild: clean build

check:

install: install-data

install-data: install-html

install-html: build
	@mkdir -p $(DESTDIR)$(htmldir)
	@cp -a $(DOXY_PACKAGE)/* $(DESTDIR)$(htmldir)

clean:
	@rm -rf $(DOXY_PACKAGE) $(DOXY_CONFIG)
	@rm -rf $(DOXY_SRC_DIR)/*.png

docs: html

docs-dist: rebuild
	@tar cvzf $(DOXY_PACKAGE).tar.gz $(DOXY_PACKAGE)
	@rm -rf $(DOXY_PACKAGE)

dist:
	$(MAKE) distdir=$(PACKAGE) distdir
	@tar cvzf $(PACKAGE).tar.gz $(PACKAGE)
	@rm -rf $(PACKAGE)

distdir: distclean
	@mkdir -p $(distdir)
	@cp -rf src Makefile COPYING README $(distdir)
	@rm -f `find  $(distdir) -name '.gitignore'`

distclean: clean
	@rm -rf $(DOXY_PACKAGE).tar.gz $(DOXY_PACKAGE)
	@rm -rf $(PACKAGE).tar.gz $(PACKAGE)

mostlyclean: clean

maintainer-clean: distclean

install-exec uninstall installdirs installcheck:
install-dvi install-info install-ps install-pdf:
dvi info ps pdf:
tags ctags:
